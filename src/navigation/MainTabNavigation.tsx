import React from 'react'
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import ScreensName from '@constant/screensName'
import Account from '@screens/Account/Account'
import Home from '@screens/Home/Home'
import {MainTabParamList, MainTabParamsDefault} from '@app-types/NavigationType'
import MainTabBar from '@components/MainTabBar'
import {translate} from '@i18n/translate'
import {NavigationBar} from '@ui-kit/components'
import ListCars from '@screens/ListCars/ListCars'
import {TxKeyPath} from '@i18n/i18n'

const {Navigator, Screen} = createBottomTabNavigator<MainTabParamList>()

export default function MainTabNavigation() {
  return (
    <Navigator
      screenOptions={{
        header: ({route: {name, params}}) => {
          if (name === ScreensName.LIST) {
            const title = (params as MainTabParamsDefault)?.title
            return (
              <NavigationBar title={translate(title as TxKeyPath)} hideBack />
            )
          }
          return null // => don't showing header
        },
      }}
      tabBar={props => <MainTabBar {...props} />}
    >
      <Screen
        initialParams={{
          icon: 'IcoMoon',
          title: 'common.home',
        }}
        name={ScreensName.HOME_SCREEN}
        component={Home}
      />
      <Screen
        initialParams={{
          icon: 'IcoMoon',
          title: 'common.list',
        }}
        name={ScreensName.LIST}
        component={ListCars}
      />
      <Screen
        initialParams={{
          icon: 'user',
          title: 'common.account',
        }}
        name={ScreensName.ACCOUNT}
        component={Account}
      />
    </Navigator>
  )
}
