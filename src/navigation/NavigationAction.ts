/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable max-len */
import {
  CommonActions,
  createNavigationContainerRef,
  StackActions,
} from '@react-navigation/native'
import ScreensName from '@constant/screensName'
import store from '@app/stores/index'
import AppDataSingleton from '@constant/singletons/appData.singleton'
import {RootStackParamList} from '@app-types/NavigationType'
import {debounce} from 'lodash'

export const navigationRef = createNavigationContainerRef<RootStackParamList>()

export const navigate = <RouteName extends keyof RootStackParamList>(
  name: RouteName,
  params?: RootStackParamList[RouteName]
): void => {
  if (navigationRef.isReady()) {
    navigationRef.navigate(name as string, params as never)
  }
}

export const navigateAndSimpleReset = debounce(
  (name: string, index = 0): void => {
    if (navigationRef.isReady()) {
      navigationRef.dispatch(
        CommonActions.reset({
          index,
          routes: [{name}],
        })
      )
    }
  },
  100
)

export const navigateAndCheckAuth = (name: string, params?: never): void => {
  if (
    !store.getState()?.auth?.user ||
    !AppDataSingleton.getInstance().getToken()
  ) {
    navigate(ScreensName.SIGN_IN)
  } else {
    navigate(name as never, params as never)
  }
}

export const getCurrentScreenName = () => navigationRef?.getCurrentRoute()?.name

export const getPrevScreenName = () => {
  const navState = navigationRef.getRootState()
  const index = navState?.index
  const route = navState?.routes?.[index - 1]

  if (route?.name === ScreensName.MAIN) {
    const state = route?.state
    const history = state?.history
    const routes = state?.routes
    const historyLength = history?.length
    if (state && historyLength && routes?.length) {
      const currentRouteKey = (history?.[historyLength - 1] as any)?.key
      const findItem = routes.find(
        (item: any) => currentRouteKey && currentRouteKey === item?.key
      )
      return findItem?.name ?? ScreensName.HOME_SCREEN
    } else {
      return ScreensName.HOME_SCREEN
    }
  }

  return route?.name ?? ScreensName.HOME_SCREEN
}

export const resetScreen = (
  name: string,
  params: Record<string, unknown> | undefined = undefined
): void => {
  if (navigationRef.isReady()) {
    const currentScreen = getCurrentScreenName()
    if (currentScreen === name) {
      navigationRef?.dispatch(StackActions.replace(name, params))
    } else {
      navigate(name as never, params as never)
    }
  }
}

export const resetScreenToMain = (
  name: string,
  params: Record<string, unknown> | undefined = undefined
): void => {
  navigationRef?.dispatch(
    CommonActions.reset({
      index: 1,
      routes: [
        {name: ScreensName.MAIN},
        {
          name: name,
          params: params,
        },
      ],
    })
  )
}
