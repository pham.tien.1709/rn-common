import React from 'react'
import {createNativeStackNavigator} from '@react-navigation/native-stack'
import {RootStackParamList} from '@app-types/NavigationType'
import ScreensName from '@constant/screensName'
import Introduction from '@screens/Introduction/Introduction'

const {Screen, Navigator} = createNativeStackNavigator<RootStackParamList>()

export default function LanguageNavigator() {

  return (
    <Navigator
      initialRouteName={ScreensName.APP_INTRO}
      screenOptions={{
        animation: 'slide_from_right',
        headerShown: false,
      }}
    >
      <Screen name={ScreensName.APP_INTRO} component={Introduction} />
    </Navigator>
  )
}
