import React from 'react'
import {createNativeStackNavigator} from '@react-navigation/native-stack'
import {enableScreens} from 'react-native-screens'
import {
  MainTabParamsDefault,
  RootStackParamList,
} from '@app-types/NavigationType'
import ScreensName from '@constant/screensName'
import MainTabNavigation from './MainTabNavigation'
import {NavigationBar} from '@ui-kit/components'
import Introduction from '@screens/Introduction/Introduction'
import {useTranslation} from '@i18n/useTranslation'
import WebViewContent from '@screens/WebViewContent'
import Login from '@screens/Login/Login'
import DesignSystemNavigator from '@ui-kit/Demo'
import {TxKeyPath} from '@i18n/i18n'
import ComingSoonScreen from '@screens/ComingSoon/ComingSoonScreen'

const {Screen, Navigator, Group} =
  createNativeStackNavigator<RootStackParamList>()

enableScreens()

const initialRouteName = () => ScreensName.MAIN

export default function MainNavigator() {
  const {translate} = useTranslation()

  return (
    <Navigator
      initialRouteName={initialRouteName()}
      screenOptions={{
        headerShown: false,
        animation: 'slide_from_right',
      }}
    >
      <Group
        screenOptions={{
          animation: 'slide_from_bottom',
        }}
      >
        <Screen name={ScreensName.SIGN_IN} component={Login} />
      </Group>
      {/* Screen With gestureEnabled = false */}
      <Group
        screenOptions={{
          gestureEnabled: false,
          header: ({route: {params}}) => {
            const title = (params as MainTabParamsDefault)?.title
            return <NavigationBar title={translate(title as TxKeyPath)} />
          },
        }}
      >
        <Screen
          name={ScreensName.MAIN}
          component={MainTabNavigation}
          options={{animation: 'fade'}}
        />
        <Screen name={ScreensName.APP_INTRO} component={Introduction} />
      </Group>

      {/* Screen With Navigation */}
      <Group
        screenOptions={{
          headerShown: true,
          header: props => <NavigationBar {...props} />,
        }}
      >
        <Screen
          options={{title: ''}}
          name={ScreensName.COMING_SOON}
          component={ComingSoonScreen}
        />
      </Group>
      {/* WITHOUT NAVIGATION */}
      <Screen name={ScreensName.WEB_VIEW_CONTENT} component={WebViewContent} />
      <Screen
        name={ScreensName.DESIGN_SYSTEM}
        component={DesignSystemNavigator}
      />
    </Navigator>
  )
}
