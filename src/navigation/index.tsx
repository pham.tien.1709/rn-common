import React, {useEffect, useState} from 'react'
import {NavigationContainer} from '@react-navigation/native'
import SplashScreen from 'react-native-splash-screen'

import {navigationRef} from './NavigationAction'
import MainNavigator from './MainStackNavigation'
import {colors} from '@ui-kit/theme'
import {StatusBar} from '@ui-kit/components'
import AppDataSingleton from '@constant/singletons/appData.singleton'
import {addEventListener} from '@react-native-community/netinfo'

const AppNavigation: React.FC = () => {
  const [, onCheckNetwork] = useState(true)

  useEffect(() => {
    SplashScreen.hide()
    const unsubscribe = addEventListener(netInfoEventListener)
    return () => {
      unsubscribe()
    }
  }, [])

  const netInfoEventListener = async (state: any) => {
    if (AppDataSingleton.getInstance().getIsOnline() !== state.isConnected) {
      AppDataSingleton.getInstance().setIsOnline(
        state.isConnected === null ? false : state.isConnected
      )
      if (state.isConnected) {
        onCheckNetwork(state.isConnected === null ? false : state.isConnected)
        return
      }
      onCheckNetwork(state.isConnected === null ? false : state.isConnected)
    }
  }

  const onReady = () => {
    //
  }

  return (
    <NavigationContainer onReady={onReady} ref={navigationRef}>
      <StatusBar backgroundColor={colors.white} barStyle="dark-content" />
      <MainNavigator />
    </NavigationContainer>
  )
}

export default AppNavigation
