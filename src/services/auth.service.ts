import {AxiosInstance} from 'axios'

const authUrl = '/api/v1/user/'
class AuthService {
  constructor(private axios: AxiosInstance) {
    this.axios = axios
  }

  login = async (params: unknown): Promise<unknown> => {
    const res = await this.axios.post(`${authUrl}`, params)
    return res.data
  }
}

export default AuthService
