import type {StackScreenProps} from '@react-navigation/stack'
import ScreensName from '@constant/screensName'
import {TxKeyPath} from '@i18n/i18n'
import {IconName} from '@ui-kit/components'
import {NativeStackScreenProps} from '@react-navigation/native-stack'
import {NavigationProp, RouteProp} from '@react-navigation/native'

declare global {
  namespace ReactNavigation {
    interface RootParamList extends RootStackParamList {}
  }
}

export type RootStackParamList = {
  // Main: NavigatorScreenParams<MainTabParamList>
  [key: string]: undefined // all route params = undefined
}

export type ScreenProps<T extends keyof RootStackParamList> =
  NativeStackScreenProps<RootStackParamList, T>

export type RootStackScreenProps<T extends keyof RootStackParamList> =
  StackScreenProps<RootStackParamList, T>

export type StackNavigation =
  NativeStackScreenProps<RootStackParamList>['navigation']

export type StackRoute = NativeStackScreenProps<RootStackParamList>['route']

export type NavigationType<T extends keyof RootStackParamList> = NavigationProp<
  RootStackParamList,
  T
>

export type RouteType<T extends keyof RootStackParamList> = RouteProp<
  RootStackParamList,
  T
>

export type MainTabParamsDefault = {
  icon: IconName
  title: TxKeyPath
}

export type MainTabParamList = {
  [ScreensName.HOME_SCREEN]?: MainTabParamsDefault
  [ScreensName.SETTINGS]?: MainTabParamsDefault
  [ScreensName.ACCOUNT]?: MainTabParamsDefault
  [ScreensName.LIST]?: MainTabParamsDefault
}

export type AuthStackParamList = {
  [ScreensName.SIGN_IN]: undefined
  [ScreensName.MAIN]: undefined
}

export type AuthStackRouteProps = RouteProp<
  AuthStackParamList,
  ScreensName.SIGN_IN
>

