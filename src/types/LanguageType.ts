import {FontFamily} from '@ui-kit/types'

export type LanguageType = 'en' | 'km' | 'zh'

export type LanguageItemType = {
  id: number
  label: string
  value: LanguageType
  fontFamily: FontFamily
}
