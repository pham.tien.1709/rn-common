export type ApiUserRequestType = {
  type: string
  username: string
  fullName: string
  language?: string
  prefix?: string | undefined
}
export type ApiUserDataResponseType = {
  id: number
  username: string
  avatar: string
  phoneNumber: string
  email: string
  gender: number
  birthday: string
  status: number
  address: string
  userType: number
  isRemoved: boolean
  createdAt: string
  updatedAt: string
  title: string
}