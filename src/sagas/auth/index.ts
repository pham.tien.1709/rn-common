import {takeEvery} from 'redux-saga/effects'
import {sagaActions} from '../actions'

// eslint-disable-next-line require-yield
function* loginRequest() {
  try {
  } catch (error) {
    /* empty */
  }
}

export default function* rootSaga() {
  yield takeEvery(sagaActions.LOGIN_REQUEST, loginRequest)
}
