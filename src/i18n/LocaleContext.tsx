/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable react/destructuring-assignment */
import React, {ReactNode, createContext, useCallback} from 'react'
import I18n from 'i18n-js'
import {translate} from './translate'
import {useMMKVString} from 'react-native-mmkv'
import storage, {STORAGE_KEY} from '@utils/storage'
import {LanguageType} from '@app-types/LanguageType'

type TypeLocaleContext = {
  localeProvider: LanguageType
  translate: typeof translate
  changeLocale: (locale: LanguageType) => void
  t: (key: string, option?: object) => void
}
const LocaleContext = createContext<TypeLocaleContext>({
  localeProvider: 'km',
  translate,
  changeLocale: () => {},
  t: () => {},
})

export const LocaleStorage = {
  set: (value: LanguageType) => storage.set(STORAGE_KEY.LOCALE, value),
  get: (): LanguageType =>
    (storage.getString(STORAGE_KEY.LOCALE) as LanguageType) || 'km',
}

export function LocaleContextProvider(props: {children: ReactNode}) {
  const [locale = 'km', changeLocale] = useMMKVString(STORAGE_KEY.LOCALE) as [
    LanguageType,
    (lang: LanguageType) => void,
  ]

  I18n.locale = locale

  const onChangeLocale = (lang: LanguageType) => {
    LocaleStorage.set(lang)
    I18n.locale = lang
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    changeLocale(lang)
  }

  const t = useCallback(
    (key: string, option?: object) => {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
      I18n.locale = locale
      return I18n.t(
        key,
        typeof option === 'object'
          ? // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
            {...option, locale}
          : // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
            {locale}
      )
    },
    [locale]
  )

  return (
    <LocaleContext.Provider
      value={{
        ...I18n,
        localeProvider: locale,
        translate,
        changeLocale: onChangeLocale,
        t,
      }}
    >
      {props.children}
    </LocaleContext.Provider>
  )
}

export default LocaleContext
