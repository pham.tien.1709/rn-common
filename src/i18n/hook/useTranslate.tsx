// import {useAppSelector} from '@store/index'

import {useTranslation} from '@i18n/useTranslation'
import {TxKeyPath} from '../i18n'

import i18n from 'i18n-js'
import {translate} from '@i18n/translate'

export const useTranslate = (
  key: TxKeyPath,
  txOptions?: i18n.TranslateOptions
) => {
  const {localeProvider} = useTranslation()
  if (typeof txOptions === 'object') {
    return translate(key, {
      ...txOptions,
      locale: localeProvider,
      defaultValue: key,
    })
  }
  return translate(key, {locale: localeProvider, defaultValue: key})
}
