/* eslint-disable @typescript-eslint/no-redeclare */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable no-use-before-define */
import {I18nManager} from 'react-native'
import I18n from 'i18n-js'

// if English isn't your default language, move Translations to the appropriate language file.
import en from './en/common.json'
// import vi from './kh/common.json'
// type Translations = keyof typeof en
// i18n.fallbacks = true
/**
 * we need always include "*-US" for some valid language
 * codes because when you change the system language,
 * the language code is the suffixed with "-US". i.e. if a device is set to English ("en"),
 * if you change to another language and then return to English language code is now "en-US".
 */
// i18n.translations = {en, 'en-US': en, cn, kh}

export const {isRTL} = I18nManager

export {en}

I18nManager.allowRTL(isRTL)
I18nManager.forceRTL(isRTL)

I18n.locale = 'km'

const setupI18n = (data: {[key: string]: object}) => {
  I18n.fallbacks = true
  I18n.translations = data
}

setupI18n({
  en,
  'en-US': en,
})

// via: https://stackoverflow.com/a/65333050
export type TxKeyPath = RecursiveKeyOf<typeof en>
type RecursiveKeyOf<TObj extends object> = {
  [TKey in keyof TObj & (string | number)]: RecursiveKeyOfHandleValue<
    TObj[TKey],
    `${TKey}`
  >
}[keyof TObj & (string | number)]

type RecursiveKeyOfInner<TObj extends object> = {
  [TKey in keyof TObj & (string | number)]: RecursiveKeyOfHandleValue<
    TObj[TKey],
    `['${TKey}']` | `.${TKey}`
  >
}[keyof TObj & (string | number)]

type RecursiveKeyOfHandleValue<
  TValue,
  Text extends string
> = TValue extends any[]
  ? Text
  : TValue extends object
  ? Text | `${Text}${RecursiveKeyOfInner<TValue>}`
  : Text
