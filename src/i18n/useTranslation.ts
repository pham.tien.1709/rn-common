import {useContext} from 'react'
import LocaleContext from './LocaleContext'

// eslint-disable-next-line import/prefer-default-export
export const useTranslation = () => useContext(LocaleContext)
