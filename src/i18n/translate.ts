import i18n from 'i18n-js'
import {TxKeyPath} from './i18n'

export const translate = (key: TxKeyPath, options?: i18n.TranslateOptions) =>
  i18n.t(key, options)
