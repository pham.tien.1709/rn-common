import {Platform, Alert} from 'react-native'
import {
  PERMISSIONS,
  request,
  check,
  RESULTS,
  openSettings,
  Permission,
} from 'react-native-permissions'

import {IS_IOS, OS_VERSION} from '@ui-kit/constant'

type showQuestionAlert = {
  message?: string
  yesText?: string
  noText?: string
  onYesPress?: () => void
  onNoPress?: () => void
  delayTime?: number | undefined
  title?: string | undefined
}
export function showAlert(
  message: string | undefined,
  title: string | undefined,
  buttons = [
    {
      text: 'Cancel',
      onPress: () => {},
    },
  ],
  delayTime = 250,
  cancelable = true
): void {
  setTimeout(() => {
    Alert.alert(title || '', message, buttons, {cancelable})
  }, delayTime)
}
export function showQuestionAlert({
  message,
  yesText = 'Ok',
  noText = 'Close',
  onYesPress = () => {},
  onNoPress = () => {},
  delayTime = 250,
  title = '',
}: showQuestionAlert) {
  const buttons = [
    {
      text: noText,
      onPress: onNoPress,
    },
    {
      text: yesText,
      onPress: onYesPress,
    },
  ]
  showAlert(message, title, buttons, delayTime)
}
export function showAlertForRequestPermission({
  message,
  yesText,
  noText,
  title = '',
}: showQuestionAlert) {
  const onYesPress = () => {
    openSettings()
  }
  const onNoPress = () => {}
  showQuestionAlert({
    message,
    yesText: yesText ?? 'Open Settings',
    noText: noText ?? 'Close',
    onYesPress,
    onNoPress,
    delayTime: 250,
    title,
  })
}

/*
  permission type
*/
export const PERMISSION_CHECK = {
  CAMERA: Platform.select({
    android: PERMISSIONS.ANDROID.CAMERA,
    ios: PERMISSIONS.IOS.CAMERA,
  }),
  PHOTO: Platform.select({
    android:
      (Platform.Version as number) < 33
        ? PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE
        : PERMISSIONS.ANDROID.READ_MEDIA_IMAGES,
    ios: PERMISSIONS.IOS.PHOTO_LIBRARY,
  }),
  LOCATION: Platform.select({
    android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
    ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
  }),
}

// only check permission
export const checkPermission = async (permission: Permission) => {
  const resCheck = await check(permission)
  if (resCheck === RESULTS.GRANTED) {
    return true
  }
  if (Platform.OS === 'ios') {
    if (resCheck === RESULTS.LIMITED) {
      return true
    }
  }
  return false
}

// only request permission
export const requestPermission = async (permission: Permission) => {
  const resRequest = await request(permission)
  if (resRequest === RESULTS.GRANTED) {
    return true
  }
  showAlertForRequestPermission({
    title: 'Cho phép truy cập quyền',
    message: 'Không thể truy cập, vui lòng cấp quyền để sử dụng',
  })
  return false
}

// check and request
export const checkAndRequestPermission = async (
  permission: Permission
  // message = 'Không thể truy cập, vui lòng cấp quyền để sử dụng',
) => {
  const resCheck = await check(permission)
  if (resCheck === RESULTS.GRANTED) {
    return true
  }
  const resRequest = await request(permission)
  if (resRequest === RESULTS.GRANTED) {
    return true
  }
  return false
}

export const PERMISSION_REJECT_IMAGE = 'PERMISSION_REJECT_IMAGE'

const GALLERY_PERMITS = IS_IOS
  ? PERMISSIONS.IOS.PHOTO_LIBRARY
  : OS_VERSION < 13
    ? PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE
    : PERMISSIONS.ANDROID.READ_MEDIA_IMAGES

export const checkGalleryPermissions = () =>
  request(GALLERY_PERMITS)
    .then(result => {
      switch (result) {
        case RESULTS.UNAVAILABLE:
          return false
        case RESULTS.GRANTED:
        case RESULTS.LIMITED:
          return true
        default:
          return false
      }
    })
    .catch(() => false)
