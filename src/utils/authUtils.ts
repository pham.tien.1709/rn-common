import {REGEX} from '@constant/constant/constants'
import * as yup from 'yup'


// validate login screen
export const loginValidationSchema = yup.object().shape({
  username: yup
    .string()
    .required('global.phoneIsRequired')
    .matches(REGEX.VALIDATE_PHONE, {
      message: 'global.phoneIsNotInTheCorrectFormat',
      excludeEmptyString: true,
    }),
  password: yup
    .string()
    .required('global.passwordRequired')
    .min(6, ({min}: {min: number}) =>
      'global.passwordIncorrectFormat'.replace('xxx', min.toString())
    ),
})
