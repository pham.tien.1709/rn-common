import {Linking} from 'react-native'

export const openLink = (link: string) => {
  try {
    Linking.openURL(link)
  } catch (error) {
    //
  }
}
