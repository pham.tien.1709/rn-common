import AppDataSingleton from '@constant/singletons/appData.singleton'
import dayjs from 'dayjs'
import NetInfo from '@react-native-community/netinfo'

export function hasPhoneNumber(text: any) {
  return !!text && !isNaN(text)
}

export function replaceAll(str: string, match: string, replacement: string) {
  const escapeRegExp = (string: string) =>
    string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&')
  return str.replace(new RegExp(escapeRegExp(match), 'g'), () => replacement)
}

export const formatHtmlOutput = (html: string) => {
  if (!html) return null
  return `
    <html>
      <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
          body {
            margin: 0 !important;
            padding: 0 !important;
          }
        </style>
      </head>
      <body>
        ${html}
      <script>
          var images = document.querySelectorAll("img");
          images.forEach(function(img){
            if(img.clientWidth > 50) {
              img.styles.cssText ="width: 100%!important; object-fit: cover; max-width: 100%!important; height: auto!important; max-height: auto!important;"
            };
          })
      </script>
      </body>
    </html>
  `
}

export const checkInternet = async () => {
  // eslint-disable-next-line import/no-named-as-default-member
  const response = await NetInfo.fetch()
  return response.isConnected
}

class Format {
  updateTimePick = (value?: string) => {
    if (!value) return
    const data = dayjs(value)
    const date = data.format('ddd')
    const day = data.get('D')
    const month = data.format('MMM')
    const year = data.format('YYYY')
    return `${date} ${day} (${month} ${year})`
  }

  convertToString = (value?: string) => {
    if (typeof value === 'string') return value
    return ''
  }

  convertCurrency = (value?: number) => {
    if (!value) return 0
    return new Intl.NumberFormat().format(value).replaceAll('.', ',')
  }

  checkSingleFileSize = (size?: number, maxSize = 0) => {
    if (!size) return false
    if (size > maxSize * 1048576) return false
    return true
  }

  capitalizeFirstLetter = (str?: string) => {
    if (!str) return ''
    return str.length ? str.charAt(0).toUpperCase() + str.slice(1) : str
  }

  formatDateFromISO8601 = (str?: string, type = 'DD.MM.YYYY') => {
    if (!str) return ''
    const dqw = str.split('T')
    return dayjs(dqw[0]).format(type)
  }

  formatCurrencyInput = (str?: string) => {
    if (!str) return ''
    return str.replace(/[^0-9]/g, '')
  }

  sliceString = (str?: string, indexEnd = 15) => {
    if (!str) return ''
    if (str.length <= 15) return str
    return `${str.slice(0, indexEnd)} ...`
  }

  formatDollar = (number = 0, isHaveSpace = false) => {
    const p = number.toFixed(2).split('.')
    return `$${isHaveSpace ? ' ' : ''}${p[0]
      .split('')
      .reverse()
      .reduce(
        (acc, num, i) => num + (num !== '-' && i && !(i % 3) ? ',' : '') + acc,
        ''
      )}.${p[1]}`
  }
}

export const _format = new Format()

export const formatWhiteSpaceString = (value: string): string =>
  value.toString().trim().replace(/\s\s+/g, ' ')

export const checkAppStateRequestApi = (nextAppState: string) => {
  if (
    !AppDataSingleton.getInstance().getAppState() ||
    nextAppState === 'background'
  ) {
    AppDataSingleton.getInstance().setAppState(nextAppState)
  }
  if (
    nextAppState === 'active' &&
    AppDataSingleton.getInstance().getAppState() !== nextAppState &&
    AppDataSingleton.getInstance().getAppState() === 'background'
  ) {
    AppDataSingleton.getInstance().setAppState(nextAppState)
    return true
  }
  return false
}
