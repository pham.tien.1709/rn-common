/* eslint-disable no-console */
import AsyncStorage from '@react-native-async-storage/async-storage'
import {MMKV} from 'react-native-mmkv'

const storage = new MMKV()

// TODO: Remove `hasMigratedFromAsyncStorage` after a while (when everyone has migrated)
export const hasMigratedFromAsyncStorage = storage.getBoolean(
  'hasMigratedFromAsyncStorage'
)

// TODO: Remove `hasMigratedFromAsyncStorage` after a while (when everyone has migrated)
export async function migrateFromAsyncStorage(): Promise<void> {
  console.log('Migrating from AsyncStorage -> MMKV...')
  const start = global.performance.now()

  const keys = await AsyncStorage.getAllKeys()

  for (const key of keys) {
    try {
      const value = await AsyncStorage.getItem(key)

      if (value != null) {
        if (['true', 'false'].includes(value)) {
          storage.set(key, value === 'true')
        } else {
          storage.set(key, value)
        }

        AsyncStorage.removeItem(key)
      }
    } catch (error) {
      console.error(
        `Failed to migrate key "${key}" from AsyncStorage to MMKV!`,
        error
      )
      throw error
    }
  }

  storage.set('hasMigratedFromAsyncStorage', true)

  const end = global.performance.now()
  console.log(`Migrated from AsyncStorage -> MMKV in ${end - start}ms!`)
}

function setObject(key: string, value: any) {
  return storage.set(key, JSON.stringify(value))
}

/**
 * Get the string value for the given `key`, or `undefined` if it does not exist.
 *
 * @default undefined
 */
function getObject(key: string): any {
  try {
    if (storage.contains(key)) {
      const value = storage.getString(key)
      if (value) return JSON.parse(value)
    }
  } catch (e) {}
  return undefined
}

export default Object.assign(storage, {
  setObject,
  getObject,
})

export const STORAGE_KEY = {
  USER_INFO: 'USER_INFO',
  USER_TOKEN_INFO: 'USER_TOKEN_INFO',
  INSTALL_APP: 'INSTALL_APP',
  LOCALE: 'language',
}
