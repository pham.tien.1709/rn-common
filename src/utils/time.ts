import dayjs from 'dayjs'

export const getTimeSecondsRemaining = (
  timeStart?: string,
  timeEnd?: string
): number => {
  return dayjs(timeEnd).unix() - dayjs(timeStart).unix()
}
