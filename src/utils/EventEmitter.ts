/* eslint-disable import/no-named-as-default-member */
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import {EventSubscription} from 'react-native'
import _EventEmitter from 'react-native/Libraries/vendor/emitter/EventEmitter'

export enum EMIT_ACTION {
  GLOBAL_ALERT = 'GLOBAL_ALERT',
  EXAMPLE = 'EXAMPLE',
}
export type AlertType =
  | 'info'
  | 'warn'
  | 'error'
  | 'custom'
  | 'success'
  | 'warning'

export type EmitterCallbackType = {
  title?: string
  message?: string
  type?: AlertType
  showPopup?: boolean
  refreshedCarListStatus?: string
  isHasTwoButton?: boolean
  leftButtonContent?: string
  rightButtonContent?: string
  mainButtonContent?: string
  isEdit?: boolean
  isLiked?: boolean
  onTap?: () => void
  carId?: number
  isShowBanner?: boolean
  isMuted?: boolean
  accessory?: any
}

class Emitter {
  event = new _EventEmitter()

  addListener = <T extends keyof typeof EMIT_ACTION>(
    key: T,
    callback: (res: EmitterCallbackType) => void
  ): EventSubscription => this.event.addListener(key, callback)

  removeListener = () => {
    this.event.removeAllListeners()
  }

  emit = <T extends keyof typeof EMIT_ACTION>(
    key: T,
    data: EmitterCallbackType
  ) => {
    this.event.emit(key, data)
  }
}
const event = new Emitter()
export default event
