import {Platform} from 'react-native'

export const generateAssetFontCss = ({
  fontFileName,
  extension = 'ttf',
}: {
  fontFileName: string
  extension?: string
}) => {
  const fileUri = Platform.select({
    ios: `${fontFileName}.${extension}`,
    android: `file:///android_asset/fonts/${fontFileName}.${extension}`,
  })

  return `@font-face {
      font-family: '${fontFileName}';
      src: local('${fontFileName}'), url('${fileUri}') ;
  }`
}

export const LANGUAGE_FONT_MAP: Record<string, string> = {
  en: 'NotoSans-Regular',
  vn: 'NotoSans-Regular',
  kh: 'KantumruyPro-Regular',
  cn: 'NotoSans-Regular',
}

export enum TYPE_POST_MESSAGE {
  UPDATE_HEIGHT = 'update-height',
  URL = 'url',
}

export const injectScript = `
  window.ReactNativeWebView.postMessage(JSON.stringify({ height:0, type:'update-height' }));
  (function () {
    window.onclick = function(e) {
      e.preventDefault();
      window.ReactNativeWebView.postMessage(JSON.stringify({url: e.target.href, type:'url'}));
      e.stopPropagation()
    }
  }());
`

export const renderHtml = (fontName: string, html?: string) => `
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta content="width=width, initial-scale=1, maximum-scale=1" name="viewport" />
  </head>
  <style type="text/css">
    ${generateAssetFontCss({
      fontFileName: fontName,
      extension: 'ttf',
    })}
    body {
      font-family: ${fontName};
      font-size: 11px;
      -webkit-touch-callout: none;
      -webkit-user-select: none;
      -khtml-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
    }
  </style>
  <body>
    ${html ?? ''}
    <script>
      setTimeout(function() {
        var images = document.querySelectorAll("img"); images.forEach(function(img){
          if( img.clientWidth > 100) {
            img.styles.cssText ="width: 100%!important; object-fit: cover; max-width: 100%!important; height: auto!important; max-height: auto!important;"
          };
        })
      }, 1)
    </script>
  </body>
</html>
`
