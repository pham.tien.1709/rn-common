export const formatWhiteSpaceString = (value: string): string =>
  value.toString().trim().replace(/\s\s+/g, ' ')

export const formatNumber = (number: number, decimals: number = 1): string => {
  const suffixes: string[] = ['', 'K', 'M', 'B', 'T']
  const threshold: number = 1000

  // Handle negative numbers correctly
  const sign: string = Math.sign(number) < 0 ? '-' : ''
  let absNumber: number = Math.abs(number)

  // Determine the appropriate suffix based on magnitude
  let suffixIndex: number = 0
  while (absNumber >= threshold) {
    absNumber /= threshold
    suffixIndex++
  }

  // Round to specified decimals using toFixed()
  const formattedNumber: string = absNumber.toFixed(decimals)

  // Return the formatted string with sign, number, and suffix
  return `${sign}${formattedNumber}${suffixes[suffixIndex]}`
}

export const formatBidderName = (name: string): string => {
  // const nameArr = name.split(' ')
  // if (nameArr.length > 1) {
  //   return `${nameArr[0]}****`
  // }
  return `${name.slice(0, 1)}*****`
}
