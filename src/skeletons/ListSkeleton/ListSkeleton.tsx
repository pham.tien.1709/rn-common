import React from 'react'
import {colors, defaultColor, space} from '@ui-kit/theme'
import {StyleSheet, View} from 'react-native'
import {PlaceholderLine} from 'rn-placeholder'

const ListSkeleton = () => (
  <View style={styles.wrapListActivities}>
    {Array.from(Array(5).keys()).map(item => (
      <View key={item} style={styles.wrapItemWithBorderBottom}>
        <PlaceholderLine noMargin width={45} height={25} />
        <PlaceholderLine noMargin width={90} height={25} />
      </View>
    ))}
  </View>
)

const styles = StyleSheet.create({
  wrapListActivities: {
    backgroundColor: colors.white,
    marginHorizontal: space.md,
    marginTop: space['3xs'],
    paddingHorizontal: space.md,
    borderRadius: space['3xs'],
    marginBottom: space.md,
  },
  wrapItemWithBorderBottom: {
    borderBottomWidth: 1,
    borderBottomColor: defaultColor[100],
    paddingVertical: space.md,
    rowGap: space.sm,
  },
})

export default ListSkeleton
