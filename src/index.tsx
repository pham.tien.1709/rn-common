import DynamicConstants from '@constant/constant/dynamic.constants'
import React from 'react'
import AppDev from './AppDev'
import App from './App'

const AppENV: React.FC = () => {
  if (DynamicConstants.NODE_ENV === 'development') return <AppDev />
  return <App />
}

export default AppENV
