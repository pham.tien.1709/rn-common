/* eslint-disable @typescript-eslint/ban-types */
/* eslint-disable @typescript-eslint/no-unused-vars */
import React, {useEffect, useState} from 'react'
import AppNavigation from './navigation'
import AppInit, {initAppData} from '@components/AppInit'
import {migrateFromAsyncStorage} from '@utils/storage'
import {StyleSheet, View} from 'react-native'
import ErrorBoundary from 'react-native-error-boundary'
import ErrorView from '@components/ErrorView'

function Root() {
  const [checkApp, onCheckApp] = useState(false)

  useEffect(() => {
    migrateFromAsyncStorage()
    setupApp()
  }, [])

  const setupApp = async () => {
    try {
      await initAppData()
    } catch (e) {
      //
    } finally {
      onCheckApp(true)
    }
  }

  const CustomFallback = (props: {error: Error; resetError: Function}) => (
    <View style={styles.emptyContainer}>
      <ErrorView />
    </View>
  )

  return checkApp ? (
    <ErrorBoundary FallbackComponent={CustomFallback}>
      <>
        <AppNavigation />
        <AppInit />
      </>
    </ErrorBoundary>
  ) : null
}

const styles = StyleSheet.create({
  emptyContainer: {
    flex: 1,
  },
})
export default Root
