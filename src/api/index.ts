import AuthService from '@app/services/auth.service'
import {ApiClient} from './ApiClient'

type CommonService = {}
export class ApiService {
  public auth: AuthService

  public common: CommonService

  constructor() {
    this.auth = new AuthService(ApiClient)

    this.common = {}
  }
}

const api = new ApiService()

export default api
