import {APP_NAME_REQUEST, AppConstants} from '@constant/constant/constants'
import ApiClientSingleton from '@constant/singletons/apiClient.singleton'
import NetworkingManagerSingleton from '@constant/singletons/networkingManager.singleton'
import {closeAlertPopup, openAlertPopup} from '@ui-kit/common'
import axios, {
  AxiosError,
  InternalAxiosRequestConfig,
  AxiosResponse,
} from 'axios'
import store from '../stores'
import {logout} from '@stores/slices/auth/slice'
import {setTotalNotifyOfNews} from '@stores/slices/notification/slice'
import {navigateAndSimpleReset} from '@navigation/NavigationAction'
import ScreensName from '@constant/screensName'
import {setApiVersion} from '@utils/apiClientUtils'
import {setTotalItemsInCart} from '@stores/slices/cart/slice'
import AppDataSingleton from '@constant/singletons/appData.singleton'

const baseURL = AppConstants.API_URL
const {dispatch} = store

const ApiClient = axios.create({
  baseURL,
  timeout: 30000,
})

const ERROR_CODE_CHECK_401 = [
  'token_has_expired',
  'token_is_revoked',
  'user_not_found',
  'your_account_is_disabled',
  'jwt_invalid',
  'refresh_token_invalid',
  'refresh_token_has_expired',
  'token_is_out_dated',
]

ApiClient.interceptors.request.use(
  async (config: InternalAxiosRequestConfig) => {
    const headers: any =
      await ApiClientSingleton.getInstance().getApiClientHeader(
        APP_NAME_REQUEST
      )
    const apiVersion = await setApiVersion(config.method, config.url)
    delete config.headers.Authorization
    config.headers = {
      ...config.headers,
      ...headers,
    }
    if (apiVersion) {
      config.headers['x-carz-api-version'] = apiVersion
    } else {
      delete config.headers['x-carz-api-version']
    }
    config.headers['accept-language'] =
      AppDataSingleton.getInstance().getAppLanguage()

    return config
  },

  (error: Error) => Promise.reject(error)
)

ApiClient.interceptors.response.use(
  (response: any) => {
    NetworkingManagerSingleton.getInstance().addRequest({
      response,
      error: null,
      id: new Date().getMilliseconds(),
    })
    return response
  },
  (error: AxiosError<AxiosResponse>) => {
    NetworkingManagerSingleton.getInstance().addRequest({
      response: null,
      error,
      id: new Date().getMilliseconds(),
    })
    if (
      !ApiClientSingleton.getInstance().getError401() &&
      error.response?.status === 401 &&
      ERROR_CODE_CHECK_401.includes((error.response?.data as any)?.code)
    ) {
      ApiClientSingleton.getInstance().setError401(true)
      closeAlertPopup()
      openAlertPopup({
        status: 'warning',
        message: (error.response?.data as any)?.message,
      })
      setTimeout(() => {
        dispatch(logout())
        dispatch(setTotalNotifyOfNews(0))
        dispatch(setTotalItemsInCart(0))
        navigateAndSimpleReset(ScreensName.SIGN_IN, 0)
      }, 400)
    }
    return Promise.reject(error)
  }
)

export {ApiClient}
