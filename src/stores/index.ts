import {configureStore} from '@reduxjs/toolkit'
import createSagaMiddleware from 'redux-saga'
import reducers from './slices'
import {TypedUseSelectorHook, shallowEqual, useSelector} from 'react-redux'
// import saga from '../sagas/auth'

export type RootState = ReturnType<typeof reducers>

// const sagaMiddleware = createSagaMiddleware()

const store = configureStore({
  reducer: reducers,
})

// sagaMiddleware.run(saga)

const useShallowEqualSelector = (
  selector: (state: any) => any,
  equalityFn: any
) => useSelector(selector, equalityFn || shallowEqual)

export const useAppSelector: TypedUseSelectorHook<RootState> =
  useShallowEqualSelector

export default store
