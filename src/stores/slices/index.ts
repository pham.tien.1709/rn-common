import {combineReducers} from '@reduxjs/toolkit'
// IMPORT REDUCER
import themeReducer from './theme/slice'
import authReducer from './auth/slice'

const rootReducer = combineReducers({
  theme: themeReducer,
  auth: authReducer,
})

export default rootReducer
