import ApiClientSingleton from '@constant/singletons/apiClient.singleton'
import AppDataSingleton from '@constant/singletons/appData.singleton'
import {createSlice, PayloadAction} from '@reduxjs/toolkit'
import {userInfo} from '@utils/authUtils'

export interface AuthStates {
  tokenInfo: any
  user: any
}

const defaultState: AuthStates = {
  tokenInfo: null,
  user: null,
}

const authSlice = createSlice({
  name: 'auth',
  initialState: defaultState,
  reducers: {
    login: (state: AuthStates, action: PayloadAction<any>): AuthStates => {
      AppDataSingleton.getInstance().setToken(
        action.payload?.tokenInfo?.jwt || ''
      )
      ApiClientSingleton.getInstance().setError401(false)
      userInfo.set(action.payload)
      return {
        ...state,
        ...action.payload,
      }
    },
    logout: (state: AuthStates): AuthStates => {
      AppDataSingleton.getInstance().setToken(undefined)
      userInfo.reset()
      return {
        ...state,
        tokenInfo: null,
        user: null,
      }
    },
    updateProfile: (
      state: AuthStates,
      action: PayloadAction<any>
    ): AuthStates => {
      const newState = {
        ...state,
        user: action.payload,
      }
      userInfo.set({
        ...userInfo.get(),
        user: action.payload,
      })
      return newState
    },
  },
})

export const {
  login,
  logout,
  updateProfile,
} = authSlice.actions
export default authSlice.reducer
