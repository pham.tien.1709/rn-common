import * as colors from '@ui-kit/theme/color'
import {ColorType, ThemeName} from '@ui-kit/types'
import {PayloadAction, createSlice} from '@reduxjs/toolkit'

export interface ThemeStates extends ColorType {
  theme: ThemeName
  firstTimeUseApp: boolean
}

const defaultState: ThemeStates = {
  theme: 'light',
  firstTimeUseApp: true,
  ...colors.colors,
}

const themeSlice = createSlice({
  name: 'theme',
  initialState: defaultState,
  reducers: {
    toggleTheme: (state: ThemeStates): ThemeStates => {
      const theme: ThemeName = state.theme === 'light' ? 'dark' : 'light'
      const color = (colors as any)[theme]
      return {
        ...state,
        ...color,
        theme,
      }
    },
    setFirstTimeUseApp: (
      state: ThemeStates,
      action: PayloadAction<boolean>
    ): ThemeStates => ({
      ...state,
      firstTimeUseApp: action.payload,
    }),
  },
})

export const {toggleTheme, setFirstTimeUseApp} = themeSlice.actions
export default themeSlice.reducer
