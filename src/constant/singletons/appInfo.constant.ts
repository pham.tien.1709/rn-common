enum APP_VERSION_CODE {
  ANDROID = 'android_app',
  IOS = 'ios_app',
}

export {APP_VERSION_CODE}
