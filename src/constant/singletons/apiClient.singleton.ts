import {
  getUniqueId,
} from 'react-native-device-info'
import AppDataSingleton from './appData.singleton'

class ApiClientSingleton {
  apiClientHeader: any | undefined

  deviceId: string | undefined

  isError401: boolean | false | undefined

  private static instance: ApiClientSingleton

  private constructor() {}

  public static getInstance(): ApiClientSingleton {
    if (!ApiClientSingleton.instance) {
      ApiClientSingleton.instance = new ApiClientSingleton()
    }
    return ApiClientSingleton.instance
  }

  async getDeviceId() {
    if (!this.deviceId) {
      let deviceId = await getUniqueId()
      deviceId = deviceId
      this.deviceId = deviceId
      return deviceId
    }
    return this.deviceId
  }

  async getApiClientHeader(app: string) {
    if (!this.apiClientHeader) {

      this.apiClientHeader = {}
    }

    if (AppDataSingleton.getInstance().getToken()) {
      this.apiClientHeader.Authorization = `Bearer ${AppDataSingleton.getInstance().getToken()}`
    } else if (this.apiClientHeader?.Authorization) {
      delete this.apiClientHeader?.Authorization
    }
    return this.apiClientHeader
  }

  setError401(value: boolean) {
    this.isError401 = value
  }

  getError401() {
    return this.isError401
  }
}

export default ApiClientSingleton
