class AppDataSingleton {
  token: string | undefined

  private static instance: AppDataSingleton

  isOnline: boolean | true | undefined

  appState: string | undefined

  private constructor() {}

  public static getInstance(): AppDataSingleton {
    if (!AppDataSingleton.instance) {
      AppDataSingleton.instance = new AppDataSingleton()
    }
    return AppDataSingleton.instance
  }
  setToken(token?: string) {
    this.token = token
  }
  getToken() {
    return this.token
  }

  setIsOnline(value: boolean) {
    this.isOnline = value
  }

  getIsOnline() {
    if (this.isOnline === undefined) return false
    return this.isOnline
  }

  setAppState(appState: string) {
    this.appState = appState
  }

  getAppState() {
    return this.appState
  }
}

export default AppDataSingleton
