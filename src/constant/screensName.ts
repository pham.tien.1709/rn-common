enum ScreensName {
  MAIN = 'Main',
  AUTH = 'Auth',
  HOME_SCREEN = 'HomeScreen',
  ACCOUNT = 'Account',
  SIGN_IN = 'SignIn',
  APP_INTRO = 'AppIntro',
  DESIGN_SYSTEM = 'DesignSystem',
  LIST = 'LIST',
  COMING_SOON = 'ComingSoon',
  WEB_VIEW_CONTENT = 'WebViewContent',
}
export default ScreensName
