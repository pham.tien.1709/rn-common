import {LanguageItemType} from '@app-types/LanguageType'

export const LANGUAGE_SETTING: LanguageItemType[] = [
  {
    id: 3,
    label: 'EN',
    value: 'en',
    fontFamily: 'Inter'
  },
  {
    id: 2,
    label: '中文',
    value: 'zh',
    fontFamily: 'Noto Sans SC'
  }
]
