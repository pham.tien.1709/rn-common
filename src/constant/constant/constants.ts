import {Platform} from 'react-native'
import DynamicConstants from './dynamic.constants'

export const AppConstants = {
  ENVIRONMENT: DynamicConstants.NODE_ENV,
  API_URL: DynamicConstants.API_URL ?? ''
}

export const Language = {
  VIETNAM: 'vi',
  ENGLAND: 'en'
}
export const PREFIX_PHONE_NUMBER = [
  {
    id: 1,
    country: 'vn',
    countryName: 'Vietnam',
    value: '+84',
    title: ''
  },
  {
    id: 2,
    country: 'en',
    countryName: 'US',
    value: '+1',
    title: ''
  }
]

export const IS_ANDROID = Platform.OS === 'android'
export const IS_IOS = Platform.OS === 'ios'

export const REGEX = {
  EXCLUDE_SPECIAL_CHARACTERS:
    /[`~!@#%^&*()_|+\-=?;:'",.<>{}[\]\\/\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff]]/gi,
  EXCLUDE_SPECIAL_CHARACTERS_AND_NUMBER:
    /[0-9`~!@#$%^&*()_|+\-=?;:'",.<>{}[\]\\/\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff]]/gi,
  ONLY_NUMBER: /[^0-9]/g,
  VALIDATE_PHONE_MAIL:
    /^[0-9]{4,13}$|^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$/, // REGEX email by RFC
  VALIDATE_PHONE: /^[0-9]{8,15}$/
}
export const FORMAT_DATE = 'YYYY-MM-DD'

export const TELL_PHONE = {
  ios: 'tel://',
  android: 'tel:'
}

export const TAKE = 10
export const HIT_SLOP = {
  top: 10,
  right: 10,
  bottom: 10,
  left: 10
}
