enum GENDER {
  MALE = 1,
  FEMALE = 2,
  UNKNOWN = 3,
}

enum TYPE {
  CLIENT = 1,
  AGENT = 2,
}

enum STATUS {
  ENABLE = 1,
  DISABLE = 2,
}

export const USER_CONSTANT = {
  GENDER,
  TYPE,
  STATUS,
}
