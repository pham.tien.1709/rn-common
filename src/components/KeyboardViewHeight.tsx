import React from 'react'
import {useReanimatedKeyboardAnimation} from 'react-native-keyboard-controller'
import Animated, {useAnimatedStyle} from 'react-native-reanimated'

const KeyboardViewHeight = () => {
  const {height} = useReanimatedKeyboardAnimation()
  const heightStyle = useAnimatedStyle(() => {
    return {height: -height.value}
  }, [])

  return <Animated.View style={heightStyle} />
}

export default KeyboardViewHeight
