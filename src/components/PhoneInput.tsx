import {PREFIX_PHONE_NUMBER} from '@constant/constant/constants'
import {orange} from '@constant/themes/colors'
import {translate} from '@i18n/translate'
import {
  BottomSheet,
  BottomSheetRef,
  Icon,
  Input,
} from '@ui-kit/components'
import {IS_ANDROID} from '@ui-kit/constant'
import {iconSize, primary, space, trueGray} from '@ui-kit/theme'
import React, {forwardRef, useImperativeHandle, useRef, useState} from 'react'
import {Keyboard, Pressable, StyleSheet, Text, View} from 'react-native'

interface IPhoneInputProps {
  error?: string
  onChangeText?: (t: string) => void
}

interface IPhoneInputRef {
  phoneNumber: string
  prefix: string
}

const LeftItemPhoneNumber = ({
  onPress,
  value,
}: {
  onPress: () => void
  value: string
}) => {
  return (
    <Pressable style={styles.leftInput} onPress={onPress}>
      <Icon icon="IcoMoon" size={iconSize.sm} />
      <Text style={styles.mr8}>{value}</Text>
      <Icon icon="arrow-down" size={iconSize.sm} />
    </Pressable>
  )
}

const PhoneInput: React.ForwardRefRenderFunction<
  IPhoneInputRef,
  IPhoneInputProps
> = ({error, onChangeText}, ref) => {
  const sheetRef = useRef<BottomSheetRef>(null)
  const [prefix, setPrefix] = useState<string>(PREFIX_PHONE_NUMBER[0].value)
  const [phoneNumber, setPhoneNumber] = useState<string>('')

  useImperativeHandle(ref, () => ({
    phoneNumber,
    prefix,
  }))

  const onSelectCountryCode = () => {
    if (sheetRef.current) {
      Keyboard.dismiss()
      sheetRef.current.present()
    }
  }

  const onSelectPrefix = (value: string) => {
    setPrefix(value)
    if (sheetRef.current) {
      sheetRef.current.close()
    }
  }

  const onChangeTextInput = (text: string) => {
    setPhoneNumber(text)
    onChangeText?.(text)
  }

  return (
    <View>
      <Input
        LeftItem={() => (
          <LeftItemPhoneNumber onPress={onSelectCountryCode} value={prefix} />
        )}
        placeholder={translate('login.phoneNumber')}
        style={styles.input}
        error={error}
        inputMode="numeric"
        value={phoneNumber}
        onChangeText={onChangeTextInput}
      />
      <BottomSheet
        ref={sheetRef}
        title={translate('login.selectCountryCode')}
        titleProps={{style: styles.title}}
        enableDynamicSizing
      >
        {PREFIX_PHONE_NUMBER.map((phoneNumber, idx) => {
          return (
            <Pressable
              key={phoneNumber.id}
              style={[
                styles.countryCodeField,
                idx < PREFIX_PHONE_NUMBER.length - 1
                  ? styles.bottomLine
                  : {
                      paddingBottom: IS_ANDROID ? space['4xl'] : space['3xs'],
                    },
              ]}
              onPress={() => {
                onSelectPrefix(phoneNumber.value)
              }}
            >
              <Icon icon="mobile" size={iconSize.sm} color={trueGray[300]} />
              <Text
                style={[
                  {marginLeft: space['3xs']},
                  prefix === phoneNumber.value ? {color: orange[400]} : {},
                ]}
              >
                {phoneNumber.value}
              </Text>
            </Pressable>
          )
        })}
      </BottomSheet>
    </View>
  )
}

const styles = StyleSheet.create({
  countryCodeField: {
    flexDirection: 'row',
    marginHorizontal: space.xs,
    paddingHorizontal: space.sm,
    paddingVertical: space.sm,
    alignItems: 'center',
  },
  input: {
    marginTop: space.lg,
    marginBottom: space.xs,
    paddingHorizontal: space.md,
  },
  leftInput: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  mr8: {
    marginHorizontal: space.xs,
  },
  bottomLine: {
    borderBottomColor: trueGray[100],
    borderBottomWidth: 1,
  },
  title: {
    color: primary[800],
  },
})

export default forwardRef(PhoneInput)
