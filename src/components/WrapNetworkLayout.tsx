import React, {PropsWithChildren, useEffect, useState} from 'react'

import {View} from 'react-native'
import {
  NetInfoState,
  addEventListener as addEventNetworkListener,
} from '@react-native-community/netinfo'
import {OverlayLoading} from './OverlayLoading'
import LostConnection from './LostConnection'
import {colors} from '@ui-kit/theme'

export interface IPropsData
  extends PropsWithChildren<{
    color?: string
    loading?: boolean
  }> {}

const WrapAppLayout: React.FC<IPropsData> = ({
  color = colors.white,
  children,
  loading,
}) => {
  const [isCheckNetwork, onCheckNetwork] = useState(true)

  const netInfoEventListener = async (state: NetInfoState) => {
    onCheckNetwork(state.isConnected === null ? false : state.isConnected)
  }

  const onReCheckNetwork = async () => {
    onCheckNetwork(true)
  }

  useEffect(() => {
    const unsubscribe = addEventNetworkListener(netInfoEventListener)
    return () => {
      unsubscribe()
    }
  }, [])
  const styleContainer = {flex: 1, backgroundColor: color}
  return (
    <View style={styleContainer}>
      {isCheckNetwork ? (
        <>{children}</>
      ) : (
        <LostConnection onPress={onReCheckNetwork} />
      )}
      {loading && <OverlayLoading />}
    </View>
  )
}

export default React.memo(WrapAppLayout)
