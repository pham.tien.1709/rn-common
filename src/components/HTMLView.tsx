import {
  HTMLIframe,
  iframeModel,
  useHtmlIframeProps,
} from '@native-html/iframe-plugin'
import React, {FC} from 'react'
import {
  StyleProp,
  StyleSheet,
  TextStyle,
  View,
  ViewStyle,
  Linking,
} from 'react-native'
import HTML, {
  CustomRendererProps,
  InternalRendererProps,
  MixedStyleDeclaration,
  TBlock,
  useInternalRenderer,
  defaultHTMLElementModels,
  HTMLContentModel,
} from 'react-native-render-html'
import WebView from 'react-native-webview'
import {Image} from '@ui-kit/components'
import {colors, fontSize, screenSize, space} from '@ui-kit/theme'
import {useGetFonts} from '@ui-kit/hook'
import {TrackingEventName, tracking} from '@utils/TrackingLogSystem'
import ScreensName from '@constant/screensName'
import {IS_ANDROID} from '@ui-kit/constant'

interface Props {
  id?: number
  content: string
  textDefaultStyle?: TextStyle
  containerStyle?: StyleProp<ViewStyle>
  baseStyle?: MixedStyleDeclaration
  contentWidth?: number
  screenName?: string
}

const isHttpsImage = (source: string) => source?.startsWith?.('http')
function CustomImageRenderer(props: InternalRendererProps<any>) {
  const {Renderer, rendererProps} = useInternalRenderer('img', props)
  const {
    source = {},
    contentWidth = 0,
    height,
    width,
    testID,
  } = rendererProps || {}
  const {uri} = source
  const _width = !Number(width) ? contentWidth : Number(width)
  const _height = !Number(height) ? undefined : Number(height)
  if (!isHttpsImage(uri ?? '')) return null

  if (!_height || Number(width) <= contentWidth) {
    return <Renderer {...rendererProps} />
  }
  const contentHeight = (contentWidth / _width) * _height
  return (
    <View key={testID} style={{width: contentWidth, height: contentHeight}}>
      <Image resizeMode="cover" source={{uri}} style={styles.img} />
    </View>
  )
}

const IframeRenderer = function IframeRenderer(
  props: CustomRendererProps<TBlock>
) {
  const iframeProps = useHtmlIframeProps(props)
  return <HTMLIframe {...iframeProps} />
}

const renderersProps = (
  id: number | undefined,
  screenName: string | undefined
) => ({
  img: {
    enableExperimentalPercentWidth: true,
  },
  iframe: {
    scalesPageToFit: true,
    webViewProps: {
      allowsFullscreenVideo: false,
    },
  },
  a: {
    onPress(_event: any, url: any) {
      if (url) {
        if (ScreensName.NOTIFICATION_DETAIL === screenName) {
          tracking.log(TrackingEventName.Click_on_any_url_on_description, {
            metadata: {
              notification_id: id,
              url: url,
            },
          })
        }
        Linking.openURL(url)
      }
    },
  },
})

const customHTMLElementModels = {
  iframe: iframeModel,
  video: iframeModel,
  img: defaultHTMLElementModels.img.extend({
    contentModel: IS_ANDROID ? HTMLContentModel.block : HTMLContentModel.mixed,
  }),
}

const renderers = {
  img: CustomImageRenderer,
  iframe: IframeRenderer,
}

const HTMLView: FC<Props> = ({
  id,
  screenName,
  content,
  baseStyle: baseOptionStyle,
  contentWidth = screenSize.width - space.md * 2,
}) => {
  const {fontFamily} = useGetFonts()

  const baseStyle = {
    color: colors.black,
    fontSize: fontSize.md,
    fontFamily: fontFamily,
  }

  const styles = {...baseStyle, ...baseOptionStyle}

  return (
    <View renderToHardwareTextureAndroid={true}>
      <HTML
        source={{html: content}}
        baseStyle={style}
        enableExperimentalMarginCollapsing
        renderersProps={renderersProps(id, screenName)}
        renderers={renderers}
        WebView={WebView}
        customHTMLElementModels={customHTMLElementModels}
        tagsStyles={{...tagsStyles}}
        contentWidth={contentWidth}
      />
    </View>
  )
}

const tagsStyles = {
  p: {lineHeight: space.lg},
  a: {lineHeight: space.lg},
}
const styles = StyleSheet.create({
  img: {
    width: '100%',
    height: '100%',
  },
})
export default HTMLView
