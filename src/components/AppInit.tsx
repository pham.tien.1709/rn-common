import React, {useEffect} from 'react'
import ApiClientSingleton from '@constant/singletons/apiClient.singleton'
import AppDataSingleton from '@constant/singletons/appData.singleton'
import {AppState} from 'react-native'
import NetInfo from '@react-native-community/netinfo'
import {checkAppStateRequestApi} from '@utils/utils'
import storage, {STORAGE_KEY} from '@utils/storage'
import {userInfo} from '@utils/authUtils'

const checkUser = async () => {
  const oldUser = storage.getString(STORAGE_KEY.INSTALL_APP)
  if (oldUser) {
    const info = userInfo.get()
    if (info && info.tokenInfo) {
      //
    }
  }
}

export const initAppData = async () => {
  await checkUser()
}

// Component
const AppInit = () => {
  useEffect(() => {
    const handleAppStateChange = async (nextAppState: string) => {
      if (checkAppStateRequestApi(nextAppState)) {
        //
      }
    }
    const myListener = AppState.addEventListener('change', handleAppStateChange)
    ApiClientSingleton.getInstance().setError401(false)

    // eslint-disable-next-line import/no-named-as-default-member
    const unsubscribe = NetInfo.addEventListener(netInfoEventListener)
    return () => {
      myListener.remove()
      unsubscribe()
    }
  }, [])

  const netInfoEventListener = async (state: any) => {
    if (AppDataSingleton.getInstance().getIsOnline() !== state.isConnected) {
      AppDataSingleton.getInstance().setIsOnline(
        state.isConnected === null ? false : state.isConnected
      )
      if (state.isConnected) {
        await initAppData()
        return
      }
    }
  }

  return <></>
}

export default AppInit
