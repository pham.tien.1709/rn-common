import {Text} from '@ui-kit/components'
import {defaultColor, fontSize, primary, space} from '@ui-kit/theme'
import React from 'react'
import {StyleSheet, View} from 'react-native'

interface IHeaderWithBorderBottomPropsType {
  title: string
}

const HeaderWithBorderBottom = ({title}: IHeaderWithBorderBottomPropsType) => {
  return (
    <View style={styles.container}>
      <View style={styles.titleContainer}>
        <Text children={title} style={styles.title} />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: defaultColor[50],
    paddingLeft: space.md,
  },
  titleContainer: {
    alignSelf: 'flex-start',
    borderBottomWidth: 2,
    borderBottomColor: primary[800],
    paddingVertical: space.md,
  },
  title: {
    fontSize: fontSize.lg,
    fontWeight: '700',
  },
})

export default HeaderWithBorderBottom
