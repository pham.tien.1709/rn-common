import {Pressable, StyleSheet, View} from 'react-native'
import React from 'react'
import {Container, Icon, Image, Text} from '@ui-kit/components'
import {Images} from '@constant/themes/images'
import {useTranslate} from '@i18n/hook/useTranslate'
import {
  avatarSize,
  defaultColor,
  fontSize,
  insets,
  primary,
  space,
} from '@ui-kit/theme'

const LostConnection = ({onPress}: {onPress: () => void}) => {
  const txtLostNetworkTitle = useTranslate('lostConnection.lostConnection')
  const txtLostNetworkContent = useTranslate(
    'lostConnection.lostConnectionContent'
  )

  return (
    <Container style={styles.container}>
      <View style={styles.body}>
        <Image
          source={Images.ico_lose_network}
          style={styles.iconLostNetwork}
        />
        <Text style={styles.txtLostNetworkTitle}>{txtLostNetworkTitle}</Text>
        <Text style={styles.txtLostNetworkContent}>
          {txtLostNetworkContent}
        </Text>
      </View>
      <View style={styles.header}>
        <Pressable
          style={{paddingLeft: space.md, paddingBottom: space.md}}
          onPress={onPress}
        >
          <Icon icon="close-outline" size={space.md} />
        </Pressable>
      </View>
    </Container>
  )
}

export default LostConnection

const styles = StyleSheet.create({
  container: {flex: 1},
  header: {
    top: insets.top + 20,
    right: space.xl,
    alignItems: 'flex-end',
    position: 'absolute',
  },
  body: {alignItems: 'center', justifyContent: 'center', flex: 1},
  iconLostNetwork: {
    width: avatarSize.xl,
    height: avatarSize.xl,
    resizeMode: 'contain',
  },
  txtLostNetworkTitle: {
    color: primary[800],
    fontWeight: '700',
    fontSize: fontSize.lg,
  },
  txtLostNetworkContent: {
    marginTop: space.xs,
    color: defaultColor[600],
    fontWeight: '400',
    fontSize: fontSize.md,
    textAlign: 'center',
  },
})
