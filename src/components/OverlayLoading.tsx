import React, {ReactNode} from 'react'
import {StyleSheet, View, ViewProps} from 'react-native'
import Animated, {FadeInUp, FadeOutDown} from 'react-native-reanimated'
import {screenSize} from '@ui-kit/theme'
import {closePortal, openPortal} from '@ui-kit/common'

export interface OverlayLoadingProps {
  contentStyle?: ViewProps['style']
  children?: ReactNode
}

export const OverlayLoading = (props: OverlayLoadingProps): any => {
  const {children} = props
  return (
    <Animated.View
      exiting={FadeOutDown.duration(200).springify()}
      style={[StyleSheet.absoluteFill, styles.container]}
    >
      <Animated.View
        key="OverlayLoading"
        entering={FadeInUp.springify()}
        style={[styles.contentStyle, props?.contentStyle]}
      >
        {children}
      </Animated.View>
    </Animated.View>
  )
}

const show = () => {
  openPortal(
    <View key="OverlayLoadingWrapper" style={styles.portal}>
      <OverlayLoading />
    </View>
  )
}
const hide = (callback?: () => void) => closePortal(callback)

export default Object.assign(OverlayLoading, {
  show,
  hide
})

const backgroundColor = 'rgba(0,0,0,.2)'

const styles = StyleSheet.create({
  container: {
    backgroundColor,
    justifyContent: 'center',
    alignItems: 'center'
  },
  contentStyle: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  portal: {
    flex: 1,
    width: screenSize.width,
    height: '100%'
  }
})
