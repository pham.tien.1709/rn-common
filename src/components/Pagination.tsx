import React, {FC} from 'react'
import {HStack} from '@ui-kit/components'
import {View, StyleSheet, ViewProps} from 'react-native'
import {colors, space} from '@ui-kit/theme'

export interface PaginationProps extends ViewProps {
  activeIndex?: number
  backgroundColor?: string
  length?: number
  size?: number
  renderItem?: (item: any, index: number, backgroundColor: string) => any
}

const Pagination: FC<PaginationProps> = props => {
  const {
    activeIndex,
    length,
    backgroundColor = colors.white,
    renderItem,
  } = props

  const renderDot = (_: any, index: number) => {
    const active = activeIndex === index
    return (
      <View
        key={`pagination-${index}`}
        style={[styles.dot, active ? styles.dotActive : {}, {backgroundColor}]}
      />
    )
  }

  return (
    <HStack style={[styles.pagination, props?.style]} gap={space.xs}>
      {Array(length)
        .fill(0)
        .map((item, index: number) =>
          renderItem ? renderItem(item, index, backgroundColor) : renderDot
        )}
    </HStack>
  )
}
export default Pagination

const styles = StyleSheet.create({
  dot: {
    width: space['3xs'],
    height: space['3xs'],
    borderRadius: space.xs,
    opacity: 0.3,
  },
  dotActive: {
    width: space.sm,
    opacity: 1,
  },
  pagination: {
    justifyContent: 'center',
  },
})
