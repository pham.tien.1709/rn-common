import React, {ReactNode, useEffect, useState} from 'react'
import {Linking, Pressable, StyleSheet, View, ViewProps} from 'react-native'
import {colors, iconSize, screenSize, space} from '@ui-kit/theme'
import {closeBannersPopup, openBannersPopup} from '@ui-kit/common'
import {Icon, Image} from '@ui-kit/components'
import {ApiPopupType} from '@app-types/PopupType'
import Carousel from 'react-native-reanimated-carousel'
import {useSelector} from 'react-redux'
import {RootState} from '@stores'
import {SCREEN_WIDTH} from '@constant/themes/variables'
// import {isIPhoneWithDynamicIsland, isIPhoneWithMonobrow} from '@utils/statusBar'
import Animated, {
  interpolate,
  useAnimatedStyle,
  useSharedValue,
} from 'react-native-reanimated'
import Pagination from './Pagination'
import {PaginationItem} from './BannerSlider'
import {tracking} from '@utils/TrackingLogSystem'

export interface PopupViewProps {
  contentStyle?: ViewProps['style']
  children?: ReactNode
  popups: ApiPopupType[]
  onClosePopup: () => void
}

const SwipeItem = ({
  data,
  userId,
}: {
  data: ApiPopupType
  userId: number | undefined
}) => {
  const {image, link, id} = data

  const onClickItem = () => {
    tracking.trackingPopupClick({
      popup_id: id,
      group_id: id,
      user_id: userId,
    })
    if (link) {
      Linking.openURL(link)
    }
  }

  return (
    <Pressable style={styles.content} onPress={onClickItem}>
      <Image source={image} style={styles.banner} resizeMode="stretch" />
    </Pressable>
  )
}

export const PopupView = ({
  popups,
  onClosePopup,
}: PopupViewProps): ReactNode => {
  const [activeIndex, setActiveIndex] = useState(0)
  const user = useSelector((state: RootState) => state.auth.user)

  // const renderDot = (_: any, index: number) => {
  //   const active = activeIndex === index
  //   const dotStyle = {opacity: active ? 1 : 0.3}
  //   return (
  //     <View style={[styles.dot, dotStyle]} key={`dot-${index.toString()}`} />
  //   )
  // }

  const progressValue = useSharedValue<number>(0)
  const animationStyle = useAnimatedStyle(() => {
    const itemGap = interpolate(
      progressValue.value,
      [-3, -2, -1, 0, 1, 2, 3],
      [-30, -15, 0, 0, 0, 15, 30]
    )

    const inputRange = [-1, 0, 1]
    const outputRange = [-space['3xs'], 0, space['3xs']]

    const translateX =
      interpolate(progressValue.value, inputRange, outputRange) +
      (screenSize.width - space.md * 2) / 2 -
      itemGap

    return {
      transform: [{translateX}],
    }
  }, [])

  const {length} = popups

  useEffect(() => {
    tracking.trackingPopupImpression({
      popup_id: popups[activeIndex].id,
      group_id: popups[activeIndex].id,
      user_id: user?.id,
    })
  }, [activeIndex])

  return (
    <View key="PopupView" style={styles.container}>
      <Pressable
        style={styles.contentStyle}
        onPress={() => {
          onClosePopup()
          hide()
        }}
      >
        <Pressable
          onPress={() => {
            onClosePopup()
            hide()
          }}
          style={styles.btnClose}
        >
          <Icon icon="close-outline" color={colors.white} size={iconSize.sm} />
        </Pressable>
        <Carousel
          data={popups}
          width={screenSize.width}
          pagingEnabled
          renderItem={({item}) => (
            <SwipeItem data={item} userId={user?.id} key={item.id} />
          )}
          onSnapToItem={setActiveIndex}
          onProgressChange={(_, absoluteProgress) =>
            (progressValue.value = absoluteProgress)
          }
          modeConfig={{
            parallaxScrollingScale: 1,
            parallaxScrollingOffset: 0,
            parallaxAdjacentItemScale: 0.8,
          }}
          enabled
          scrollAnimationDuration={500}
          style={styles.carousel}
        />
        {/* <HStack style={styles.pagination} gap={space.xs}>
          {popups.map(renderDot)}
        </HStack> */}
        <Animated.View style={[animationStyle, styles.paginationAnimated]}>
          <Pagination
            activeIndex={activeIndex}
            length={length}
            renderItem={(_, index) => (
              <PaginationItem
                animValue={progressValue}
                index={index}
                key={`pagination-${index}`}
                length={length}
                backgroundColor={colors.white}
              />
            )}
            style={styles.paginationItemAnimated}
          />
        </Animated.View>
      </Pressable>
    </View>
  )
}

const show = (data: ApiPopupType[] | undefined, onClosePopup: () => void) => {
  if (data) {
    openBannersPopup(
      <View style={styles.portal}>
        <PopupView popups={data} onClosePopup={onClosePopup} />
      </View>
    )
  }
}
const hide = () => {
  closeBannersPopup()
}

export default Object.assign(PopupView, {
  show,
  hide,
})

const backgroundColor = 'rgba(0,0,0,.3)'
const marginTopPopup = 135

const styles = StyleSheet.create({
  container: {
    backgroundColor,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  contentStyle: {
    marginTop: marginTopPopup,
    flex: 1,
    width: '100%',
  },
  portal: {
    flex: 1,
    backgroundColor,
    width: screenSize.width,
    height: '100%',
  },
  banner: {
    width: SCREEN_WIDTH - space.md * 2 + 4,
    height: ((SCREEN_WIDTH - space.md * 2) / 4) * 5,
    borderRadius: 8,
    overflow: 'hidden',
    alignSelf: 'center',
  },
  content: {},
  paginationAnimated: {
    position: 'absolute',
    justifyContent: 'center',
    top:
      ((SCREEN_WIDTH - space.md * 2) / 4) * 5 +
      iconSize.sm +
      space.xs * 2 +
      space.xs,
  },
  paginationItemAnimated: {
    justifyContent: 'center',
  },
  // pagination: {
  //   position: 'absolute',
  //   justifyContent: 'center',
  //   top: SCREEN_WIDTH - space.md * 4 + space['4xl'],
  // },
  carousel: {
    // height: screenSize.half_height,
    // alignItems: 'center',
  },
  // dot: {
  //   backgroundColor: colors.white,
  //   width: space.xs,
  //   height: space.xs,
  //   borderRadius: space.md,
  // },
  btnClose: {
    alignSelf: 'flex-end',
    paddingHorizontal: space.lg,
    paddingVertical: space.xs,
  },
})
