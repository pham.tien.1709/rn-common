import React, {useEffect} from 'react'
import {StyleSheet, View, TextInput} from 'react-native'
import Animated, {
  useSharedValue,
  useAnimatedStyle,
  useAnimatedGestureHandler,
  useAnimatedProps,
  runOnJS,
} from 'react-native-reanimated'
import {PanGestureHandler} from 'react-native-gesture-handler'
import {Colors} from '@constant/themes/variables'

export interface RangeSliderProps {
  sliderWidth: number
  min: number
  max: number
  step: number
  positionMin: number
  positionMax: number
  onValueChange: (values: {min: number; max: number}) => void
  type: 'price' | 'mile' | 'year' | 'litre'
}

const RangeSlider: React.FC<RangeSliderProps> = ({
  sliderWidth,
  min,
  max,
  step,
  positionMin,
  positionMax,
  type,
  onValueChange,
}) => {
  const position = useSharedValue(
    (sliderWidth / (max - min)) * (positionMin - min)
  )
  const position2 = useSharedValue(
    (sliderWidth / (max - min)) * (positionMax - min)
  )

  const opacity = useSharedValue(1)
  const opacity2 = useSharedValue(1)
  const zIndex = useSharedValue(0)
  const zIndex2 = useSharedValue(0)
  useEffect(() => {
    if (positionMin === min) {
      position.value = 0
    } else {
      position.value = (sliderWidth / (max - min)) * (positionMin - min)
    }
    if (positionMax === max) {
      position2.value = sliderWidth
    } else {
      position2.value = (sliderWidth / (max - min)) * (positionMax - min)
    }
  }, [positionMin, positionMax])

  const gestureHandler = useAnimatedGestureHandler({
    onStart: (_, ctx: any) => {
      ctx.startX = position.value
    },
    onActive: (e, ctx) => {
      if (ctx.startX + e.translationX < 0) {
        position.value = 0
      } else if (ctx.startX + e.translationX > position2.value) {
        position.value = position2.value
        zIndex.value = 1
        zIndex2.value = 0
      } else {
        position.value = ctx.startX + e.translationX
      }
    },
    onEnd: () => {
      runOnJS(onValueChange)({
        min:
          min +
          Math.floor(position.value / (sliderWidth / ((max - min) / step))) *
            step,
        max:
          min +
          Math.floor(position2.value / (sliderWidth / ((max - min) / step))) *
            step,
      })
    },
  })

  const gestureHandler2 = useAnimatedGestureHandler({
    onStart: (_, ctx: any) => {
      ctx.startX = position2.value
    },
    onActive: (e, ctx) => {
      if (ctx.startX + e.translationX > sliderWidth) {
        position2.value = sliderWidth
      } else if (ctx.startX + e.translationX < position.value) {
        position2.value = position.value
        zIndex.value = 0
        zIndex2.value = 1
      } else {
        position2.value = ctx.startX + e.translationX
      }
    },
    onEnd: () => {
      runOnJS(onValueChange)({
        min:
          min +
          Math.floor(position.value / (sliderWidth / ((max - min) / step))) *
            step,
        max:
          min +
          Math.floor(position2.value / (sliderWidth / ((max - min) / step))) *
            step,
      })
    },
  })

  const animatedStyle = useAnimatedStyle(() => ({
    transform: [{translateX: position.value}],
    zIndex: zIndex.value,
  }))

  const animatedStyle2 = useAnimatedStyle(() => ({
    transform: [{translateX: position2.value}],
    zIndex: zIndex2.value,
  }))

  const opacityStyle = useAnimatedStyle(() => ({
    opacity: opacity.value,
  }))

  const opacityStyle2 = useAnimatedStyle(() => ({
    opacity: opacity2.value,
  }))

  const sliderStyle = useAnimatedStyle(() => ({
    transform: [{translateX: position.value}],
    width: position2.value - position.value,
  }))

  const AnimatedTextInput = Animated.createAnimatedComponent(TextInput)
  const minLabelText = useAnimatedProps(() => ({
    text:
      type !== 'year'
        ? `${type === 'price' ? '$' : ''}${(
            min +
            Math.floor(position.value / (sliderWidth / ((max - min) / step))) *
              step
          )
            .toString()
            .replace(/\B(?=(\d{3})+(?!\d))/g, ',')} ${
            type === 'mile' ? 'Km' : type === 'litre' ? 'L' : ''
          }`
        : (
            min +
            Math.floor(position.value / (sliderWidth / ((max - min) / step))) *
              step
          ).toString(),
  }))
  const maxLabelText = useAnimatedProps(() => ({
    text:
      type !== 'year'
        ? `${type === 'price' ? '$' : ''}${(
            min +
            Math.floor(position2.value / (sliderWidth / ((max - min) / step))) *
              step
          )
            .toString()
            .replace(/\B(?=(\d{3})+(?!\d))/g, ',')} ${
            type === 'mile' ? 'Km' : type === 'litre' ? 'L' : ''
          }`
        : (
            min +
            Math.floor(position2.value / (sliderWidth / ((max - min) / step))) *
              step
          ).toString(),
  }))
  return (
    <View style={[styles.sliderContainer, {width: sliderWidth}]}>
      <View style={[styles.sliderBack, {width: sliderWidth}]} />
      <Animated.View style={[sliderStyle, styles.sliderFront]} />
      <PanGestureHandler onGestureEvent={gestureHandler}>
        <Animated.View style={[animatedStyle, styles.thumb]}>
          <Animated.View style={[opacityStyle, styles.label]}>
            <Animated.View style={styles.triangle} />
            <AnimatedTextInput
              style={styles.labelText}
              animatedProps={minLabelText as any}
              editable={false}
              defaultValue={
                type !== 'year'
                  ? `${type === 'price' ? '$' : ''}${positionMin
                      .toString()
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ',')} ${
                      type === 'mile' ? 'Km' : type === 'litre' ? 'L' : ''
                    }`
                  : positionMin.toString()
              }
            />
          </Animated.View>
        </Animated.View>
      </PanGestureHandler>
      <PanGestureHandler onGestureEvent={gestureHandler2}>
        <Animated.View style={[animatedStyle2, styles.thumb]}>
          <Animated.View style={[opacityStyle2, styles.label]}>
            <Animated.View style={styles.triangle} />
            <AnimatedTextInput
              style={styles.labelText}
              animatedProps={maxLabelText as any}
              editable={false}
              defaultValue={
                type !== 'year'
                  ? `${type === 'price' ? '$' : ''}${positionMax
                      .toString()
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ',')} ${
                      type === 'mile' ? 'Km' : type === 'litre' ? 'L' : ''
                    }`
                  : positionMax.toString()
              }
            />
          </Animated.View>
        </Animated.View>
      </PanGestureHandler>
    </View>
  )
}

export default RangeSlider

const styles = StyleSheet.create({
  sliderContainer: {
    justifyContent: 'center',
    alignSelf: 'center',
  },
  sliderBack: {
    height: 6,
    backgroundColor: Colors.gray_500,
    borderRadius: 20,
  },
  sliderFront: {
    height: 6,
    backgroundColor: Colors.primary,
    borderRadius: 20,
    position: 'absolute',
  },
  thumb: {
    left: -10,
    width: 20,
    height: 20,
    position: 'absolute',
    backgroundColor: Colors.white,
    borderColor: Colors.primary,
    borderWidth: 5,
    borderRadius: 10,
  },
  label: {
    position: 'absolute',
    backgroundColor: Colors.primary,
    borderRadius: 15,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    top: 20,
  },
  labelText: {
    color: Colors.white,
    paddingTop: 4,
    paddingBottom: 8,
    paddingHorizontal: 6,
    width: '100%',
    // fontSize: 14,
    // lineHeight: 22,
    // height: 22,
  },
  triangle: {
    width: 0,
    height: 0,
    top: -5,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderLeftWidth: 5,
    borderRightWidth: 5,
    borderBottomWidth: 5,
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: Colors.primary,
  },
})
