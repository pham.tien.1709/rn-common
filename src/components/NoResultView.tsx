import React from 'react'
import {Images} from '@constant/themes/images'
import {StyleSheet, View} from 'react-native'
import {Image, Text} from '@ui-kit/components'
import {colors} from '@ui-kit/theme'

const NoResultView = ({
  text,
  containerStyle = {},
  ...props
}: {
  text: string
  containerStyle?: any
}) => (
  <View {...props} style={[styles.container, containerStyle]}>
    <Image source={Images.mascotEmpty} style={styles.image} />
    <Text color={colors.black} style={styles.title}>
      {text}
    </Text>
  </View>
)
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 109,
    height: 106,
  },
  title: {
    color: colors.black,
    textAlign: 'center',
  },
})
export default NoResultView
