import React from 'react'
import {Images} from '@constant/themes/images'
import {StyleSheet, View} from 'react-native'
import {Button, Image, Text} from '@ui-kit/components'
import {
  colors,
  defaultColor,
  fontSize,
  primary,
  space,
} from '@ui-kit/theme'
import {translate} from '@i18n/translate'

const ErrorView = (props: any) => {
  const onConfirm = () => {
    if (props?.onConfirm) {
      props?.onConfirm()
      return
    }
  }
  return (
    <View style={styles.container}>
      <Image source={Images.img_system_maintainer} style={styles.image} />
      <Text color={colors.black} style={styles.title}>
        {translate('maintainer.title')}
      </Text>
      <Text color={colors.black} style={styles.description}>
        {translate('maintainer.description')}
      </Text>
      <Button style={styles.button} onPress={onConfirm}>
        {translate('maintainer.goToHome')}
      </Button>
    </View>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 155,
    height: 135,
  },
  description: {
    marginTop: space.xs,
    color: defaultColor[600],
    fontWeight: '400',
    fontSize: fontSize.md,
    textAlign: 'center',
  },
  title: {
    color: primary[800],
    fontWeight: '700',
    fontSize: fontSize.lg,
    marginTop: space.xs,
  },
  button: {
    marginTop: space.md,
  },
})
export default ErrorView
