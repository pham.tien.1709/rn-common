import React from 'react'

import {StyleSheet, TouchableOpacity} from 'react-native'
import {BottomTabBarProps} from '@react-navigation/bottom-tabs'
import {colors, defaultColor, fontSize, insets, space} from '@ui-kit/theme'
import {MainTabParamsDefault} from '@app-types/NavigationType'
import {useTranslation} from '@i18n/useTranslation'
import {HStack, Icon, Text} from '@ui-kit/components'
import {Haptic} from '@ui-kit/common'

export default function MainTabBar({
  state,
  descriptors,
  navigation
}: BottomTabBarProps) {
  const {translate} = useTranslation()
  return (
    <>
      <HStack style={styles.container}>
        {state.routes.map((route, index) => {
          const {options} = descriptors[route.key]
          const params = route.params as MainTabParamsDefault
          const {icon} = params
          const {title} = params

          const isFocused = state.index === index
          const color = isFocused ? colors.primary : defaultColor[400]

          const onLongPress = () => {
            navigation.emit({
              type: 'tabLongPress',
              target: route.key
            })
          }
          const onPress = () => {
            Haptic()
            const event = navigation.emit({
              type: 'tabPress',
              target: route.key,
              canPreventDefault: true
            })
            if (!isFocused && !event.defaultPrevented) {
              navigation.navigate(route.name)
            }
          }

          return (
            <TouchableOpacity
              key={route.name}
              activeOpacity={1}
              accessibilityRole="button"
              accessibilityState={isFocused ? {selected: true} : {}}
              accessibilityLabel={options.tabBarAccessibilityLabel}
              testID={options.tabBarTestID}
              onPress={onPress}
              onLongPress={onLongPress}
              style={styles.tabItem}
            >
              <Icon color={color} icon={icon} />
              <Text
                numberOfLines={1}
                adjustsFontSizeToFit
                fontWeight={isFocused ? '500' : '400'}
                style={[styles.title, {color}]}
              >
                {translate(title)}
              </Text>
            </TouchableOpacity>
          )
        })}
      </HStack>
    </>
  )
}

const styles = StyleSheet.create({
  tabItem: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  container: {
    backgroundColor: colors.white,
    paddingTop: space.xs,
    paddingBottom: insets.bottom,
    borderTopWidth: 1,
    borderColor: defaultColor[100]
    // paddingHorizontal: space.sm,
  },
  title: {
    textAlign: 'center',
    fontSize: fontSize.xs,
    marginTop: space['3xs']
  }
})
