import React, {FC} from 'react'
import {StyleSheet, ViewProps} from 'react-native'
import Animated, {
  Extrapolate,
  SharedValue,
  interpolate,
  useAnimatedStyle,
} from 'react-native-reanimated'
import {colors, defaultColor, screenSize, space} from '@ui-kit/theme'

interface AnimatedPaginationProps extends ViewProps {
  progressValue: SharedValue<number>
  length: number
}

export function AnimatedPagination(props: AnimatedPaginationProps) {
  const {progressValue, length} = props

  const animationStyle = useAnimatedStyle(() => {
    'worklet'
    const {value} = progressValue

    const itemGap = interpolate(
      value,
      [-3, -2, 0, 2, 3],
      [-space.xs * 2, -space.xs, 0, space.xs, space.xs * 2]
    )

    const inputRange = [-2, 1, 0, 1]
    const outputRange = [-PAGINATE_WIDTH, 0, PAGINATE_WIDTH, 0]

    const translateX =
      interpolate(value, inputRange, outputRange) + centerOffset - itemGap

    return {
      transform: [{translateX}],
    }
  }, [length])

  return (
    <Animated.View style={[animationStyle, styles.container, props.style]}>
      {Array(length)
        .fill(0)
        .map((_, index: number) => (
          <PaginationItem
            animValue={progressValue}
            index={index}
            key={`pagination-${index}`}
            length={length}
          />
        ))}
    </Animated.View>
  )
}

export const PaginationItem: FC<{
  index: number
  length: number
  animValue: SharedValue<number>
  isRotate?: boolean
  backgroundColor?: string
  inActiveColor?: string
}> = props => {
  const {
    animValue,
    index,
    length,
    backgroundColor = colors.primary,
    inActiveColor = defaultColor[200],
  } = props

  const animStyle = useAnimatedStyle(() => {
    'worklet'
    const inputRange = [index - 3, index, index + 3]
    const outputRange = [-PAGINATE_WIDTH, 0, PAGINATE_WIDTH]

    const inputRangeOpacity = [index - 1, index, index + 1]
    const outputRangeOpacity = [0, 1, 0]

    return {
      opacity: interpolate(
        animValue?.value,
        inputRangeOpacity,
        outputRangeOpacity,
        Extrapolate.CLAMP
      ),
      transform: [
        {
          translateX: interpolate(
            animValue?.value,
            inputRange,
            outputRange,
            Extrapolate.CLAMP
          ),
        },
      ],
    }
  }, [animValue, index, length])

  const widthAnim = useAnimatedStyle(() => {
    'worklet'
    const inputRange = [index - 1, index, index + 1]
    const outputRange = [PAGINATE_WIDTH, space.sm, PAGINATE_WIDTH]

    const inputScale = [index - 3, index - 2, index, index + 2, index + 3]
    const outputScale = [0, 1, 1, 1, 0]

    const inputOpacity = [index - 3, index - 2, index, index + 2, index + 3]
    const outputOpacity = [0, 1, 1, 1, 0]

    return {
      width: interpolate(
        animValue?.value,
        inputRange,
        outputRange,
        Extrapolate.CLAMP
      ),

      opacity: interpolate(
        animValue?.value,
        inputOpacity,
        outputOpacity,
        Extrapolate.CLAMP
      ),

      transform: [
        {
          scale: interpolate(
            animValue?.value,
            inputScale,
            outputScale,
            Extrapolate.CLAMP
          ),
        },
      ],
    }
  }, [animValue, index, length])

  return (
    <Animated.View
      style={[
        styles.paginationItemView,
        inActiveColor ? {backgroundColor: inActiveColor} : {},
        widthAnim,
      ]}
    >
      <Animated.View
        style={[
          styles.paginationItem,
          animStyle,
          {backgroundColor: backgroundColor},
        ]}
      />
    </Animated.View>
  )
}

export const CONTENT_WIDTH = Math.round(screenSize.width - space.md * 2)
const centerOffset = CONTENT_WIDTH / 2
const PAGINATE_WIDTH = space['3xs']

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    gap: space.xs,
  },
  paginationItemView: {
    backgroundColor: colors.white,
    width: PAGINATE_WIDTH,
    height: PAGINATE_WIDTH,
    borderRadius: screenSize.width,
    overflow: 'hidden',
  },
  paginationItem: {
    borderRadius: screenSize.width,
    flex: 1,
  },
})
