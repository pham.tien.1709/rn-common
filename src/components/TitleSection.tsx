import React from 'react'
import {Pressable, StyleSheet, ViewStyle} from 'react-native'
import {HStack, Text} from '@ui-kit/components'
import {useTranslation} from '@i18n/useTranslation'
import {colors, fontSize, space} from '@ui-kit/theme'
import {TxKeyPath} from '@i18n/i18n'

export type TitleSectionProps = {
  onPress?: () => void
  title?: string
  tx?: TxKeyPath
  style?: ViewStyle
  upperCaseTitle?: boolean
}

export default function TitleSection(props: TitleSectionProps) {
  const {onPress, title, tx, upperCaseTitle} = props
  const {translate} = useTranslation()
  return (
    <HStack gap={space.md} style={[styles.header, props?.style]}>
      <Text style={styles.title}>
        {upperCaseTitle
          ? (tx ? translate(tx) : title)?.toUpperCase()
          : tx
            ? translate(tx)
            : title}
      </Text>
      {typeof onPress === 'function' ? (
        <Pressable onPress={onPress}>
          <Text fontWeight="500" fontSize={'sm'}>
            {translate('home.seeAll')}
          </Text>
        </Pressable>
      ) : null}
    </HStack>
  )
}

const styles = StyleSheet.create({
  header: {
    paddingHorizontal: space.md,
  },
  title: {
    flex: 1,
    fontSize: fontSize.lg,
    color: colors.primary,
    fontWeight: 'bold',
  },
})
