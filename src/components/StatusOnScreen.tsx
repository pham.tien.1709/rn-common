import {Images} from '@constant/themes/images'
import {avatarSize, colors, insets, space} from '@ui-kit/theme'
import React, {useEffect, useState} from 'react'
import {ScrollView, StyleSheet, View, RefreshControl} from 'react-native'
import {checkInternet} from '@utils/utils'
import {translate} from '@i18n/translate'
import OverlayLoading from './OverlayLoading'
import {Button, Image} from '@ui-kit/components'

type IProps = {
  children?: any
  error?: Record<string, any>
  loading?: boolean
  SkeletonView?: React.ReactElement
  onRefresh?: () => void
}

const LostNetwork = ({onRecheckNetwork = () => {}}) => {
  return (
    <ScrollView
      contentContainerStyle={styles.container}
      refreshControl={
        <RefreshControl
          style={styles.refreshing}
          refreshing={false}
          onRefresh={onRecheckNetwork}
        />
      }
    >
      <View style={styles.body}>
        <Image source={Images.app_logo} style={styles.iconLostNetwork} />
        <Button style={styles.buttonReload} onPress={onRecheckNetwork}>
          {translate('common.reload')}
        </Button>
      </View>
    </ScrollView>
  )
}

const StatusOnScreen: React.FC<IProps> = ({
  children,
  loading,
  SkeletonView,
  onRefresh
}) => {
  const [isOnline, changeOnline] = useState(false)
  const [isChecked, onChecked] = useState(false)

  useEffect(() => {
    checkNetwork()
  }, [])

  const checkNetwork = async () => {
    const isOnline = await checkInternet()
    changeOnline(isOnline ?? false)
    if (!isChecked) {
      onChecked(true)
      return
    }
    if (onRefresh && isOnline) {
      onRefresh()
    }
  }

  const onRefreshCheckNetwork = async () => {
    OverlayLoading.show()
    checkNetwork()
  }

  if (!isChecked) return null
  if (!isOnline) {
    return <LostNetwork onRecheckNetwork={onRefreshCheckNetwork} />
  }

  if (loading && SkeletonView) {
    return SkeletonView
  }
  return children
}

export default StatusOnScreen

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    flexGrow: 1
  },
  body: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    gap: space.xs
  },
  iconLostNetwork: {
    width: avatarSize.xl,
    height: avatarSize.xl,
    resizeMode: 'contain'
  },
  refreshing: {
    top: insets.top
  },
  buttonReload: {
    paddingHorizontal: space.md,
    marginTop: space.xs
  }
})
