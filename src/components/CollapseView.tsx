import {TxKeyPath} from '@i18n/i18n'
import React, {useEffect, useState} from 'react'
import {Button, Icon, Text} from '@ui-kit/components'
import {
  colors,
  defaultColor,
  fontSize,
  iconSize,
  primary,
  space,
} from '@ui-kit/theme'
import {StyleSheet, View} from 'react-native'
import {translate} from '@i18n/translate'
import {SCREEN_WIDTH} from '@constant/themes/variables'

export interface CollapseViewProps {
  name: string
  content?: string
  children?: React.ReactNode
  txContent?: TxKeyPath
  onOpenCollapseView?: () => void
  borderTitle?: boolean
  borderBottom?: boolean
}

const CollapseView: React.FC<CollapseViewProps> = ({
  name,
  content,
  children,
  txContent,
  onOpenCollapseView,
  borderTitle = true,
  borderBottom = true,
}) => {
  const [isOpen, setIsOpen] = useState(false)
  const open = () => {
    setIsOpen(!isOpen)
  }

  useEffect(() => {
    if (isOpen) {
      onOpenCollapseView?.()
    }
  }, [isOpen])

  const renderContent = () =>
    // FIXME: change color follow config
    children ||
    (content ? (
      <Text style={borderTitle ? styles.contentTextBorder : styles.contentText}>
        {content}
      </Text>
    ) : (
      <Text
        style={borderTitle ? styles.contentTextBorder : styles.contentText}
        children={txContent ? translate(txContent) : ''}
      />
    ))
  return (
    <View
      style={[
        styles.container,
        !borderTitle
          ? !borderBottom
            ? styles.noBorderBottomContainer
            : styles.noBorderContainer
          : {},
      ]}
    >
      <Button
        style={[
          borderTitle ? styles.buttonBorder : styles.button,
          borderTitle
            ? {
                marginVertical: space.md,
              }
            : styles.noMarginBottom,
        ]}
        onPress={open}
      >
        <Text
          style={
            borderTitle
              ? styles.name
              : isOpen
                ? styles.name
                : styles.nameInactive
          }
        >
          {name}
        </Text>
        <View style={styles.icon}>
          <Icon icon={isOpen ? 'up' : 'down'} size={15} color={primary[800]} />
        </View>
      </Button>
      {isOpen && renderContent()}
      {/* {renderContent()} */}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {backgroundColor: colors.white},
  noBorderBottomContainer: {
    paddingBottom: space.md,
    marginHorizontal: space.md,
  },
  noBorderContainer: {
    borderBottomColor: defaultColor[300],
    borderBottomWidth: 1,
    paddingBottom: space.md,
    marginHorizontal: space.md,
  },
  contentText: {
    marginHorizontal: space.md,
    marginTop: space.md,
    textAlign: 'justify',
  },
  contentTextBorder: {
    marginHorizontal: space['2xl'],
    marginTop: space.xl,
  },
  buttonBorder: {
    marginHorizontal: space.md,
    paddingHorizontal: space.md,
    borderColor: defaultColor[300],
    borderWidth: 1,
    borderRadius: space['3xs'],
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: colors.white,
  },
  button: {
    // marginHorizontal: space.md,
    paddingHorizontal: space.md,
    borderRadius: space['3xs'],
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: colors.white,
  },
  nameInactive: {
    fontSize: fontSize.md,
    fontWeight: '400',
    color: primary[800],
    maxWidth: SCREEN_WIDTH - space['5xl'] * 2,
    flex: 1,
  },
  name: {
    fontSize: fontSize.md,
    fontWeight: '700',
    color: primary[800],
    maxWidth: SCREEN_WIDTH - space['5xl'] * 2,
    flex: 1,
  },
  noMarginBottom: {
    marginTop: space.md,
    marginBottom: 0,
  },
  icon: {
    width: iconSize.xs,
    height: iconSize.xs,
    alignItems: 'flex-end',
  },
})

export default CollapseView
