import React from 'react'
import {StyleSheet} from 'react-native'
import {Container, Text} from '@ui-kit/components'
import {colors, space} from '@ui-kit/theme'
import {translate} from '@i18n/translate'

const ComingSoonScreen = () => (
  <Container style={[styles.container]}>
    <Text style={styles.content}>{translate('common.comingSoon')}</Text>
  </Container>
)
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    color: colors.black,
    textAlign: 'center',
    marginBottom: space.sm,
    fontWeight: '700',
  },
})
export default ComingSoonScreen
