import React, {useRef} from 'react'
import {StyleSheet} from 'react-native'
import {
  MainTabParamList,
  RootStackParamList,
  RootStackScreenProps,
} from '@app-types/NavigationType'
import {BottomTabScreenProps} from '@react-navigation/bottom-tabs'
import {CompositeScreenProps, useScrollToTop} from '@react-navigation/native'
import {Container, Text} from '@ui-kit/components'
import StatusOnScreen from '@components/StatusOnScreen'
import { insets } from '@ui-kit/theme'

export type HomeTabScreenProps<T extends keyof MainTabParamList> =
  CompositeScreenProps<
    BottomTabScreenProps<MainTabParamList, T>,
    RootStackScreenProps<keyof RootStackParamList>
  >

const Home = () => {
  const listRef = useRef<any>(null)

  useScrollToTop(listRef)

  return (
    <Container isHidedHeader style={styles.container}>
      <StatusOnScreen>
        <Text>Home</Text>
      </StatusOnScreen>
    </Container>
  )
}

export default Home

const styles = StyleSheet.create({
  container: {
    paddingTop: insets.top
  },
})
