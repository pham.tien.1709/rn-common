import {
  colors,
  defaultColor,
  fontSize,
  iconSize,
  insets,
  primary,
  screenSize,
  space,
  warning,
} from '@ui-kit/theme'
import {StyleSheet} from 'react-native'

export const styles = StyleSheet.create({
  list: {
    flex: 1,
    // paddingHorizontal: space.md,
  },
  skeletonContainer: {
    padding: space.md,
    flex: 1,
    backgroundColor: colors.background2,
    zIndex: -999,
  },
  inputView: {
    flex: 1,
    flexDirection: 'row',
  },
  input: {
    borderRadius: space.xs,
    backgroundColor: defaultColor[50],
    flex: 1,
  },
  container: {
    paddingTop: insets.paddingTop,
  },
  itemContainer: {
    marginVertical: space.xs,
  },
  buttonChangeStyle: {
    alignSelf: 'flex-end',
  },
  loadingSkeleton: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    gap: space.sm,
  },
  carSkeleton: {marginBottom: 20},
  gridView: {paddingTop: 0},
  contentContainer: {
    paddingBottom: 60,
    backgroundColor: colors.background2,
    paddingHorizontal: space.md - space.sm / 2,
  },
  emptyContentContainer: {
    flex: 1,
  },
  filterView: {
    paddingBottom: space.sm,
  },
  rowFilterView: {
    paddingHorizontal: space.md,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  txtCarForSale: {
    color: primary[800],
    fontWeight: '700',
    fontSize: fontSize.lg,
  },
  txtResult: {
    color: defaultColor[400],
    fontWeight: '400',
    fontSize: fontSize.md,
    marginLeft: space.xs,
  },
  btnClearAll: {
    borderRightColor: defaultColor[200],
    borderRightWidth: 1,
    paddingRight: space.sm,
    paddingLeft: space.lg,
    height: space.xl,
    justifyContent: 'center',
  },
  txtClearAll: {
    color: primary[800],
    fontWeight: '500',
    fontSize: fontSize.sm,
  },
  btnFilter: {},
  txtKeyFilter: {},
  txtValueFilter: {},
  btnValue: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: space.xl,
    backgroundColor: defaultColor[50],
    borderRadius: 3,
    paddingLeft: space.md,
    paddingRight: space.xs,
    marginLeft: space.sm,
    alignSelf: 'flex-end',
  },
  txtValueTitle: {
    color: defaultColor[700],
    fontWeight: '400',
    fontSize: fontSize.sm,
  },
  txtValue: {
    color: warning[400],
    fontWeight: '400',
    fontSize: fontSize.sm,
  },
  listValuesContainer: {
    paddingRight: space.lg,
    marginTop: space.sm,
  },
  fieldFilterRow: {
    alignItems: 'flex-start',
    flexDirection: 'row',
    marginTop: space.md,
  },
  btnClose: {
    height: space.xl,
    justifyContent: 'center',
    paddingLeft: space.xs,
  },
  btnClearText: {},
  fieldBrands: {
    marginTop: space.md,
  },
  btnBrand: {
    alignItems: 'center',
  },
  txtBrand: {
    color: colors.black,
    fontWeight: '400',
    fontSize: fontSize.sm,
    width: iconSize['4xl'],
    textAlign: 'center',
    marginTop: space.xs,
  },
  imgBrand: {
    marginHorizontal: space['2xs'],
    minWidth: space['5xl'] * 2,
    height: space['5xl'],
    aspectRatio: 80 / 40,
    resizeMode: 'contain',
  },
  itemGridContainer: {
    marginHorizontal: space.sm / 2,
    marginVertical: space['4xs'],
    flex: 1,
  },
  filterStyle: {
    alignItems: 'flex-end',
    alignSelf: 'flex-end',
  },
  emptyContainer: {
    height: screenSize.height / 2,
  },
  listViewFilter: {
    paddingRight: space['4xl'],
  },
  marginTopCarCard: {marginTop: space.md},
  bannerGrid: {
    marginHorizontal: -space.md + space.sm / 2,
    width: screenSize.width,
    marginVertical: space.xs,
  },
  banner: {
    marginHorizontal: -space.md + space.sm / 2,
    width: screenSize.width,
    marginVertical: space.xs,
  },
})
