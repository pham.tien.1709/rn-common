import React from 'react'
import {styles} from './styles'
import {Text, Container} from '@ui-kit/components'
import StatusOnScreen from '@components/StatusOnScreen'

const ListCars = () => {
  const handleRefresh = () => {}

  return (
    <StatusOnScreen onRefresh={handleRefresh}>
      <Container style={styles.container} isHidedHeader>
        <Text>List</Text>
      </Container>
    </StatusOnScreen>
  )
}

export default ListCars
