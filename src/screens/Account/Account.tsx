import React from 'react'
import {StackNavigation} from '@app-types/NavigationType'
import {useNavigation} from '@react-navigation/native'
import {Button, Container, NavigationBar} from '@ui-kit/components'
import {ScrollView, StyleSheet} from 'react-native'
import GroupMenu from './containers/GroupMenu'
import {colors, defaultColor, space} from '@ui-kit/theme'
import MenuHorizontal from './containers/MenuHorizontal'
import {useTranslate as translate} from '@i18n/hook/useTranslate'
import {useDebouncePress} from '@hooks/useDebouncePress'
import ScreensName from '@constant/screensName'

const Account = () => {
  const {debounce} = useDebouncePress()
  const navigation = useNavigation<StackNavigation>()

  const onLogin = async () => {
    navigation.navigate(ScreensName.SIGN_IN)
  }
  const handlePressItem = React.useCallback(
    async (item: {screen: string; params?: any}) => {
      if (!item.screen) return
      return navigation?.navigate(item.screen, item?.params || {})
    },
    [navigation]
  )

  return (
    <Container style={styles.container} level={2} isHidedHeader>
      <NavigationBar
        hideBack
        style={{backgroundColor: colors.transparent}}
        title={translate('common.account')}
      />
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.content}
      >
        <>
          <MenuHorizontal data={[]} onPressItem={handlePressItem} />
          <GroupMenu data={[]} onPressItem={handlePressItem} />
          <Button style={styles.btn} onPress={() => debounce(onLogin)}>
            {translate('login.login')}
          </Button>
        </>
      </ScrollView>
    </Container>
  )
}

export default Account

const styles = StyleSheet.create({
  content: {paddingHorizontal: space.md, paddingBottom: space.xl},
  btn: {marginTop: space.md},
  container: {
    backgroundColor: defaultColor[50]
  }
})
