import React from 'react'
import {
  Group,
} from '@ui-kit/components'
import ScreensName from '@constant/screensName'

type DevSettingProps = {
  onPressItem: (data: any) => void
}

export function DevSetting({onPressItem}: DevSettingProps) {
  return (
    <Group>
      <Group.Item
        LeftItem={{
          icon: 'rocket',
        }}
        RightItem={{
          icon: 'arrow-right',
        }}
        onPress={() =>
          onPressItem({
            screen: ScreensName.DESIGN_SYSTEM,
            name: undefined,
          })
        }
      >
        Design System
      </Group.Item>
      <Group.Item
        LeftItem={{
          icon: 'rocket',
        }}
        RightItem={{
          icon: 'arrow-right',
        }}
        onPress={() =>
          onPressItem({
            screen: ScreensName.HOME_SCREEN,
            name: undefined,
          })
        }
      >
        Log out
      </Group.Item>
    </Group>
  )
}
