import {Group, Text} from '@ui-kit/components'
import React from 'react'
import {useTranslate as translate} from '@i18n/hook/useTranslate'
import {StyleSheet, View} from 'react-native'
import {colors, space} from '@ui-kit/theme'
import DynamicConstants from '@constant/constant/dynamic.constants'
import {DevSetting} from './DevSetting'

export interface AccountContainerType<T> {
  name: string | T
  detail?: string
  screen: string
  hide?: boolean
  nameNoTranslate?: string
  params?: any
}

interface GroupMenuProps<T> {
  data: Array<AccountContainerType<T>>[]
  onPressItem: (data: AccountContainerType<T>) => void
  showDevTab?: boolean
}

const GroupMenu: React.FC<GroupMenuProps<any>> = ({
  data,
  onPressItem,
  showDevTab = true,
}) => {
  return (
    <View style={styles.list}>
      {data.map((groups = [], i: number) => (
        <Group key={i}>
          {groups.map((item, ii: number) => {
            return (
              <Group.Item
                key={ii}
                border={ii + 1 < groups?.length}
                RightItem={
                  item.detail
                    ? () => <Text color={colors.primary}>{item.detail}</Text>
                    : undefined
                }
                titleProps={{style: styles.textItem}}
                onPress={() => onPressItem(item)}
              >
                {translate(item.name)}
              </Group.Item>
            )
          })}
        </Group>
      ))}
      {DynamicConstants.NODE_ENV === 'development' && showDevTab && (
        <DevSetting onPressItem={onPressItem} />
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  list: {
    rowGap: space.sm,
  },
  textItem: {
    color: colors.primary,
  },
})
export default React.memo(GroupMenu, () => true)
