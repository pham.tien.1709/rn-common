import {HStack, Icon, IconName, Text} from '@ui-kit/components'
import React from 'react'
import {StyleSheet, Pressable} from 'react-native'
import {colors, space, fontSize, iconSize} from '@ui-kit/theme'

interface ItemType<T> {
  name: string | T
  icon: string
  screen: string
}

interface MenuHorizontalProps<T> {
  data: Array<ItemType<T>>
  onPressItem: (data: ItemType<T>) => void
}

const MenuHorizontal: React.FC<MenuHorizontalProps<any>> = ({
  data,
  onPressItem,
}) => {
  return (
    <HStack style={styles.list}>
      {data.map((item, i: number) => {
        return (
          <Pressable
            key={i}
            style={styles.item}
            onPress={() => onPressItem(item)}
          >
            <Icon icon={item?.icon as IconName} size={iconSize.sm} />
            <Text style={styles.textItem}>{item?.name}</Text>
          </Pressable>
        )
      })}
    </HStack>
  )
}

const styles = StyleSheet.create({
  list: {
    rowGap: space.xs,
    backgroundColor: colors.white,
    marginBottom: space.md,
    paddingVertical: space.md,
    paddingHorizontal: space.sm,
    borderRadius: space['3xs'],
  },
  item: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textItem: {
    marginTop: space['3xs'],
    fontSize: fontSize.xs,
    color: colors.primary,
  },
})
export default MenuHorizontal
