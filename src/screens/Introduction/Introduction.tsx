import React from 'react'
import {StyleSheet} from 'react-native'
import {Container, StatusBar, Text} from '@ui-kit/components'
import {colors} from '@ui-kit/theme'

export default function Introduction() {
  return (
    <Container style={styles.container}>
      <StatusBar
        backgroundColor={colors.transparent}
        barStyle={'light-content'}
      />
      <Text>Introduction</Text>
    </Container>
  )
}

const styles = StyleSheet.create({
  container: {},
})
