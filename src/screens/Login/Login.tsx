import React, {useRef, useState} from 'react'
import {
  BottomSheet,
  BottomSheetRef,
  Button,
  Container,
  Icon,
  Input,
  KeyboardDismissContainer,
  Text,
} from '@ui-kit/components'
import styles from './styles'
import {useTranslate} from '@i18n/hook/useTranslate'
import {Keyboard, Pressable, View} from 'react-native'
import {NavigationProp, useNavigation} from '@react-navigation/native'
import {AuthStackParamList} from '@app-types/NavigationType'
import {translate} from '@i18n/translate'
import {iconSize, primary, space, trueGray} from '@ui-kit/theme'
import {PREFIX_PHONE_NUMBER} from '@constant/constant/constants'
import {orange} from '@constant/themes/colors'
import WrapNetworkLayout from '@components/WrapNetworkLayout'
import {IS_ANDROID} from '@ui-kit/constant'

const LeftItemPhoneNumber = ({
  onPress,
  value,
}: {
  onPress: () => void
  value: string
}) => {
  return (
    <Pressable style={styles.leftInput} onPress={onPress}>
      <Icon icon="phone" size={20} />
      <Text style={styles.mr8}>{value}</Text>
      <Icon icon="arrow-down" size={20} />
    </Pressable>
  )
}

const LeftItemPassword = () => {
  return (
    <View style={styles.leftInput}>
      <Icon icon="lock" size={20} />
    </View>
  )
}

const Login = () => {
  const navigation = useNavigation<NavigationProp<AuthStackParamList>>()

  const sheetRef = useRef<BottomSheetRef>(null)

  const [isSubmit1stTime] = useState(true)
  const [isLoading] = useState(false)
  const [prefixPhoneNumber, setPrefixPhoneNumber] = useState<string>(
    PREFIX_PHONE_NUMBER[0].value
  )
  const [errorMessageFormatPhone, setErrorMessageFormatPhone] =
    useState<string>('')
  const [errorMessagePassword, setErrorMessagePassword] = useState<string>('')
  const [phoneNumber, setPhoneNumber] = useState<string>('')
  const [password, setPassword] = useState<string>('')

  const txtWelcome = useTranslate('login.welcome')
  const txtPhoneNumber = useTranslate('login.phoneNumber')
  const txtPassword = useTranslate('login.password')
  const txtLogin = useTranslate('login.login')

  const onGoBack = () => {
    navigation.goBack()
  }

  const onSelectCountryCode = () => {
    if (sheetRef.current) {
      Keyboard.dismiss()
      sheetRef.current.present()
    }
  }

  const onSelectPrefix = (value: string) => {
    setPrefixPhoneNumber(value)
    if (sheetRef.current) {
      sheetRef.current.close()
    }
  }

  const onCheckFormatPhone = (phoneValue?: string) => {
    const lengthPhoneNumber =
      (phoneValue ? phoneValue : phoneNumber).match(/\d/g)?.length || 0
    const valid = lengthPhoneNumber >= 8 && lengthPhoneNumber <= 15
    if (!valid) {
      setErrorMessageFormatPhone(translate('login.incorrectFormat'))
    } else if (errorMessageFormatPhone.length) {
      setErrorMessageFormatPhone('')
    }
    return valid
  }

  const onCheckFormatPassword = (passwordValue?: string) => {
    const lengthPassword =
      (passwordValue ? passwordValue : password).length || 0
    const valid = lengthPassword >= 6
    if (!valid) {
      setErrorMessagePassword(translate('login.validPassword'))
    } else if (errorMessagePassword.length) {
      setErrorMessagePassword('')
    }
    return valid
  }

  const onLogin = async () => {}

  return (
    <WrapNetworkLayout>
      <KeyboardDismissContainer>
        <Container style={styles.container}>
          <View style={styles.header}>
            <Pressable
              hitSlop={space.xs}
              onPress={onGoBack}
              style={{paddingLeft: space.md, paddingBottom: space.md}}
            >
              <Icon icon="cross" size={space.md} />
            </Pressable>
          </View>
          <Container style={styles.body}>
            <Text style={styles.txtWelcome}>{txtWelcome}</Text>
            <Input
              LeftItem={() => (
                <LeftItemPhoneNumber
                  onPress={onSelectCountryCode}
                  value={prefixPhoneNumber}
                />
              )}
              placeholder={txtPhoneNumber}
              style={styles.input}
              error={errorMessageFormatPhone}
              inputMode="numeric"
              value={phoneNumber}
              onChangeText={t => {
                setPhoneNumber(t)
                if (!isSubmit1stTime) {
                  onCheckFormatPhone(t)
                }
              }}
            />
            <Input
              LeftItem={LeftItemPassword}
              placeholder={txtPassword}
              style={styles.input}
              secureTextEntry
              value={password}
              error={errorMessagePassword}
              onChangeText={t => {
                setPassword(t)
                if (!isSubmit1stTime) {
                  onCheckFormatPassword(t)
                }
              }}
            />
            <Button
              size="lg"
              onPress={onLogin}
              loading={isLoading}
              disabled={!(!!phoneNumber.length && !!password.length)}
            >
              {txtLogin.toUpperCase()}
            </Button>
          </Container>
          <BottomSheet
            ref={sheetRef}
            title={translate('login.selectCountryCode')}
            titleProps={{style: {color: primary[800]}}}
            enableDynamicSizing
          >
            {PREFIX_PHONE_NUMBER.map((phoneNumber, idx) => {
              return (
                <Pressable
                  key={phoneNumber.id}
                  style={[
                    styles.countryCodeField,
                    idx < PREFIX_PHONE_NUMBER.length - 1
                      ? styles.bottomLine
                      : {
                          paddingBottom: IS_ANDROID
                            ? space['4xl']
                            : space['3xs'],
                        },
                  ]}
                  onPress={() => {
                    onSelectPrefix(phoneNumber.value)
                  }}
                >
                  <Icon
                    icon="mobile"
                    size={iconSize.sm}
                    color={trueGray[300]}
                  />
                  <Text
                    style={[
                      {marginLeft: space['3xs']},
                      prefixPhoneNumber === phoneNumber.value
                        ? {color: orange[400]}
                        : {},
                    ]}
                  >
                    {phoneNumber.value}
                  </Text>
                </Pressable>
              )
            })}
          </BottomSheet>
        </Container>
      </KeyboardDismissContainer>
    </WrapNetworkLayout>
  )
}

export default Login
