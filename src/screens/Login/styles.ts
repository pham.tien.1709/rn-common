import {SCREEN_WIDTH} from '@constant/themes/variables'
import {
  avatarSize,
  colors,
  fontSize,
  info,
  insets,
  primary,
  space,
  trueGray,
} from '@ui-kit/theme'
import {StyleSheet} from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: space.md,
  },
  body: {alignContent: 'center', flex: 1, marginTop: 50},
  footer: {
    position: 'absolute',
    bottom: insets.bottom,
    justifyContent: 'center',
    alignContent: 'center',
    width: SCREEN_WIDTH,
  },
  header: {
    marginTop: insets.top + 20,
    alignItems: 'flex-end',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: space.xl,
  },
  btnSocial: {
    width: 60,
    height: 60,
    backgroundColor: colors.white,
    borderWidth: 1,
    borderColor: trueGray[200],
    marginHorizontal: space.lg,
    borderRadius: 10,
  },
  imgSocial: {
    width: avatarSize.xs,
    height: avatarSize.xs,
    resizeMode: 'contain',
  },
  txtWelcome: {
    fontSize: fontSize['2xl'],
    marginBottom: space.md,
    fontWeight: '700',
  },
  txtForgotPassword: {
    marginVertical: space.xl,
    color: primary[600],
  },
  input: {marginTop: space.lg, marginBottom: space.xs},
  txtCreateAccount: {textAlign: 'center', marginBottom: space.sm},
  txtCreateAccountActive: {color: info[600]},
  txtGuest: {textAlign: 'center', color: primary[800], fontSize: fontSize.md},
  leftInput: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  countryCodeField: {
    flexDirection: 'row',
    marginHorizontal: space.xs,
    paddingHorizontal: space.sm,
    paddingVertical: space.sm,
    alignItems: 'center',
  },
  mr8: {marginHorizontal: 8},
  alignEnd: {alignSelf: 'flex-end'},
  center: {flexDirection: 'row', justifyContent: 'center'},
  hCreateAccount: {
    height: space.xl,
  },
  bottomLine: {
    borderBottomColor: trueGray[100],
    borderBottomWidth: 1,
  },
  markdown: {
    fontSize: fontSize.xs,
    marginHorizontal: space['4xl'],
    textAlign: 'center',
    marginTop: space.xs,
    width: 190,
    alignSelf: 'center',
  },
})
