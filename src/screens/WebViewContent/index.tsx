import React from 'react'
import {ActivityIndicator, StyleSheet} from 'react-native'
import {Container, NavigationBar} from '@ui-kit/components'
import {ScreenProps} from '@app-types/NavigationType'
import ScreensName from '@constant/screensName'
import {useTranslate} from '@i18n/hook/useTranslate'
import {TxKeyPath} from '@i18n/i18n'
import WebView from 'react-native-webview'
import StatusOnScreen from '@components/StatusOnScreen'
import {useEffectAfterTransition} from '@hooks/QueryHook'

type WebViewContentProps = ScreenProps<ScreensName.WEB_VIEW_CONTENT>

export default function WebViewContent({route}: WebViewContentProps) {
  const {params} = route
  const txTitle = useTranslate((params as any)?.txTitle as TxKeyPath)
  const uri = 'https://www.google.com'
  const [isReady, setIsReady] = React.useState(false)
  // laggy when navigation
  useEffectAfterTransition(() => {
    setIsReady(true)
  }, [])

  return (
    <Container style={styles.container}>
      <NavigationBar
        title={(params as any).title ? (params as any).title : txTitle}
      />
      <StatusOnScreen
        loading={!isReady}
        SkeletonView={<ActivityIndicator style={styles.loadingView} />}
      >
        <WebView source={{uri}} startInLoadingState={true} />
      </StatusOnScreen>
    </Container>
  )
}

const styles = StyleSheet.create({
  container: {
    //
  },
  loadingView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
})
