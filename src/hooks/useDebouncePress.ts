import {useRef} from 'react'

const BOUNCE_RATE = 2000

export const useDebouncePress = () => {
  const busy = useRef(false)

  const debounce = async (
    callback: (() => void) | undefined,
    bounceRate?: number
  ) => {
    if (!callback) return
    setTimeout(() => {
      busy.current = false
    }, bounceRate || BOUNCE_RATE)

    if (!busy.current) {
      busy.current = true
      callback?.()
    }
  }

  return {debounce}
}
