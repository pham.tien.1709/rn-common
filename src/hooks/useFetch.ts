import {useEffect, useReducer} from 'react'

interface State<T> {
  data?: T
  error?: Error
  loading?: boolean
  onLoad: () => void
  setData: (data: any) => void
}

// discriminated union type
type Action =
  | {type: 'loading'}
  | {type: 'fetched'; payload: any[]}
  | {type: 'error'; payload: Error}

export function useFetch<T>(
  fetchFn: () => Promise<any>,
  options?: {
    lazy?: boolean
    onSuccess?: (data: T) => void
  }
): State<T> {
  const lazy = options?.lazy

  const initialState: State<T> = {
    error: undefined,
    data: undefined,
    loading: !lazy,
    onLoad: () => {},
    setData: () => {},
  }

  // Keep state logic separated
  const fetchReducer = (state: State<T>, action: Action): State<T> => {
    switch (action.type) {
      case 'loading':
        return {...initialState, loading: true}
      case 'fetched':
        return {
          ...initialState,
          data: action.payload as T,
          error: undefined,
          loading: false,
        }
      case 'error':
        return {
          ...initialState,
          error: action.payload,
          loading: false,
        }
      default:
        return state
    }
  }

  const [state, dispatch] = useReducer(fetchReducer, initialState)

  const onLoad = () => {
    dispatch({type: 'loading'})
    loadData()
  }

  const loadData = () => {
    const fetchData = async () => {
      try {
        const response = await fetchFn()
        const status = response?.status

        if (status === 200 || Array.isArray(response)) {
          const data = response?.data ?? response

          options?.onSuccess?.(data)
          dispatch({type: 'fetched', payload: data})
          return
        }

        throw response
      } catch (error) {
        dispatch({type: 'error', payload: error as Error})
      }
    }

    fetchData()
  }

  const setData = (value: any) => {
    dispatch({
      type: 'fetched',
      payload: value,
    })
  }

  useEffect(() => {
    // Use the cleanup function for avoiding a possibly...
    // ...state update after the component was unmounted
    if (lazy) return
    onLoad()
  }, [])

  return {...state, onLoad, setData}
}
