import {ListResponse} from '@app-types/ApiRequestType'
import {TAKE} from '@constant/constant/constants'
import {useEffect, useReducer} from 'react'

interface State {
  total?: number
  data?: any[]
  error?: Error
  loading?: boolean
  loadEnd?: boolean
  loadMore?: boolean
  refreshing?: boolean
  onLoad: () => void
  onLoadMore?: () => void
  onRefresh: () => void
  setLoading?: () => void
}

// discriminated union type
type Action =
  | {type: 'refreshing'}
  | {type: 'loadMore'}
  | {type: 'loading'}
  | {type: 'fetched'; payload: any[]}
  | {type: 'error'; payload: Error}

export function useFetchList<Item>(
  fetchFn: ({skip}: {skip: number}) => Promise<ListResponse<Item>>,
  options?: {
    lazy?: boolean
    keyDependency?: any[]
    defaultValue?: any
  }
): State {
  const lazy = options?.lazy

  const initialState: State = {
    error: undefined,
    data: options?.defaultValue,
    loading: !lazy,
    loadEnd: false,
    loadMore: false,
    refreshing: false,
    onRefresh: () => {},
    onLoad: () => {},
  }

  // Keep state logic separated
  const fetchReducer = (state: State, action: Action): State => {
    switch (action.type) {
      case 'refreshing':
        return {
          ...state,
          data: undefined,
          refreshing: true,
        }
      case 'loadMore':
        return {
          ...state,
          loadMore: true,
        }
      case 'loading':
        return {...initialState, loading: true}
      case 'fetched':
        return {
          ...initialState,
          ...action.payload,
          loading: false,
          loadMore: false,
          refreshing: false,
        }
      case 'error':
        return {
          ...initialState,
          error: action.payload,
          loading: false,
          loadMore: false,
          refreshing: false,
        }
      default:
        return state
    }
  }

  const [state, dispatch] = useReducer(fetchReducer, initialState)

  const onLoad = () => {
    dispatch({type: 'loading'})
    loadData(true)
  }

  const loadData = (refreshing?: boolean) => {
    const fetchData = async () => {
      try {
        const response = await fetchFn({
          skip: refreshing ? 0 : state?.data?.length ?? 0,
        })
        const status = response?.status

        if (status === 200) {
          const data = response?.data ?? []
          const length = data?.length ?? 0

          const list = [...(state.data ?? []), ...data]
          const payload = {
            data: refreshing ? data : list,
            total: response?.total ?? 0,
          } as any

          if (!length || length < TAKE) {
            payload.loadEnd = true
          }
          dispatch({type: 'fetched', payload})
          return
        }

        throw response
      } catch (error) {
        dispatch({type: 'error', payload: error as Error})
      }
    }

    fetchData()
  }

  const onLoadMore = () => {
    if (state.loading || state.loadMore || state.refreshing || state.loadEnd)
      return null
    dispatch({type: 'loadMore'})
    loadData()
  }

  const onRefresh = () => {
    dispatch({type: 'refreshing'})
    loadData(true)
  }

  useEffect(() => {
    // Use the cleanup function for avoiding a possibly...
    // ...state update after the component was unmounted
    if (lazy) return
    dispatch({type: 'loading'})
    onLoad()
  }, [...(options?.keyDependency ?? [])])

  return {
    ...state,
    onLoadMore,
    onLoad,
    onRefresh,
    setLoading: () => dispatch({type: 'loading'}),
  }
}
