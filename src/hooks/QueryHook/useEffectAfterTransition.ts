import {useEffect} from 'react'
import {InteractionManager} from 'react-native'

export default function useEffectAfterTransition(
  expensiveTask: () => void,
  deps: unknown[]
): void {
  useEffect(() => {
    const task = InteractionManager.runAfterInteractions(() => {
      expensiveTask()
    })
    return () => task.cancel()
  }, deps)
}
