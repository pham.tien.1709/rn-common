import React from 'react'

interface CustomMutationHook<ReqBody, Response> {
  mutateData: (data: ReqBody) => Promise<Response>
}

interface OptionMutation<Response> {
  onSuccess?: (res: Response) => void
  onError?: (err: unknown) => void
}

const useCustomMutation = <ReqBody, Response>({
  mutateData,
}: CustomMutationHook<ReqBody, Response>) => {
  const [loading, setLoading] = React.useState<boolean>(false)
  const [isSuccess, setIsSuccess] = React.useState<boolean>(false)
  const [submitSuccessCount, setSubmitSuccessCount] = React.useState<number>(0)

  const submitData = async (
    data: ReqBody,
    option?: OptionMutation<Response>
  ) => {
    try {
      setLoading(true)
      const response = await mutateData(data)
      setIsSuccess(true)
      setSubmitSuccessCount(prev => prev + 1)
      option?.onSuccess?.(response)
      return response
    } catch (error) {
      setIsSuccess(false)
      option?.onError?.(error)
      // throw new Error(error as any)
    } finally {
      setLoading(false)
    }
  }

  return {
    loading,
    isSuccess,
    submitSuccessCount,
    mutate: submitData,
  }
}

export default useCustomMutation
