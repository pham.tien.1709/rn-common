import React from 'react'

/**
 * `dependency` re-render with the first page of new params.
 * `callBack` func fetch Data with page in props
 */
interface CustomQueryHook<Res> {
  keyDependency?: React.DependencyList
  fetchData: () => Promise<Res>
}
const useCustomQuery = <Res>({
  keyDependency = [],
  fetchData,
}: CustomQueryHook<Res>) => {
  const [loading, setLoading] = React.useState<boolean>(true)
  const [refreshing, setRefreshing] = React.useState<boolean>(false)
  const [data, setData] = React.useState<Res | undefined>(undefined)

  const getData = React.useCallback(async () => {
    try {
      const response = await fetchData()
      if (response) {
        setData(response)
      }
      return response
    } catch (error: any) {
      throw new Error(error)
    } finally {
      setLoading(false)
      setRefreshing(false)
    }
  }, [fetchData])

  const refetchData = React.useCallback(async () => {
    setRefreshing(true)
    getData()
  }, [])

  React.useEffect(() => {
    getData()
  }, [...keyDependency])

  const convertData = React.useMemo(
    () => ({
      data,
    }),
    [data]
  )

  return {
    loading,
    data: convertData.data,
    getData,
    refreshing,
    refetch: refetchData,
  }
}

export default useCustomQuery
