import React from 'react'
import _ from 'lodash'

type IFetchData<Res> = (params: {page: number; skip: number}) => Promise<
  | {
      data: Res[]
      total: number
    }
  | undefined
>

interface CustomInfiniteQueryHook<Res> {
  keyDependency?: React.DependencyList
  fetchData: IFetchData<Res>
}
/**
 * `dependency` re-render with the first page of new params.
 * `callBack` func fetch Data with page in props
 */
const useCustomInfiniteQuery = <Res>({
  keyDependency = [],
  fetchData,
}: CustomInfiniteQueryHook<Res>) => {
  const [loading, setLoading] = React.useState<boolean>(true)
  const [refreshing, setRefreshing] = React.useState<boolean>(false)
  const [fetchingNextPage, setFetchingNextPage] = React.useState<boolean>(false)
  const [data, setData] = React.useState<Res[] | undefined>(undefined)
  const [total, setTotal] = React.useState<number>(0)
  const [currentPage, setCurrentPage] = React.useState<number>(0)
  const [error, setError] = React.useState()

  const convertPageToSkip = React.useCallback((page: number) => {
    let skip
    if (page === 0) {
      skip = 0
    } else {
      skip = page * 10
    }
    return skip
  }, [])

  const getData = React.useCallback(
    async (params: any) => {
      try {
        const skip = convertPageToSkip(params.page)
        return await fetchData({...params, skip})
      } catch (error: any) {
        setError(error)
        throw new Error(error)
      } finally {
        setLoading(false)
        setFetchingNextPage(false)
        setRefreshing(false)
      }
    },
    [fetchData, convertPageToSkip]
  )

  const resetData = async (params?: any) => {
    try {
      const response = await getData({...params, page: 0})
      if (response) {
        setCurrentPage(0)
        setTotal(response?.total)
        setData(response?.data)
      }
    } catch {
      /* empty */
    }
  }

  const fetchNextPage = React.useCallback(async () => {
    try {
      setFetchingNextPage(true)
      const response = await getData({
        page: currentPage + 1,
      })
      if (response) {
        setCurrentPage(currentPage + 1)
        setTotal(response?.total)
        setData(prev => {
          if (!prev) return []
          return _.uniqBy([...prev, ...response.data], 'id')
        })
      }
    } catch {
      /* empty */
    }
  }, [getData, currentPage])

  const refetchData = React.useCallback(async () => {
    setRefreshing(true)
    resetData()
  }, [])

  React.useEffect(() => {
    resetData()
  }, [...keyDependency])

  const convertData = React.useMemo(() => {
    const hasNextPage = (data ?? []).length < total
    return {
      data,
      hasNextPage,
    }
  }, [data, total])

  return {
    loading,
    data: convertData.data,
    hasNextPage: convertData.hasNextPage,
    getData: resetData,
    fetchingNextPage,
    fetchNextPage,
    convertPageToSkip,
    changeData: setData,
    refreshing,
    refetch: refetchData,
    // prefetchData: prefetchData,
    total,
    error,
  }
}

export default useCustomInfiniteQuery
