import store, {useAppSelector} from '@app/stores'
import AppDataSingleton from '@constant/singletons/appData.singleton'

export const checkAuth = () => store.getState().auth?.tokenInfo?.jwt

export const useAuth = () => {
  const auth = useAppSelector(state => state.auth)
  AppDataSingleton.getInstance().setToken(auth.tokenInfo?.jwt)
  return {isAuthenticated: auth.tokenInfo != null && auth.user != null}
}
