import {translate} from '@i18n/translate'
import {Notify} from '@ui-kit/common'
import Clipboard from '@react-native-clipboard/clipboard'

const useCopyToClipboard = (link: string) => {
  const copyLink = () => {
    if (link) {
      Clipboard.setString(link)
      Notify.show({
        title: translate('referral.copied'),
        // status: 'info',
        type: 'subtle',
      })
    }
  }
  return {copyLink}
}

export default useCopyToClipboard
