import React from 'react'
import {LocaleContextProvider} from './i18n'
import {LogBox, UIManager} from 'react-native'
import {KeyboardProvider} from 'react-native-keyboard-controller'
import {Provider} from 'react-redux'
import store from './stores'
import {Provider as DesignSystemProvider} from '@ui-kit/components'
import Root from './Root'
import {IS_ANDROID} from '@ui-kit/constant'
import Sentry from '@utils/sentry'

LogBox.ignoreLogs(['Please pass alt prop to Image component'])
LogBox.ignoreAllLogs()

if (IS_ANDROID && UIManager.setLayoutAnimationEnabledExperimental) {
  UIManager.setLayoutAnimationEnabledExperimental(true)
}

function App() {
  return (
    <Provider store={store}>
      <LocaleContextProvider>
        <KeyboardProvider statusBarTranslucent>
          <DesignSystemProvider>
            <Root />
          </DesignSystemProvider>
        </KeyboardProvider>
      </LocaleContextProvider>
    </Provider>
  )
}

// export default Sentry.wrap(App)

export default App
