split_version() {
  TAG_NAME=$1
  VERSION_TYPE=$2 # full, all, major, minor, patch, increment_patch, build, increment_build, env

  VERSION_NAME_PATH=($(grep -oE '[a-z,0-9,-.+]*' <<<"$TAG_NAME"))

  ARRAY_SIZE=${#VERSION_NAME_PATH[@]}

  VERSION=${VERSION_NAME_PATH[$((ARRAY_SIZE - 1))]}
  VERSION_ARRAY=($(grep -oE '[0-9]*' <<<"$VERSION"))

  case $VERSION_TYPE in
  full)
    echo "${VERSION_ARRAY[0]}.${VERSION_ARRAY[1]}.${VERSION_ARRAY[2]}"
    ;;
  all)
    echo ${VERSION}
    ;;
  env)
    ENV_SUB=($(echo $TAG_NAME | sed -e 's/\(^.*-\)//g' | sed -e 's/\(\+.*$\)//g'))
    if [[ $ENV_SUB == $TAG_NAME ]]; then
      echo prd
    else
      echo $ENV_SUB
    fi
    ;;
  major)
    echo ${VERSION_ARRAY[0]}
    ;;
  minor)
    echo ${VERSION_ARRAY[1]}
    ;;
  patch)
    echo ${VERSION_ARRAY[2]}
    ;;
  build)
    echo ${VERSION_ARRAY[3]}
    ;;
  next_build)
    echo ${VERSION_ARRAY[3]} + 1 | bc
    ;;
  esac
}

get_message_information() {
  TAG_SOURCE=$(get_tag_source)
  VERSION=($(grep -oE '[a-z]{3,4}|[0-9]{1,3}' <<<"$TAG_NAME"))
  VERSION_APPNAME="${VERSION[0]}-${VERSION[1]}"
  VERSION_ENV=$(split_version $TAG_SOURCE env)
  VERSION_MAJOR=$(split_version $TAG_SOURCE major)
  VERSION_MINOR=$(split_version $TAG_SOURCE minor)
  VERSION_PATCHE=$(split_version $TAG_SOURCE patche)
  VERSION_BUILD_NUMBER=$(split_version $TAG_SOURCE build)
  VERSION_FULL=$(split_version $TAG_SOURCE full)
  LINK_ACTION="https://github.com/$GITHUB_REPOSITORY/actions/runs/$GITHUB_RUN_ID"
  # AUTHOR=$GITHUB_TRIGGERING_ACTOR
  AUTHOR=$TRIGGERING_ACTOR
  if [ -z "$AUTHOR" ]; then
    AUTHOR=$(git show -s --format='%an')
  fi
  AUTHOR=$(get_author_name "$AUTHOR")
  MESSAGE_INFO="\n- App: Carz Customer.\n"
  MESSAGE_INFO+="- Môi trường: $VERSION_ENV.\n"
  MESSAGE_INFO+="- Phiên bản: $VERSION_FULL.\n"
  MESSAGE_INFO+="- Bản build số: $VERSION_BUILD_NUMBER.\n"
  MESSAGE_INFO+="- Thanh niên: ${AUTHOR}.\n"
  MESSAGE_INFO+="- Tag: ${TAG_SOURCE}.\n"
  MESSAGE_INFO+="- Link action: $LINK_ACTION.\n"
  echo $MESSAGE_INFO
}

get_message_information_for_generate_lang() {
  LINK_ACTION="https://github.com/$GITHUB_REPOSITORY/actions/runs/$GITHUB_RUN_ID"
  AUTHOR=$GITHUB_TRIGGERING_ACTOR
  if [ -z "$AUTHOR" ]; then
    AUTHOR=$(git show -s --format='%an')
  fi
  AUTHOR=$(get_author_name "$AUTHOR")
  MESSAGE_INFO="\n- App: Carz Customer.\n"
  MESSAGE_INFO+="- Thanh niên: ${AUTHOR}.\n"
  MESSAGE_INFO+="- Branch: ${GITHUB_REF_NAME}.\n"
  MESSAGE_INFO+="- Link action: $LINK_ACTION.\n"
  echo $MESSAGE_INFO
}

get_author_name() {
  AUTHOR_NAME=$1
  case $AUTHOR_NAME in
  phamtien1709)
    AUTHOR_NAME="Tiến - Hanoi"
    ;;
  'Titus Pham')
    AUTHOR_NAME="Tiến - Hanoi"
    ;;
  esac
  echo $AUTHOR_NAME
}

get_tag_source() {
  if [[ "$GITHUB_REF_NAME" =~ v[0-9]*\.[0-9]*\.[0-9]*\-(dev|stg|prd)\+[0-9]* ]]; then
    echo $GITHUB_REF_NAME
  elif [[ "$REF_NAME" =~ v[0-9]*\.[0-9]*\.[0-9]*\-(dev|stg|prd)\+[0-9]* ]]; then
    echo $REF_NAME
  else
    SILENT_LOGS=$(git fetch --tags --force)
    git describe --tags
  fi
}

config_github_token() {
  if [[ $(git config user.email) == '41898282+github-actions[bot]@users.noreply.github.com'* ]]; then
    echo "Already config github token"
  else
    REMOTE_REPO="https://${GITHUB_ACTOR}:${GITHUB_TOKEN}@github.com/${GITHUB_REPOSITORY}.git"
    git remote add publisher $REMOTE_REPO || true
    git config user.name "GitHub Actions"
    git config user.email "41898282+github-actions[bot]@users.noreply.github.com"
  fi
}
