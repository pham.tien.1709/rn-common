# =====

function fail {
  echo $1 >&2
  exit 1
}

function retry {
  local n=1
  local max=100
  local delay=1
  while true; do
    "$@" && break || {
      if [[ $n -lt $max ]]; then
        ((n++))
        echo "Command failed. Attempt $n/$max:"
        sleep $delay
      else
        fail "The command has failed after $n attempts."
      fi
    }
  done
}

# =====
function backup_pod_to_local {
  CACHE_FOLDER_NAME="carz-customer-pods-$(md5sum yarn.lock | awk '{ print $1 }')"
  CACHE_PATH=.cache/$CACHE_FOLDER_NAME
  # =====
  echo INFO: CREATE CACHE FOLDER.
  if [ ! -d "$CACHE_PATH" ]; then
    echo "- cache does not exist."
    mkdir $CACHE_PATH
  else
    echo "- cache exist."
    rm -rf $CACHE_PATH/manifest.txt
    rm -rf $CACHE_PATH/cache.tzst
    rm -rf $CACHE_PATH/cache.tgz
  fi
  echo INFO: Backup manifest.txt.
  echo $'ios/Pods\nios/Podfile.lock' >$CACHE_PATH/manifest.txt
  echo '- success'
  echo INFO: Backup cache.
  # tar --posix -cf $CACHE_PATH/cache.tzst -P -C . --files-from $CACHE_PATH/manifest.txt --use-compress-program zstdmt
  tar --posix -cf $CACHE_PATH/cache.tgz -P -C . --files-from $CACHE_PATH/manifest.txt -z
  echo '- success'
}

function backup_pod_to_remote {
  CACHE_FOLDER_NAME="carz-customer-pods-$(md5sum yarn.lock | awk '{ print $1 }')"
  CACHE_PATH=.cache/$CACHE_FOLDER_NAME
  # =====
  echo INFO: Upload
  retry scp -i .github/scripts/data/rsa/hippo_id_rsa -P 228 -r $CACHE_PATH hippo@hippo.lbtvip.pro:actions-runner/carz/hippo-alpha/_work/_tool/LibertyCarz/rn-carz-customer
  echo '- success'
}

function restore_pod_from_local {
  CACHE_FOLDER_NAME="carz-customer-pods-$(md5sum yarn.lock | awk '{ print $1 }')"
  CACHE_PATH=.cache/$CACHE_FOLDER_NAME
  # =====
  echo INFO: clean Pods and Podfile.lock.
  rm -rf ios/Pods && rm -rf ios/Podfile.lock
  echo '- success'
  echo INFO: Restore cache.
  # tar -xf $CACHE_PATH/cache.tzst -P -C . --use-compress-program unzstd
  tar -xf $CACHE_PATH/cache.tgz -P -C . -z
  echo '- success'
}

function restore_pod_from_remote {
  CACHE_FOLDER_NAME="carz-customer-pods-$(md5sum yarn.lock | awk '{ print $1 }')"
  CACHE_PATH=.cache/$CACHE_FOLDER_NAME
  # =====
  echo INFO: Download
  retry scp -i .github/scripts/data/rsa/hippo_id_rsa -P 228 -r hippo@hippo.lbtvip.pro:actions-runner/carz/hippo-alpha/_work/_tool/LibertyCarz/rn-carz-customer/$CACHE_FOLDER_NAME .cache
  echo '- success'
}

function pod_install {
  CACHE_FOLDER_NAME="carz-customer-pods-$(md5sum yarn.lock | awk '{ print $1 }')"
  echo "cache name: $CACHE_FOLDER_NAME"
  CACHE_PATH=.cache/$CACHE_FOLDER_NAME
  chmod 600 .github/scripts/data/rsa/hippo_id_rsa
  echo INFO: Pod install.
  if [ -d "$CACHE_PATH" ]; then
    echo "- cache local exist."
    restore_pod_from_local
  else
    echo "- cache local does not exist."
    retry ssh -i .github/scripts/data/rsa/hippo_id_rsa hippo@hippo.lbtvip.pro -p 228 "echo '- connected'"
    if ssh -i .github/scripts/data/rsa/hippo_id_rsa hippo@hippo.lbtvip.pro -p 228 "test -e actions-runner/carz/hippo-alpha/_work/_tool/LibertyCarz/rn-carz-customer/$CACHE_FOLDER_NAME"; then
      echo "- cache remote exist."
      restore_pod_from_remote
      restore_pod_from_local
    else
      echo "- cache remote does not exist."
      cd ios &&
        bundle exec pod install --repo-update &&
        cd .. &&
        backup_pod_to_local &&
        backup_pod_to_remote
    fi
  fi
}
