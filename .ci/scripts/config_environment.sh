source .github/scripts/utils.sh

TAG_SOURCE=$(get_tag_source)
VERSION_FULL=$(split_version $TAG_SOURCE full)
VERSION_BUILD_NUMBER=$(split_version $TAG_SOURCE build)
ENVIRONMENT=$(split_version $TAG_SOURCE env)

echo "INFO: Build ${ENVIRONMENT} version ${VERSION_FULL} with build number ${VERSION_BUILD_NUMBER}"

cd .github/scripts/env

if [ $ENVIRONMENT = "prd" ]; then
  bash configure_product_env.sh
fi

if [ $ENVIRONMENT = "stg" ]; then
  bash configure_staging_env.sh
fi

if [ $ENVIRONMENT = "dev" ]; then
  bash configure_develop_env.sh
fi
