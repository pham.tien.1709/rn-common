source .github/scripts/utils.sh

TAG_SOURCE=$(get_tag_source)
VERSION_FULL=$(split_version $TAG_SOURCE full)
VERSION_BUILD_NUMBER=$(split_version $TAG_SOURCE build)

perl -i -pe "s/^\s*versionCode\s*\d*$/'        versionCode ${VERSION_BUILD_NUMBER}'/e" android/app/build.gradle
perl -i -pe "s/^\s*versionName\s*\"\d*.\d*.\d*\"$/'        versionName \"${VERSION_FULL}\"'/e" android/app/build.gradle

perl -i -pe "s/^\s*CURRENT_PROJECT_VERSION\s*=\s*\d*.\d*.\d*;$/'        CURRENT_PROJECT_VERSION = ${VERSION_BUILD_NUMBER};'/e" ios/Liberty.xcodeproj/project.pbxproj
perl -i -pe "s/^\s*MARKETING_VERSION\s*=\s*\d*.\d*.\d*;$/'        MARKETING_VERSION = ${VERSION_FULL};'/e" ios/Liberty.xcodeproj/project.pbxproj
perl -i -pe "s/^\s*version:\s\"\d*.\d*.\d*\"/'      version: \"${VERSION_FULL}\"'/e" fastlane/Fastfile
