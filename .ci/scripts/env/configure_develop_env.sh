cd ../../..

echo 'run develop_configure_env.sh'

APP_ENV_FILE_OLD='src/constant/constant/dynamic.constants.ts'

APP_ENV_FILE='.github/scripts/data/env/dynamic.constants.ts.dev'

# Change env file
cp $APP_ENV_FILE $APP_ENV_FILE_OLD

DEV_PROVISION='Liberty Carz Dev'
DISTRIBUTION_PROVISION='Store Liberty Carz Dev'

DEV_WILDCARD_PROVISION='Liberty Carz Wildcard'
DISTRIBUTION_WILDCARD_PROVISION='Store Liberty Carz Wildcard'

DEV_CODE_SIGN_IDENTITY='iPhone Developer'
DISTRIBUTION_CODE_SIGN_IDENTITY='iPhone Distribution'

PROJECT_PBXPROJ_FILE='ios/Liberty.xcodeproj/project.pbxproj'

perl -pi -e "s/${DEV_PROVISION}/${DISTRIBUTION_PROVISION}/g" $PROJECT_PBXPROJ_FILE
perl -pi -e "s/${DEV_WILDCARD_PROVISION}/${DISTRIBUTION_WILDCARD_PROVISION}/g" $PROJECT_PBXPROJ_FILE
perl -pi -e "s/${DEV_CODE_SIGN_IDENTITY}/${DISTRIBUTION_CODE_SIGN_IDENTITY}/g" $PROJECT_PBXPROJ_FILE
