cd ../../..

echo 'run configure_product_env.sh'

APP_NAME_OLD='\[D\]LibertyCarz'
APP_ENV_FILE_OLD='src/constant/constant/dynamic.constants.ts'

APP_ICON_OLD='src/assets/icons/app-icon.png'
APP_ICON_BACKGROUND_OLD='src/assets/icons/app-icon-background.png'
APP_ICON_FOREGROUND_OLD='src/assets/icons/app-icon-foreground.png'

# android old information
ANDROID_APPLICATION_ID_OLD='kh.com.libertycarz.dev'
MOBILESDK_APP_ID_OLD='1:422008269517:android:efcf0e93b53dae045c9784'
GOOGLE_SERVICES_JSON_FILE_OLD='android/app/google-services.json'
DEBUG_KEYSTORE_OLD='android/app/debug.keystore'
RELEASE_KEYSTORE_OLD='android/app/release.keystore'

# ios old information
IOS_APP_IDENTIFIER_ID_OLD='kh.com.libertycarz.dev'
DEV_PROVISION_OLD='Liberty Carz Dev'
STORE_PROVISION_OLD='Store Liberty Carz Dev'
STORE_PROVISION_WILDCARD_OLD='Liberty Carz Wildcard'
CODE_SIGN_IDENTITY_OLD='iPhone Developer'
GOOGLE_SERVICES_INFO_PLIST_FILE_OLD='ios/Liberty/GoogleService-Info.plist'
TEAM_ID_OLD='M57VLZZSJU'
FASTLANE_TEAM_ID_OLD='125636177'
BRANCH_IO_DO_MAIN_OLD='libertycar-dev.app.link'
BRANCH_LIVE_KEY_OLD='key_live_gwglNKzr49HtVXZ9N5p9eldkBFpMzRNL'
BRANCH_TEST_KEY_OLD='key_test_gzbeJJDFZ1VDLZ3WU3lGZpgiBCoSAMSE'
BRANCH_SCHEMES_ID_OLD='kh.com.libertycarz.dev.1'
BRANCH_KEY_OLD='libertycar-dev.app.link'
BRANCH_KEY_ALTERNATE_OLD='libertycar-dev-alternate.app.link'
GG_CLIENT_ID_OLD='422008269517-93rlfmt5gqk9mgumhio5o8mvlmg90kd0.apps.googleusercontent.com'
GG_REVERSED_CLIENT_ID_OLD='com.googleusercontent.apps.422008269517-93rlfmt5gqk9mgumhio5o8mvlmg90kd0'
API_KEY_OLD='.github/scripts/data/apikey/AuthKey.json'
CRISP_KEY_OLD='95341f93-d151-4fb4-8ae3-62f22d7bab18'

APP_NAME='Carz'
APP_ICON='.github/scripts/data/icons/pro-app-icon.png'
APP_ICON_BACKGROUND='.github/scripts/data/icons/pro-app-icon-background.png'
APP_ICON_FOREGROUND='.github/scripts/data/icons/pro-app-icon-foreground.png'
APP_ENV_FILE='.github/scripts/data/env/dynamic.constants.ts.prod'
BRANCH_IO_DO_MAIN='customer.libertycarz.com'
BRANCH_LIVE_KEY='key_live_nykmOROeUDnhBTPMEU01bemcxxfL8JUB'
BRANCH_TEST_KEY='key_test_otcfLUUiUuhaxUPStG46flepqtcGYMLb'
BRANCH_SCHEMES_ID='com.libertycarz.customer.1'
BRANCH_KEY='customer.libertycarz.com'
BRANCH_KEY_ALTERNATE='lninx-alternate.app.link'

# Android information need to change
ANDROID_APPLICATION_ID='com.libertycarz.customer'
MOBILESDK_APP_ID='1:247290180673:android:864459278520d59d74c2c0'
ANDROID_STRING_FILE='android/app/src/main/res/values/strings.xml'
BUILD_GRADLE_FILE='android/app/build.gradle'
GOOGLE_SERVICES_JSON_FILE='.github/scripts/data/firebase/pro-google-services.json'
DEBUG_KEYSTORE='.github/scripts/data/keystore/release.keystore'
RELEASE_KEYSTORE='.github/scripts/data/keystore/release.keystore'
CRISP_KEY_FILE_IOS='ios/Liberty/AppDelegate.mm'
CRISP_KEY_FILE_ANDROID='android/app/src/main/java/com/liberty/MainApplication.java'

# ios information need to change
IOS_APP_IDENTIFIER_ID='com.libertycarz.customer'
STORE_PROVISION='Store Liberty Carz Customer App'
STORE_PROVISION_WILDCARD='Store Liberty Carz Wildcard'
CODE_SIGN_IDENTITY='iPhone Distribution'
INFO_PLIST_FILE='ios/Liberty/Info.plist'
FAST_FILE='fastlane/Fastfile'
PROJECT_PBXPROJ_FILE='ios/Liberty.xcodeproj/project.pbxproj'
GOOGLE_SERVICES_INFO_PLIST_FILE='.github/scripts/data/firebase/pro-GoogleService-Info.plist'
TEAM_ID='UN6QG6576U'
FASTLANE_TEAM_ID='126191713'
LIBERTY_ENTITLEMENTS_FILE='ios/Liberty/Liberty.entitlements'
BRANCH_JSON_FLIE='ios/branch.json'
GG_CLIENT_ID='247290180673-imoso1c4atdsru7lrla3vsctnt46pe7i.apps.googleusercontent.com'
GG_REVERSED_CLIENT_ID='com.googleusercontent.apps.247290180673-imoso1c4atdsru7lrla3vsctnt46pe7i'
API_KEY='.github/scripts/data/apikey/pro_AuthKey.json'
CRISP_KEY='184df128-679b-45fd-8668-cebc7707674d'

# Change appname android
perl -pi -e "s/${APP_NAME_OLD}/${APP_NAME}/g" $ANDROID_STRING_FILE
perl -pi -e "s/${BRANCH_IO_DO_MAIN_OLD}/${BRANCH_IO_DO_MAIN}/g" $ANDROID_STRING_FILE
perl -pi -e "s/${BRANCH_LIVE_KEY_OLD}/${BRANCH_LIVE_KEY}/g" $ANDROID_STRING_FILE
perl -pi -e "s/${BRANCH_TEST_KEY_OLD}/${BRANCH_TEST_KEY}/g" $ANDROID_STRING_FILE

# Change appid android
perl -pi -e "s/${ANDROID_APPLICATION_ID_OLD}/${ANDROID_APPLICATION_ID}/g" $BUILD_GRADLE_FILE
perl -pi -e "s/${MOBILESDK_APP_ID_OLD}/${MOBILESDK_APP_ID}/g" $FAST_FILE

# Change google-services.json file
cp $GOOGLE_SERVICES_JSON_FILE $GOOGLE_SERVICES_JSON_FILE_OLD

# Change env file
cp $APP_ENV_FILE $APP_ENV_FILE_OLD

# Change key store file
cp $DEBUG_KEYSTORE $DEBUG_KEYSTORE_OLD
cp $RELEASE_KEYSTORE $RELEASE_KEYSTORE_OLD

# Change info plist file
perl -pi -e "s/${APP_NAME_OLD}/${APP_NAME}/g" $INFO_PLIST_FILE
perl -pi -e "s/${BRANCH_IO_DO_MAIN_OLD}/${BRANCH_IO_DO_MAIN}/g" $INFO_PLIST_FILE
perl -pi -e "s/${BRANCH_LIVE_KEY_OLD}/${BRANCH_LIVE_KEY}/g" $INFO_PLIST_FILE
perl -pi -e "s/${BRANCH_SCHEMES_ID_OLD}/${BRANCH_SCHEMES_ID}/g" $INFO_PLIST_FILE
perl -pi -e "s/${GG_CLIENT_ID_OLD}/${GG_CLIENT_ID}/g" $INFO_PLIST_FILE
perl -pi -e "s/${GG_REVERSED_CLIENT_ID_OLD}/${GG_REVERSED_CLIENT_ID}/g" $INFO_PLIST_FILE

# Change appid ios
perl -pi -e "s/${IOS_APP_IDENTIFIER_ID_OLD}/${IOS_APP_IDENTIFIER_ID}/g" $FAST_FILE
perl -pi -e "s/${IOS_APP_IDENTIFIER_ID_OLD}/${IOS_APP_IDENTIFIER_ID}/g" $PROJECT_PBXPROJ_FILE

# Change provision name
perl -pi -e "s/${DEV_PROVISION_OLD}/${STORE_PROVISION}/g" $PROJECT_PBXPROJ_FILE
perl -pi -e "s/${CODE_SIGN_IDENTITY_OLD}/${CODE_SIGN_IDENTITY}/g" $PROJECT_PBXPROJ_FILE
perl -pi -e "s/${STORE_PROVISION_WILDCARD_OLD}/${STORE_PROVISION_WILDCARD}/g" $PROJECT_PBXPROJ_FILE
perl -pi -e "s/${TEAM_ID_OLD}/${TEAM_ID}/g" $PROJECT_PBXPROJ_FILE
perl -pi -e "s/${STORE_PROVISION_OLD}/${STORE_PROVISION}/g" $FAST_FILE

# Change Liberty.entitlements file
perl -pi -e "s/${BRANCH_KEY_OLD}/${BRANCH_KEY}/g" $LIBERTY_ENTITLEMENTS_FILE
perl -pi -e "s/${BRANCH_KEY_ALTERNATE_OLD}/${BRANCH_KEY_ALTERNATE}/g" $LIBERTY_ENTITLEMENTS_FILE

#Change branch.json file
perl -pi -e "s/${BRANCH_LIVE_KEY_OLD}/${BRANCH_LIVE_KEY}/g" $BRANCH_JSON_FLIE
perl -pi -e "s/${BRANCH_TEST_KEY_OLD}/${BRANCH_TEST_KEY}/g" $BRANCH_JSON_FLIE

# Change team_id fastfile
perl -pi -e "s/${FASTLANE_TEAM_ID_OLD}/${FASTLANE_TEAM_ID}/g" $FAST_FILE

# Change GoogleService-Info.plist file
cp $GOOGLE_SERVICES_INFO_PLIST_FILE $GOOGLE_SERVICES_INFO_PLIST_FILE_OLD

# Change App-icon
cp $APP_ICON $APP_ICON_OLD
cp $APP_ICON_BACKGROUND $APP_ICON_BACKGROUND_OLD
cp $APP_ICON_FOREGROUND $APP_ICON_FOREGROUND_OLD

#Change API Key file
cp $API_KEY $API_KEY_OLD

#Change Crisp
perl -pi -e "s/${CRISP_KEY_OLD}/${CRISP_KEY}/g" $CRISP_KEY_FILE_IOS
perl -pi -e "s/${CRISP_KEY_OLD}/${CRISP_KEY}/g" $CRISP_KEY_FILE_ANDROID
