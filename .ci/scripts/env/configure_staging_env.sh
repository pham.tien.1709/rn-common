cd ../../..

echo 'run configure_staging_env.sh'

APP_NAME_OLD='\[D\]LibertyCarz'
APP_ENV_FILE_OLD='src/constant/constant/dynamic.constants.ts'

APP_ICON_OLD='src/assets/icons/app-icon.png'
APP_ICON_BACKGROUND_OLD='src/assets/icons/app-icon-background.png'
APP_ICON_FOREGROUND_OLD='src/assets/icons/app-icon-foreground.png'

# android old information
ANDROID_APPLICATION_ID_OLD='kh.com.libertycarz.dev'
MOBILESDK_APP_ID_OLD='1:422008269517:android:efcf0e93b53dae045c9784'
GOOGLE_SERVICES_JSON_FILE_OLD='android/app/google-services.json'

# ios old information
IOS_APP_IDENTIFIER_ID_OLD='kh.com.libertycarz.dev'
DEV_PROVISION_OLD='Liberty Carz Dev'
STORE_PROVISION_OLD='Store Liberty Carz Dev'
STORE_PROVISION_WILDCARD_OLD='Liberty Carz Wildcard'
CODE_SIGN_IDENTITY_OLD='iPhone Developer'
GOOGLE_SERVICES_INFO_PLIST_FILE_OLD='ios/Liberty/GoogleService-Info.plist'
BRANCH_IO_DO_MAIN_OLD='libertycar-dev.app.link'
BRANCH_LIVE_KEY_OLD='key_live_gwglNKzr49HtVXZ9N5p9eldkBFpMzRNL'
BRANCH_TEST_KEY_OLD='key_test_gzbeJJDFZ1VDLZ3WU3lGZpgiBCoSAMSE'
BRANCH_SCHEMES_ID_OLD='kh.com.libertycarz.dev.1'
BRANCH_KEY_OLD='libertycar-dev'
GG_CLIENT_ID_OLD='422008269517-93rlfmt5gqk9mgumhio5o8mvlmg90kd0.apps.googleusercontent.com'
GG_REVERSED_CLIENT_ID_OLD='com.googleusercontent.apps.422008269517-93rlfmt5gqk9mgumhio5o8mvlmg90kd0'
GG_APP_ID_OLD='1:422008269517:ios:6e06a91a6822a9155c9784'

APP_NAME='\[S\]LibertyCarz'
APP_ICON='.github/scripts/data/icons/stg-app-icon.png'
APP_ICON_BACKGROUND='.github/scripts/data/icons/stg-app-icon-background.png'
APP_ICON_FOREGROUND='.github/scripts/data/icons/stg-app-icon-foreground.png'
APP_ENV_FILE='.github/scripts/data/env/dynamic.constants.ts.stg'
BRANCH_IO_DO_MAIN='libertycar-stg.app.link'
BRANCH_LIVE_KEY='key_live_eFjoJGZIX2joAJYSA5CpNkcgEwlxH7y7'
BRANCH_TEST_KEY='key_test_nuhfRP8G24geEMWVu4ygVmkmvrauN7Je'
BRANCH_SCHEMES_ID='kh.com.libertycarz.stg.1'
BRANCH_KEY='libertycar-stg'

# Android information need to change
ANDROID_APPLICATION_ID='kh.com.libertycarz.stg'
MOBILESDK_APP_ID='1:422008269517:android:8422b203ea30f9495c9784'
ANDROID_STRING_FILE='android/app/src/main/res/values/strings.xml'
BUILD_GRADLE_FILE='android/app/build.gradle'
GOOGLE_SERVICES_JSON_FILE='.github/scripts/data/firebase/stg-google-services.json'

# ios information need to change
IOS_APP_IDENTIFIER_ID='kh.com.libertycarz.stg'
STORE_PROVISION='Store Liberty Carz Stg'
STORE_PROVISION_WILDCARD='Store Liberty Carz Wildcard'
CODE_SIGN_IDENTITY='iPhone Distribution'
INFO_PLIST_FILE='ios/Liberty/Info.plist'
FAST_FILE='fastlane/Fastfile'
PROJECT_PBXPROJ_FILE='ios/Liberty.xcodeproj/project.pbxproj'
APP_DELEGATE_FILE='ios/Liberty/AppDelegate.mm'
GOOGLE_SERVICES_INFO_PLIST_FILE='.github/scripts/data/firebase/stg-GoogleService-Info.plist'
LIBERTY_ENTITLEMENTS_FILE='ios/Liberty/Liberty.entitlements'
BRANCH_JSON_FLIE='ios/branch.json'
GG_CLIENT_ID='422008269517-6n85m8mv9hd8bpu83ltr1ohi0jdb01kr.apps.googleusercontent.com'
GG_REVERSED_CLIENT_ID='com.googleusercontent.apps.422008269517-6n85m8mv9hd8bpu83ltr1ohi0jdb01kr'
GG_APP_ID='1:422008269517:ios:31eb2ec187d2030f5c9784'

# Change appname android
perl -pi -e "s/${APP_NAME_OLD}/${APP_NAME}/g" $ANDROID_STRING_FILE
perl -pi -e "s/${BRANCH_IO_DO_MAIN_OLD}/${BRANCH_IO_DO_MAIN}/g" $ANDROID_STRING_FILE
perl -pi -e "s/${BRANCH_LIVE_KEY_OLD}/${BRANCH_LIVE_KEY}/g" $ANDROID_STRING_FILE
perl -pi -e "s/${BRANCH_TEST_KEY_OLD}/${BRANCH_TEST_KEY}/g" $ANDROID_STRING_FILE

# Change appid android
perl -pi -e "s/${ANDROID_APPLICATION_ID_OLD}/${ANDROID_APPLICATION_ID}/g" $BUILD_GRADLE_FILE
perl -pi -e "s/${MOBILESDK_APP_ID_OLD}/${MOBILESDK_APP_ID}/g" $FAST_FILE

# Change google-services.json file
cp $GOOGLE_SERVICES_JSON_FILE $GOOGLE_SERVICES_JSON_FILE_OLD

# Change env file
cp $APP_ENV_FILE $APP_ENV_FILE_OLD

# Change info plist file
perl -pi -e "s/${APP_NAME_OLD}/${APP_NAME}/g" $INFO_PLIST_FILE
perl -pi -e "s/${BRANCH_IO_DO_MAIN_OLD}/${BRANCH_IO_DO_MAIN}/g" $INFO_PLIST_FILE
perl -pi -e "s/${BRANCH_LIVE_KEY_OLD}/${BRANCH_LIVE_KEY}/g" $INFO_PLIST_FILE
perl -pi -e "s/${BRANCH_SCHEMES_ID_OLD}/${BRANCH_SCHEMES_ID}/g" $INFO_PLIST_FILE
perl -pi -e "s/${GG_CLIENT_ID_OLD}/${GG_CLIENT_ID}/g" $INFO_PLIST_FILE
perl -pi -e "s/${GG_REVERSED_CLIENT_ID_OLD}/${GG_REVERSED_CLIENT_ID}/g" $INFO_PLIST_FILE

# Change appid ios
perl -pi -e "s/${GG_APP_ID_OLD}/${GG_APP_ID}/g" $FAST_FILE
perl -pi -e "s/${IOS_APP_IDENTIFIER_ID_OLD}/${IOS_APP_IDENTIFIER_ID}/g" $FAST_FILE
perl -pi -e "s/${IOS_APP_IDENTIFIER_ID_OLD}/${IOS_APP_IDENTIFIER_ID}/g" $PROJECT_PBXPROJ_FILE

# Change provision name
perl -pi -e "s/${DEV_PROVISION_OLD}/${STORE_PROVISION}/g" $PROJECT_PBXPROJ_FILE
perl -pi -e "s/${CODE_SIGN_IDENTITY_OLD}/${CODE_SIGN_IDENTITY}/g" $PROJECT_PBXPROJ_FILE
perl -pi -e "s/${STORE_PROVISION_WILDCARD_OLD}/${STORE_PROVISION_WILDCARD}/g" $PROJECT_PBXPROJ_FILE
perl -pi -e "s/${STORE_PROVISION_OLD}/${STORE_PROVISION}/g" $FAST_FILE

# Change GoogleService-Info.plist file
cp $GOOGLE_SERVICES_INFO_PLIST_FILE $GOOGLE_SERVICES_INFO_PLIST_FILE_OLD

# Change Liberty.entitlements file
perl -pi -e "s/${BRANCH_KEY_OLD}/${BRANCH_KEY}/g" $LIBERTY_ENTITLEMENTS_FILE

#Change branch.json file
perl -pi -e "s/${BRANCH_LIVE_KEY_OLD}/${BRANCH_LIVE_KEY}/g" $BRANCH_JSON_FLIE
perl -pi -e "s/${BRANCH_TEST_KEY_OLD}/${BRANCH_TEST_KEY}/g" $BRANCH_JSON_FLIE

# Change App-icon
cp $APP_ICON $APP_ICON_OLD
cp $APP_ICON_BACKGROUND $APP_ICON_BACKGROUND_OLD
cp $APP_ICON_FOREGROUND $APP_ICON_FOREGROUND_OLD
