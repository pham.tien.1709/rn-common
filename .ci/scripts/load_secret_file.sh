# base64 -i dynamic.constants.ts.prod
source .github/scripts/utils.sh

TAG_SOURCE=$(get_tag_source)
ENVIRONMENT=$(split_version $TAG_SOURCE environment)

cd .github/scripts/data
pwd

echo INFO: Load ${ENVIRONMENT} files

echo INFO: Load firebase/pro-GoogleService-Info.plist
echo $PRO_GOOGLESERVICE_INFO_PLIST >>firebase/pro-GoogleService-Info.plist
echo INFO: Load firebase/pro-google-services.json
echo $PRO_GOOGLE_SERVICES_JSON >>firebase/pro-google-services.json

echo INFO: Load env/dynamic.constants.ts.prod.base64
echo $DYNAMIC_CONSTANTS_TS_PROD_BASE64 >>env/dynamic.constants.ts.prod.base64

if [[ $OSTYPE == 'darwin'* ]]; then
  base64 --decode -i env/dynamic.constants.ts.prod.base64 -o env/dynamic.constants.ts.prod
fi

if [[ $OSTYPE == 'linux'* ]]; then
  base64 -d env/dynamic.constants.ts.prod.base64 <<<cat >env/dynamic.constants.ts.prod
fi

echo INFO: Load keystore/release.keystore.base64
echo $RELEASE_KEYSTORE_BASE64 >>keystore/release.keystore.base64

if [[ $OSTYPE == 'darwin'* ]]; then
  base64 --decode -i keystore/release.keystore.base64 -o keystore/release.keystore
fi

if [[ $OSTYPE == 'linux'* ]]; then
  base64 -d keystore/release.keystore.base64 <<<cat >keystore/release.keystore
fi

echo INFO: Load apikey/AuthKey.json
echo $API_KEY_JSON >>apikey/AuthKey.json
echo $PRO_API_KEY_JSON >>apikey/pro_AuthKey.json

echo INFO: Load apikey/PlayStoreKey.json
echo $PRO_PLAY_STORE_KEY_JSON >>apikey/PlayStoreKey.json
