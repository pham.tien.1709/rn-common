source .github/scripts/utils.sh

TAG_SOURCE=$(get_tag_source)
ENVIRONMENT=$(split_version $TAG_SOURCE env)

if [ $ENVIRONMENT = "prd" ]; then
  echo epsilon
fi

if [ $ENVIRONMENT = "stg" ]; then
  echo delta
fi

if [ $ENVIRONMENT = "dev" ]; then
  echo gamma
fi
