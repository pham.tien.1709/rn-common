var source = require('../../../DesignSystem/components/Icon/icomoon.json')
// eslint-disable-next-line no-console
console.log(
  'export type IconName = ' +
    source.icons.map(item => `'${item.properties.name}'`).join(' | ')
)
