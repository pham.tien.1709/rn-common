module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['./src'],
        alias: {
          '@ui-kit': ['./DesignSystem'],
          '@constant': ['./src/constant'],
          '@navigation': ['./src/navigation'],
          '@sagas': ['./src/sagas'],
          '@stores': ['./src/stores'],
          '@screens': ['./src/screens'],
          '@components': ['./src/components'],
          '@assets': ['./src/assets'],
          '@app-types': ['./src/types'],
          '@utils': ['./src/utils'],
          '@skeletons': ['./src/skeletons'],
          '@hooks': ['./src/hooks'],
          '@i18n': ['./src/i18n'],
          '@app': ['./src'],
          '@comp': ['./src/comp'],
          '*': ['./*']
        },
        extensions: ['.js', '.jsx', '.ts', '.tsx']
      }
    ],
    'react-native-reanimated/plugin'
  ]
}
