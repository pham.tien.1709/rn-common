import {View, StyleSheet, Pressable} from 'react-native'
import React from 'react'
import {useSafeAreaInsets} from 'react-native-safe-area-context'
import {colors, screenSize, space} from '@ui-kit/theme'
import {toggleTheme} from '@app/stores/slices/theme/slice'
import store from '@stores'
import {Icon} from '@ui-kit/components'

const ThemeButton = () => {
  const insets = useSafeAreaInsets()
  const bottom = insets.bottom + space.md

  const onChange = () => {
    store.dispatch(toggleTheme())
  }

  return (
    <View style={[styles.container, {bottom}]}>
      <Pressable onPress={onChange} style={styles.button}>
        <Icon size={24} icon="switch" color="white" />
      </Pressable>
    </View>
  )
}

export default ThemeButton

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    right: space.md,
  },
  button: {
    //
    backgroundColor: colors.primary,
    padding: space.sm,
    borderRadius: screenSize.width,
  },
})
