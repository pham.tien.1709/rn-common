import {Container, Text, Icon, Group} from '@ui-kit/components'
import {space} from '@ui-kit/theme'
import React from 'react'
import {ScrollView, StyleSheet, View} from 'react-native'

const DSGroup = () => {
  return (
    <Container level={2}>
      <ScrollView>
        <View style={styles.list}>
          <Group>
            <Group.Item>My Favourite</Group.Item>
            <Group.Item>Point</Group.Item>
            <Group.Item border={false}>My Request</Group.Item>
          </Group>
          <Group>
            <Group.Item
              LeftItem={({color}) => (
                <Icon size={16} icon="setting" color={color} />
              )}
            >
              Setting
            </Group.Item>
            <Group.Item
              LeftItem={({color}) => (
                <Icon size={16} icon="warning" color={color} />
              )}
            >
              Application Info
            </Group.Item>
            <Group.Item border={false} RightItem={() => <Text>3.11</Text>}>
              Version
            </Group.Item>
          </Group>
        </View>
      </ScrollView>
    </Container>
  )
}

export default DSGroup

const styles = StyleSheet.create({
  list: {
    rowGap: space.sm,
    padding: space.sm,
  },
})
