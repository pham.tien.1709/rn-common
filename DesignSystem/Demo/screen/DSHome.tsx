import {
  Container,
  Text,
  Divider,
  HStack,
  Layout,
  Icon,
  List,
  Input
} from '@ui-kit/components'
import React, {useEffect, useRef, useState} from 'react'
import {LayoutAnimation, Pressable, StyleSheet} from 'react-native'
import {DSScreen} from '../const'
import {
  colors,
  defaultColor,
  fontSize,
  iconSize,
  insets,
  space
} from '@ui-kit/theme'
import {useTheme} from '@ui-kit/hook'
import {navigate} from '@navigation/NavigationAction'
import {useDebounce} from '@hooks/useDebounce'
import _ from 'lodash-contrib'
import Animated, {FadeIn, FadeOut} from 'react-native-reanimated'

const data = Object.values(DSScreen)

const DSHome = () => {
  const [list, setList] = useState(data)
  const listRef = useRef<any>()

  const onSetList = (data: any[]) => {
    setList(data)
  }

  useEffect(() => {
    // This must be called before `LayoutAnimation.configureNext` in order for the animation to run properly.
    listRef.current?.prepareForLayoutAnimationRender()
    // After removing the item, we can start the animation.
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
  }, [data])

  const renderItem = ({item, index}: {item: string; index: number}) => (
    <Item index={index} item={item} />
  )

  return (
    <Container>
      <InputView setList={onSetList} />
      <List
        ref={listRef}
        ItemSeparatorComponent={Divider}
        data={list}
        keyboardDismissMode="on-drag"
        keyboardShouldPersistTaps="handled"
        renderItem={renderItem}
        style={style.list}
        contentContainerStyle={style.contentContainerStyle}
        keyExtractor={(item: string) => item}
        estimatedItemSize={16}
      />
    </Container>
  )
}

const onNavigate = (screen: string) => {
  navigate(screen)
}

const Item = ({item, index}: {item: string; index: number}) => {
  const {foreground} = useTheme()

  const isEven = index % 2 === 0

  return (
    <Animated.View exiting={FadeOut} entering={FadeIn}>
      <Pressable
        onPress={() => onNavigate(item)}
        style={[
          style.item,
          {
            backgroundColor: isEven ? colors.background2 : colors.background
          }
        ]}
      >
        <HStack>
          <HStack gap={space.xs} style={style.text}>
            <Text style={style.emoji}>{emoji?.[index] ?? emoji[0]}</Text>
            <Text fontSize="xl" style={style.text}>
              {item}
            </Text>
          </HStack>

          <Icon icon="arrow-right" size={iconSize.xs} color={foreground} />
        </HStack>
      </Pressable>
    </Animated.View>
  )
}

const InputView = ({setList}: {setList: (data: any[]) => void}) => {
  const [searchTerm, setSearchTerm] = useState('')
  const inputRef = useRef<any>()
  const search = useDebounce(searchTerm, 300)

  useEffect(() => {
    if (search?.trim?.() !== '') {
      setList(data.filter(item => _.strContains(item, search)))
    } else {
      setList(data)
    }
  }, [search])

  const onClear = () => {
    setSearchTerm('')
    inputRef.current?.focus?.()
  }

  return (
    <Layout style={style.inputView}>
      <Input
        ref={inputRef}
        icon="search"
        RightItem={() =>
          searchTerm?.trim?.() !== '' && (
            <Pressable onPress={onClear}>
              <Icon
                size={iconSize.sm}
                icon="cross"
                color={defaultColor[600]}
              />
            </Pressable>
          )
        }
        iconColor={defaultColor[400]}
        style={style.input}
        value={searchTerm}
        onChangeText={setSearchTerm}
        placeholder="Find name?"
      />
      <Divider />
    </Layout>
  )
}

export default DSHome

const style = StyleSheet.create({
  item: {
    padding: space.md
  },
  list: {
    margin: space.md,
    borderRadius: space.md
  },
  text: {
    fontWeight: '600',
    flex: 1
  },

  contentContainerStyle: {
    paddingBottom: insets.bottom
  },
  input: {
    margin: space.md,
    backgroundColor: defaultColor[50]
  },
  inputView: {
    // padding: space.md,
  },
  emoji: {
    fontSize: fontSize['2xl']
  }
})

const emoji = [
  '🍑',
  '🙈',
  '🤖',
  '🦾',
  '🫵',
  '🥷',
  '💅',
  '🧶',
  '👘',
  '🧢',
  '👒',
  '🧣',
  '🐽',
  '🐷',
  '🦁',
  '🐼',
  '🐒',
  '🐧',
  '🐤',
  '🦄',
  '🦋',
  '🦉',
  '🦅',
  '🕸️',
  '🦑',
  '🐞'
]
