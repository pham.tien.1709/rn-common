import {Container} from '@ui-kit/components'
import React from 'react'
import {StyleSheet} from 'react-native'
import {defaultColor, primary, space} from '@ui-kit/theme'
import Switch from '@ui-kit/components/Switch'

const DSSwitch = () => {
  return (
    <Container style={styles.container}>
      <Switch activeColor={primary[50]} inActiveColor={defaultColor[400]} />
    </Container>
  )
}

export default DSSwitch

const styles = StyleSheet.create({
  container: {
    padding: space.md,
  },
})
