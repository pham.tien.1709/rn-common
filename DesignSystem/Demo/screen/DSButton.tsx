import {
  Container,
  Divider,
  Button,
  HStack,
  IconName,
  Icon,
} from '@ui-kit/components'
import React from 'react'
import {Alert, FlatList, StyleSheet, View} from 'react-native'
import {space} from '@ui-kit/theme'

const buttonType = ['solid', 'outline', 'ghost']
const icon = ['bank', 'add', 'check']

const DSButton = () => {
  const onPress = () => {
    Alert.alert('Pressed')
  }

  const renderItem = ({item, index}: any) => (
    <View style={styles.row}>
      <Button onPress={onPress} style={styles.button} type={item}>
        {item}
      </Button>
      <Button disabled style={styles.button} type={item}>
        Disabled
      </Button>
      <HStack gap={space.sm}>
        <Button style={styles.button} loading type={item}>
          Loading
        </Button>
        <Button
          onPress={onPress}
          style={styles.button}
          RightItem={({color}) => <Icon color={color} icon="bank" size={14} />}
          type={item}
        >
          With Item
        </Button>
        <Button
          onPress={onPress}
          style={styles.button}
          RightItem={({color}) => (
            <Icon color={color} icon={icon[index] as IconName} size={14} />
          )}
          type={item}
        />
      </HStack>
    </View>
  )

  const ListFooterComponent = () => {
    return <View style={styles.footerList}></View>
  }

  return (
    <Container>
      <FlatList
        ItemSeparatorComponent={() => <Divider style={styles.divider} />}
        data={buttonType}
        renderItem={renderItem}
        style={styles.list}
        ListFooterComponent={ListFooterComponent}
      />
    </Container>
  )
}

export default DSButton

const styles = StyleSheet.create({
  button: {
    // flex: 1,
  },
  list: {
    paddingVertical: space.md,
  },
  divider: {
    marginVertical: space.md,
  },
  row: {
    marginHorizontal: space.md,
    rowGap: space.sm,
  },
  footerList: {
    columnGap: space.md,
    padding: space.md,
  },
})
