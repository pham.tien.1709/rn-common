import {Container, Icon, HStack, Input, Text} from '@ui-kit/components'
import React, {useState} from 'react'
import {ScrollView, StyleSheet, View} from 'react-native'
import {colors, iconSize, space} from '@ui-kit/theme'

const buttonType = ['solid', 'outline', 'ghost']

const DSInput = () => {
  const [text, setText] = useState('')
  return (
    <Container>
      <ScrollView>
        <View style={styles.list}>
          <Input />
          <Input icon="lock" />
          <Input icon="lock" placeholder="Password" secureTextEntry />
          <Input error="Error" />
          <Input placeholder="Password" secureTextEntry error="Error" />
          <Input
            icon="lock"
            placeholder="Password"
            secureTextEntry
            error="Error"
          />
          <Input
            LeftItem={() => (
              <HStack gap={space['4xs']}>
                <Text>+855</Text>
                <Icon
                  icon="arrow-down"
                  size={iconSize.xs}
                  color={colors.primary}
                />
              </HStack>
            )}
            icon="phone"
            placeholder="Phone Number"
          />
          <Input
            LeftItem={() => (
              <HStack gap={space['4xs']}>
                <Text>+855</Text>
                <Icon
                  icon="arrow-down"
                  size={iconSize.xs}
                  color={colors.primary}
                />
              </HStack>
            )}
            icon="phone"
            placeholder="Phone Number"
            error="Error"
          />
          <Input
            value={text}
            onChangeText={setText}
            multiline
            icon="alarm"
            maxLength={500}
            showMaxLength
            placeholder="Message"
          />
          <Input multiline placeholder="Message" error="Error" />
        </View>
      </ScrollView>
    </Container>
  )
}

export default DSInput

const styles = StyleSheet.create({
  list: {
    padding: space.md,
    rowGap: space.md,
  },
})
