import {
  Badge,
  BottomView,
  Button,
  Container,
  HStack,
  Text,
} from '@ui-kit/components'
import React from 'react'
import {StyleSheet} from 'react-native'
import {space} from '@ui-kit/theme'
import {Notify, NotifyOptions, NotifyType} from '@ui-kit/common'
import {Status} from '@ui-kit/types'

const status: Status[] = ['info', 'danger', 'success', 'warning']

const type: NotifyType[] = ['solid', 'outline', 'subtle']

const duration = [3000, 5000, 7500]

const DSNotify = () => {
  const showAlert = (options: NotifyOptions) => {
    Notify.show({
      ...options,
      message: 'Both animation helper methods share a similar structure.',
      dismissOnClick: true,
    })
  }

  return (
    <Container style={styles.container}>
      <Container level={2} style={styles.content}>
        <HStack gap={space.sm}>
          {status.map(item => (
            <Button
              onPress={() =>
                showAlert({
                  title: 'Status',
                  status: item,
                })
              }
              key={item}
            >
              {item}
            </Button>
          ))}
        </HStack>
        <HStack gap={space.sm}>
          {type.map((item, index) => (
            <Button
              onPress={() =>
                showAlert({
                  title: 'Type',
                  status: status[index],
                  type: item,
                })
              }
              key={item}
            >
              {item}
            </Button>
          ))}
        </HStack>
        <HStack gap={space.sm}>
          {duration.map((item: number, index) => (
            <Button
              onPress={() =>
                showAlert({
                  title: 'Duration',
                  status: status[index],
                  duration: item,
                })
              }
              key={item}
            >
              {item / 1000 + 's'}
            </Button>
          ))}
        </HStack>
      </Container>
    </Container>
  )
}

export default DSNotify

const styles = StyleSheet.create({
  container: {},
  content: {
    justifyContent: 'center',
    alignItems: 'center',
    gap: space.md,
  },
})
