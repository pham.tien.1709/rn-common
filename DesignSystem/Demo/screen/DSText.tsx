import {Button, Container, HStack, Text} from '@ui-kit/components'
import React, {useState} from 'react'
import {StyleSheet, TextStyle, View} from 'react-native'
import {fontSize, space} from '@ui-kit/theme'

const locale = {
  en: 'Sorry, you can upload only one video',
  zh: '柬埔寨金边市 水净化区 ，Keo Chenda街，Kings View办公楼B座1楼， 。',
  km: 'បូករួមនៃលទ្ធផលដែលកើតមានឡើង'
}

const weight: TextStyle['fontWeight'][] = [
  '100',
  '200',
  '300',
  '400',
  '500',
  '600',
  '700',
  '800'
]
type LocaleType = 'en' | 'zh' | 'km'

const DSText = () => {
  const [text, changeText] = useState<LocaleType>('en')
  const [fontWeight, setFontWeight] = useState<TextStyle['fontWeight']>('400')

  const changeLocale = (locale: LocaleType) => {
    changeText(locale)
  }

  return (
    <Container style={styles.container}>
      <Text style={[styles.text, styles.en]}>Finished with error</Text>
      <Text style={[styles.text, styles.zh]}>
        为以及煽动这种歧视的任何行为之害
      </Text>
      <View style={styles.bottom}>
        <Text style={[styles.text]}>{locale?.[text] ?? ''}</Text>
        <HStack gap={space.md}>
          <Button
            style={styles.button}
            type={text === 'en' ? 'solid' : 'outline'}
            onPress={() => changeLocale('en')}
          >
            English
          </Button>
          <Button
            onPress={() => changeLocale('zh')}
            style={styles.button}
            type={text === 'zh' ? 'solid' : 'outline'}
          >
            Chinese
          </Button>
        </HStack>
        <HStack style={styles.weightButton}>
          {weight.map((item: TextStyle['fontWeight']) => {
            return (
              <Button
                type={fontWeight === item ? 'solid' : 'outline'}
                key={item}
                onPress={() => setFontWeight(item)}
              >
                {item}
              </Button>
            )
          })}
        </HStack>
      </View>
    </Container>
  )
}

export default DSText

const styles = StyleSheet.create({
  container: {
    padding: space.md,
    rowGap: space.md
  },
  text: {
    fontSize: fontSize['4xl']
  },
  en: {
    fontFamily: 'Inter',
    fontWeight: '900'
  },
  zh: {
    fontFamily: 'Noto Sans SC',
    fontWeight: '900'
  },
  button: {
    flex: 1
  },
  bottom: {
    marginTop: space['4xl'],
    gap: space.md
  },
  weightButton: {
    gap: space.xs,
    flexWrap: 'wrap'
  }
})
