import {Container, Image, ScalableImage, Text} from '@ui-kit/components'
import React from 'react'
import {ScrollView, StyleSheet} from 'react-native'
import {fontSize, screenSize, space} from '@ui-kit/theme'

const DSImage = () => {
  return (
    <Container style={styles.container}>
      <ScrollView>
        <Text style={styles.title}>Image:</Text>
        <Image
          style={styles.default}
          source={
            'https://images.unsplash.com/photo-1697128439428-a68faa7f2537?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw1MHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=500&q=60'
          }
        />
        <Text style={styles.title}>ScalableImage:</Text>
        <ScalableImage
          imageWidth={screenSize.width}
          source={
            'https://images.unsplash.com/photo-1697128439428-a68faa7f2537?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw1MHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=500&q=60'
          }
        />
        <Text style={styles.title}>Error:</Text>
        <Image style={styles.error} />
      </ScrollView>
    </Container>
  )
}

export default DSImage

const styles = StyleSheet.create({
  container: {},
  title: {
    padding: space.md,
    fontWeight: 'bold',
    fontSize: fontSize['3xl'],
  },
  default: {
    width: screenSize.width,
    height: screenSize.width / 2,
  },
  error: {
    width: screenSize.width,
    height: screenSize.width,
  },
})
