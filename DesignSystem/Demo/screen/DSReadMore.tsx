import {Container, ReadMore} from '@ui-kit/components'
import React from 'react'
import {StyleSheet} from 'react-native'
import {space} from '@ui-kit/theme'

const DSReadMore = () => {
  return (
    <Container style={styles.container}>
      <ReadMore numberOfLines={2}>
        I have a Node.js web application currently running on a server
        successfully. Now Im trying to set up a local copy on my development
        server. I currently have Node.js, NPM and MongoDB Installed just like
        what I have in production server. However, the error below occurs when I
        try to start the Node.js server. What could be causing this issue?
      </ReadMore>
    </Container>
  )
}

export default DSReadMore

const styles = StyleSheet.create({
  container: {
    padding: space.md,
  },
})

const data = [1, 2, 3, 4, 5, 6, 7, 8]
