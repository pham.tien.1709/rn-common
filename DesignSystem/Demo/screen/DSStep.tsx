import {
  Button,
  Container,
  HStack,
  StepIndicator,
} from '@ui-kit/components'
import {space} from '@ui-kit/theme'
import React, {useState} from 'react'
import {StyleSheet} from 'react-native'

const data = ['Services', 'Booking', 'Finish']

const DSStep = () => {
  const [step, setStep] = useState(0)

  const onNext = () => {
    if (step < data.length - 1) setStep(step + 1)
  }

  const onPrev = () => {
    if (step !== 0) setStep(step - 1)
  }

  return (
    <Container style={styles.container}>
      <StepIndicator currentIndex={step} data={data} />
      <HStack style={styles.row}>
        <Button type="outline" style={styles.buttonPrev} onPress={onPrev}>
          Previous
        </Button>
        <Button style={styles.button} onPress={onNext}>
          Next
        </Button>
      </HStack>
    </Container>
  )
}

export default DSStep

const styles = StyleSheet.create({
  container: {
    padding: space.md,
  },
  button: {
    flex: 1,
    marginLeft: space.sm,
  },
  buttonPrev: {},
  row: {
    marginTop: space.md,
  },
})
