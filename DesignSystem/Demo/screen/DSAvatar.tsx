import {Avatar, Container, HStack} from '@ui-kit/components'
import React from 'react'
import {StyleSheet} from 'react-native'
import {space} from '@ui-kit/theme'

const data = [
  {
    name: 'Gia Bao',
    source:
      'https://images.unsplash.com/photo-1695556406049-8ca4f2ff854c?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHwzOXx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=500&q=60',
    size: 'lg',
  },
  {
    name: 'Nguyen Van Teo',
    source: 'fail',
  },
  {
    name: 'Phan Van',
    source:
      'https://images.unsplash.com/photo-1695802060930-1cca61061922?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw0N3x8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=500&q=60',
    size: 'xs',
  },
  {
    name: 'Phan Van',
    source:
      'https://images.unsplash.com/photo-1695555864269-c071fc6b9457?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw3N3x8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=500&q=60',
  },
  {
    name: 'Nguyen Khac An',
    source:
      'https://images.unsplash.com/photo-1695556128448-1050e9b277e3?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxlZGl0b3JpYWwtZmVlZHw4NXx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=500&q=60',
  },
  {
    name: 'Tran Van Chuoi Ty',
    source: 'null',
    size: 'xl',
  },
]

const DSAvatar = () => {
  const renderAvatar = (item: any, index: number) => {
    return <Avatar key={`avatar-${index}`} {...item} />
  }

  return (
    <Container style={styles.container}>
      <HStack style={styles.row}>{data.map(renderAvatar)}</HStack>
    </Container>
  )
}

export default DSAvatar

const styles = StyleSheet.create({
  container: {
    padding: space.md,
  },
  row: {
    //
    flexWrap: 'wrap',
    gap: space.sm,
  },
})
