import {
  BottomSheet,
  BottomSheetRef,
  Button,
  Container,
  HStack,
  Input,
  Text
} from '@ui-kit/components'
import React, {useRef, useState} from 'react'
import {StyleSheet, View, Image, Pressable} from 'react-native'
import {defaultColor, iconSize, screenSize, space} from '@ui-kit/theme'

const DSBottomSheet = () => {
  const [value, setValue] = useState('')
  const sheetRef = useRef<BottomSheetRef>(null)
  const dynamicSheet = useRef<BottomSheetRef>(null)
  const inputSheetRef = useRef<BottomSheetRef>(null)

  const renderItem = (item: string) => {
    return (
      <Pressable key={item}>
        <Image source={{uri: item}} style={styles.item} />
      </Pressable>
    )
  }

  const onPress = () => {
    sheetRef.current?.present()
  }

  const onPressDynamic = () => {
    dynamicSheet.current?.present()
  }

  const onPressInput = () => {
    inputSheetRef.current?.present()
  }

  const renderLanguage = ({title, icon}: {title: string; icon: string}) => (
    <HStack style={styles.language} key={icon}>
      <Image style={styles.flag} source={{uri: icon}} />
      <Text style={styles.languageTitle}>{title}</Text>
    </HStack>
  )

  return (
    <Container style={styles.container}>
      <Button onPress={onPress}>Open Sticker</Button>
      <Button onPress={onPressDynamic}>Choose Language</Button>
      <Button onPress={onPressInput}>Input</Button>
      <BottomSheet
        enableDynamicSizing
        ref={inputSheetRef}
        title="Why won't my car start?"
      >
        <View style={styles.withInput}>
          <Text style={styles.des}>
            If the car makes a rapid clicking sound when you turn the key but
            won’t start. Its usually caused by a dying or dead battery, loose...
          </Text>
          <Input
            icon="instagram"
            inBottomSheet
            value={value}
            onChangeText={setValue}
          />
        </View>
      </BottomSheet>

      <BottomSheet
        enableDynamicSizing
        ref={dynamicSheet}
        title="Choose Language"
        subTitle="4+ languages"
      >
        <View style={[styles.content, styles.noWrap]}>
          {language.map(renderLanguage)}
        </View>
      </BottomSheet>
      <BottomSheet ref={sheetRef} title="Stickers">
        <HStack style={styles.content}>{stickers.map(renderItem)}</HStack>
      </BottomSheet>
    </Container>
  )
}

export default DSBottomSheet

const styles = StyleSheet.create({
  container: {
    padding: space.md,
    gap: space.md
  },
  content: {
    flexWrap: 'wrap',
    padding: space.sm,
    gap: space.md
  },
  item: {
    aspectRatio: 1 / 1,
    width: (screenSize.width - space.md * 5) / 4
  },
  languageTitle: {
    fontWeight: '500'
  },
  flag: {
    width: iconSize.lg,
    height: iconSize.lg
  },
  language: {
    //
    gap: space.sm
  },
  des: {
    color: defaultColor[700]
  },
  noWrap: {
    //
    flexWrap: 'wrap'
  },
  withInput: {
    padding: space.md,
    gap: space.md
  }
})

const language = [
  {
    title: 'Việt Nam',
    icon: 'https://cdn-icons-png.flaticon.com/128/323/323319.png'
  },
  {
    title: 'Chinese',
    icon: 'https://cdn-icons-png.flaticon.com/128/197/197375.png'
  },
  {
    title: 'Korean',
    icon: 'https://cdn-icons-png.flaticon.com/128/197/197582.png'
  },
  {
    title: 'Belgium',
    icon: 'https://cdn-icons-png.flaticon.com/128/197/197583.png'
  },
  {
    title: 'Brazil',
    icon: 'https://cdn-icons-png.flaticon.com/128/197/197386.png'
  },
  {
    title: 'UK',
    icon: 'https://cdn-icons-png.flaticon.com/128/197/197374.png'
  },
  {
    title: 'Canada',
    icon: 'https://cdn-icons-png.flaticon.com/128/197/197430.png'
  }
]

const stickers = [
  'https://tlgrm.eu/_/stickers/351/df7/351df72b-aa79-4692-bd9d-4eb5a72d202a/192/5.webp',
  'https://tlgrm.eu/_/stickers/351/df7/351df72b-aa79-4692-bd9d-4eb5a72d202a/192/3.webp',
  'https://tlgrm.eu/_/stickers/351/df7/351df72b-aa79-4692-bd9d-4eb5a72d202a/192/26.webp',
  'https://tlgrm.eu/_/stickers/351/df7/351df72b-aa79-4692-bd9d-4eb5a72d202a/192/24.webp',
  'https://tlgrm.eu/_/stickers/351/df7/351df72b-aa79-4692-bd9d-4eb5a72d202a/192/20.webp',
  'https://tlgrm.eu/_/stickers/351/df7/351df72b-aa79-4692-bd9d-4eb5a72d202a/192/8.webp',
  'https://tlgrm.eu/_/stickers/351/df7/351df72b-aa79-4692-bd9d-4eb5a72d202a/192/13.webp'
]

const carousel = [
  'https://img.freepik.com/free-psd/gradient-world-health-day-template_23-2149308132.jpg?size=626&ext=jpg',
  'https://img.freepik.com/free-psd/liquid-banner-template_23-2148897228.jpg?size=626&ext=jpg',
  'https://img.freepik.com/free-vector/modern-gradient-background-blurred_125540-1213.jpg?size=626&ext=jpg&ga=GA1.1.1349704801.1696926484&semt=ais',
  'https://img.freepik.com/free-psd/world-health-day-banner-template_23-2149323685.jpg?size=626&ext=jpg&ga=GA1.1.1349704801.1696926484&semt=ais',
  'https://img.freepik.com/free-psd/liquid-banner-template_23-2148897230.jpg?size=626&ext=jpg&ga=GA1.1.1349704801.1696926484&semt=ais',
  'https://img.freepik.com/free-vector/gradient-abstract-fluid-technology-sale-background_23-2149166139.jpg?size=626&ext=jpg&ga=GA1.1.1349704801.1696926484&semt=ais'
]
