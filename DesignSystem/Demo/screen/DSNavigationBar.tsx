import {
  Container,
  NavigationBar,
  StatusBar,
  Text,
  Icon,
} from '@ui-kit/components'
import React from 'react'
import {ScrollView, StyleSheet, View} from 'react-native'
import {iconSize, space} from '@ui-kit/theme'
import {useTheme} from '@ui-kit/hook'

const DSNavigationBar = () => {
  const {theme} = useTheme()
  return (
    <Container level={2}>
      <StatusBar
        barStyle={theme === 'light' ? 'dark-content' : 'light-content'}
      />
      <NavigationBar title="Navigation Bar" />

      <ScrollView>
        <NavigationBar
          title="Polite Tech"
          RightItem={() => <Text>Filter</Text>}
        />
        <NavigationBar
          title="Navigation Bar"
          RightItem={({color}) => (
            <Icon icon="copy" size={iconSize.md} color={color} />
          )}
        />
        <NavigationBar title="Rental Car" subTitle="10 items" />
      </ScrollView>
    </Container>
  )
}

export default DSNavigationBar

const styles = StyleSheet.create({})
