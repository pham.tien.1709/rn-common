import {AlertPopup, Button, Container, Input} from '@ui-kit/components'
import React, {useRef} from 'react'
import {StyleSheet} from 'react-native'
import {space} from '@ui-kit/theme'
import {closeModal, openAlertPopup} from '@ui-kit/common'

const DSAlertPopup = () => {
  const ref = useRef(null)
  const openError = () => {
    openAlertPopup({
      title: 'Not enough points',
      status: 'warning',
      message: 'You do not have enough points to redeem this reward',
      confirm: 'OK',
      onConfirm: () => closeModal('AlertPopup'),
    })
  }

  const openSuccess = () => {
    openAlertPopup({
      status: 'success',
      message: 'You do not have enough points to redeem this reward',
      confirm: 'OK',
      cancel: 'Cancel',
      onConfirm: () => closeModal('AlertPopup'),
    })
  }

  const openModalInput = () => {
    ref.current?.open()
  }

  return (
    <Container level={'2'} style={styles.container}>
      <Button onPress={openError}>ERROR</Button>
      <Button onPress={openSuccess}>SUCCESS</Button>
      <Button onPress={openModalInput}>Input</Button>
      <AlertPopup confirm="OK" ref={ref} title="Enter Your Name">
        <Input />
      </AlertPopup>
    </Container>
  )
}

export default DSAlertPopup

const styles = StyleSheet.create({
  container: {
    padding: space.md,
    rowGap: space.md,
  },
})
