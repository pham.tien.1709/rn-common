import {Badge, Container, Slider, Text} from '@ui-kit/components'
import React from 'react'
import {StyleSheet, Text as RNText} from 'react-native'
import {fontSize, iconSize, space} from '@ui-kit/theme'
import {useTheme} from '@ui-kit/hook'

const DSSlider = () => {
  return (
    <Container style={styles.container}>
      <Slider
        value={[0, 20]}
        minimumValue={0}
        maximumValue={100}
        step={10}
        renderBubble={({value}) => (
          <Text style={{color: 'white'}}>${value}</Text>
        )}
      />

      <Slider
        style={{marginTop: space['4xl']}}
        value={[0, 1000]}
        minimumValue={0}
        maximumValue={1000}
        step={1}
      />
    </Container>
  )
}

export default DSSlider

const styles = StyleSheet.create({
  container: {
    padding: space.md,
  },
})
