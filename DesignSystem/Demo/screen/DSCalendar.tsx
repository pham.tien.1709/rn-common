import {Badge, Button, Container} from '@ui-kit/components'
import React, {useRef, useState} from 'react'
import {StyleSheet, Text as RNText, Text} from 'react-native'
import {fontSize, iconSize, space} from '@ui-kit/theme'
import {openCalendar, openModal} from '@ui-kit/common'

const DSCalendar = () => {
  const [date, setDate] = useState('')
  const open = () => {
    openCalendar({
      title: 'Calendar',
      onConfirm: day => setDate(day.dateString),
    })
  }

  return (
    <Container style={styles.container}>
      <Text>{date}</Text>
      <Button onPress={open}>Open</Button>
    </Container>
  )
}

export default DSCalendar

const styles = StyleSheet.create({
  container: {
    padding: space.md,
    rowGap: space.md,
  },
})
