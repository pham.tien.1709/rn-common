import {
  BottomView,
  Button,
  Container,
  HStack,
  Input,
  Radio,
  Text,
} from '@ui-kit/components'
import React from 'react'
import {Alert, ScrollView, StyleSheet, View} from 'react-native'
import {fontSize, space} from '@ui-kit/theme'

const DSBottomView = () => {
  const onPress = () => {
    Alert.alert('Pressed')
  }

  return (
    <Container>
      <ScrollView style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.title}>Login</Text>
          <Input icon="phone" placeholder="Phone Number" />
          <Input icon="locked" placeholder="Password" secureTextEntry />
        </View>
      </ScrollView>
      <BottomView avoidKeyboard>
        <Button onPress={onPress}>Bottom View</Button>
      </BottomView>
    </Container>
  )
}

export default DSBottomView

const styles = StyleSheet.create({
  container: {
    padding: space.md,
    rowGap: space.md,
  },
  title: {
    fontWeight: '700',
    fontSize: fontSize['3xl'],
  },
  header: {
    rowGap: space.md,
  },
})
