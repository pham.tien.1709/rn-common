import {
  Badge,
  Card,
  Container,
  HStack,
  List,
  NavigationBar,
  Text,
} from '@ui-kit/components'
import React from 'react'
import {StyleSheet, Text as RNText, View} from 'react-native'
import {colors, fontSize, iconSize, space} from '@ui-kit/theme'
import {useTheme} from '@ui-kit/hook'

const DSInformationCard = () => {
  const renderCard = () => {
    return (
      <Card style={styles.card}>
        <Card.Header style={styles.header}>
          <View>
            <Text style={styles.title}>Period: 07/2023</Text>
            <Text muted>
              Date: <Text>22/04/2023</Text>
            </Text>
          </View>
          <Badge size="sm" status="success">
            Paid
          </Badge>
        </Card.Header>
        <HStack style={styles.bottom}>
          <Text muted>
            Qty: <Text>4 products</Text>
          </Text>
          <Text muted>
            Total: <Text>$65,000</Text>
          </Text>
        </HStack>
      </Card>
    )
  }
  return (
    <Container style={styles.container}>
      <NavigationBar title="Payment" />
      <Container level={'2'}>
        <List data={data} renderItem={renderCard} estimatedItemSize={104} />
      </Container>
    </Container>
  )
}

export default DSInformationCard

const styles = StyleSheet.create({
  container: {},
  card: {
    marginHorizontal: space.md,
    marginTop: space.md,
  },
  header: {
    alignItems: 'flex-start',
  },
  title: {
    fontWeight: 'bold',
    color: colors.primary,
  },

  bottom: {
    justifyContent: 'space-between',
  },
})

const data = Array(1000).fill(0)
