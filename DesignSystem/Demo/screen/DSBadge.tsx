import {
  Badge,
  BadgeType,
  Container,
  Divider,
  HStack,
  Text,
} from '@ui-kit/components'
import React from 'react'
import {StyleSheet, View} from 'react-native'
import {fontSize, space} from '@ui-kit/theme'
import {Status} from '@ui-kit/types'

const badgeStatus: Status[] = [
  'primary',
  'default',
  'success',
  'warning',
  'danger',
  'info',
]

const badgeType: BadgeType[] = ['solid', 'outline', 'subtle']

const DSBadge = () => {
  return (
    <Container style={styles.container}>
      {badgeType.map(type => (
        <View style={styles.content} key={type}>
          <Text style={styles.title}>{type}</Text>
          <HStack style={styles.row} gap={space.sm}>
            {badgeStatus.map((item: Status) => (
              <Badge status={item} key={item} type={type}>
                {item}
              </Badge>
            ))}
          </HStack>
          <Divider />
        </View>
      ))}

      <View style={styles.content}>
        <Text style={styles.title}>Small Size</Text>
        <HStack style={styles.row} gap={space.sm}>
          {badgeStatus.map((item: Status) => (
            <Badge status={item} key={item} size="sm">
              {item}
            </Badge>
          ))}
        </HStack>
        <Divider />
      </View>
    </Container>
  )
}

export default DSBadge

const styles = StyleSheet.create({
  container: {
    padding: space.md,
    rowGap: space.md,
  },
  content: {
    rowGap: space.md,
  },
  row: {
    flexWrap: 'wrap',
  },
  title: {
    fontSize: fontSize.lg,
    textTransform: 'uppercase',
    fontWeight: '600',
  },
})
