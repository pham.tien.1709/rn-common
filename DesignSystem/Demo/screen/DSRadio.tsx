import {Container, HStack, Radio, Text} from '@ui-kit/components'
import React, {useState} from 'react'
import {StyleSheet, View} from 'react-native'
import {space} from '@ui-kit/theme'

const DSRadio = () => {
  const [checkList, setCheckList] = useState([
    false,
    false,
    true,
    false,
    true,
    false,
  ])

  const onCheck = (index: number, value: boolean) => {
    const list = [...checkList]
    list[index] = value
    setCheckList(list)
  }

  return (
    <Container style={styles.container}>
      <Radio checked={checkList[0]} onCheck={value => onCheck(0, value)} />
      <Radio checked={checkList[1]} onCheck={value => onCheck(1, value)}>
        System Design
      </Radio>
      <Radio checked={checkList[2]} onCheck={value => onCheck(2, value)}>
        <View>
          <Text style={styles.title}>Skeleton</Text>
          <Text fontSize={'sm'}>React native reanimated skeleton</Text>
        </View>
      </Radio>
      <Radio
        disabled
        checked={checkList[3]}
        onCheck={value => onCheck(3, value)}
      >
        Disabled
      </Radio>
      <Radio
        disabled
        checked={checkList[4]}
        onCheck={value => onCheck(4, value)}
      >
        Radio Disabled
      </Radio>
      <HStack style={styles.stack}>
        <Radio
          size="sm"
          checked={checkList[5]}
          onCheck={value => onCheck(5, value)}
        />
        <Radio checked={checkList[5]} onCheck={value => onCheck(5, value)} />
        <Radio
          size="lg"
          checked={checkList[5]}
          onCheck={value => onCheck(5, value)}
        />
      </HStack>
    </Container>
  )
}

export default DSRadio

const styles = StyleSheet.create({
  container: {
    padding: space.md,
    rowGap: space.md,
  },
  title: {
    fontWeight: 'bold',
    marginBottom: 2,
  },
  stack: {
    alignItems: 'flex-end',
    columnGap: space.md,
  },
})
