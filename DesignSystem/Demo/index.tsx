import React from 'react'
import {createNativeStackNavigator} from '@react-navigation/native-stack'

import {DSScreen} from './const'
import DSButton from './screen/DSButton'
import DSInput from './screen/DSInput'
import DSNavigationBar from './screen/DSNavigationBar'
import DSAvatar from './screen/DSAvatar'
import DSGroup from './screen/DSGroup'
import DSStep from './screen/DSStep'
import DSBadge from './screen/DSBadge'
import DSCheckBox from './screen/DSCheckBox'
import DSRadio from './screen/DSRadio'
import DSBottomView from './screen/DSBBottomView'
import DSText from './screen/DSText'
import DSBottomSheet from './screen/DSBottomSheet'
import DSNotify from './screen/DSNotify'
import DSCalendar from './screen/DSCalendar'
import DSSlider from './screen/DSSlider'
import DSInformationCard from './screen/DSInformationCard'
import DSAlertPopup from './screen/DSAlertPopup'
import DSReadMore from './screen/DSReadMore'
import DSImage from './screen/DSImage'
import DSSwitch from './screen/DSSwitch'
import {NavigationBar} from '@ui-kit/components'
import DSHome from './screen/DSHome'

const {Screen, Navigator} = createNativeStackNavigator()

const DesignSystemNavigator = () => {
  return (
    <Navigator>
      <Screen
        name={'Home'}
        options={{
          title: 'Design System',
          header: props => <NavigationBar {...props} />
        }}
        component={DSHome}
      />
      <Screen name={DSScreen.Button} component={DSButton} />
      <Screen name={DSScreen.Avatar} component={DSAvatar} />
      <Screen name={DSScreen.Image} component={DSImage} />
      <Screen name={DSScreen.Input} component={DSInput} />
      <Screen name={DSScreen.Group} component={DSGroup} />
      <Screen name={DSScreen.StepIndicator} component={DSStep} />
      <Screen name={DSScreen.Badge} component={DSBadge} />
      <Screen name={DSScreen.CheckBox} component={DSCheckBox} />
      <Screen name={DSScreen.Radio} component={DSRadio} />
      <Screen name={DSScreen.BottomView} component={DSBottomView} />
      <Screen name={DSScreen.BottomSheet} component={DSBottomSheet} />
      <Screen name={DSScreen.Notify} component={DSNotify} />
      <Screen name={DSScreen.Calendar} component={DSCalendar} />
      <Screen name={DSScreen.Slider} component={DSSlider} />
      <Screen name={DSScreen.AlertPopup} component={DSAlertPopup} />
      <Screen name={DSScreen.ReadMore} component={DSReadMore} />
      <Screen name={DSScreen.Text} component={DSText} />
      <Screen name={DSScreen.Switch} component={DSSwitch} />

      <Screen
        name={DSScreen['Information Card']}
        component={DSInformationCard}
        options={{
          header: () => null
        }}
      />

      <Screen
        name={DSScreen.NavigationBar}
        options={{
          header: () => null
        }}
        component={DSNavigationBar}
      />
    </Navigator>
  )
}

export default DesignSystemNavigator
