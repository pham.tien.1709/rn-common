import {StyleSheet, TouchableWithoutFeedback} from 'react-native'
import React, {
  useState,
  useEffect,
  useImperativeHandle,
  forwardRef,
} from 'react'
import Animated, {
  interpolateColor,
  useSharedValue,
  useAnimatedStyle,
  withSpring,
  withTiming,
  useDerivedValue,
} from 'react-native-reanimated'
import {defaultColor, space} from '@ui-kit/theme'

export interface SwitchRef {
  getValue: () => boolean
  clear: () => void
  setValue: (value: boolean) => void
}
export interface SwitchProps {
  activeColor: string
  inActiveColor: string
  activeValue?: boolean
  onChangeValue?: (value: boolean) => void
  circleStyle?: any
}

const Switch: React.ForwardRefRenderFunction<SwitchRef, SwitchProps> = (
  {
    activeColor,
    inActiveColor,
    activeValue = false,
    circleStyle = {},
    onChangeValue,
  },
  ref
) => {
  // value for Switch Animation
  const switchTranslate = useSharedValue(0)
  // state for activate Switch
  const [active, setActive] = useState(activeValue)
  // Progress Value
  const progress = useDerivedValue(() => {
    return withTiming(active ? 26 : 0)
  })

  useImperativeHandle(ref, () => ({
    getValue: () => active,
    setValue: (status: boolean) => {
      setActive(status)
    },
    clear: () => {
      setActive(false)
    },
  }))

  // useEffect for change the switchTranslate Value
  useEffect(() => {
    if (active) {
      switchTranslate.value = 26
    } else {
      switchTranslate.value = -2
    }
  }, [active, switchTranslate])

  // Circle Animation
  const customSpringStyles = useAnimatedStyle(() => {
    return {
      transform: [
        {
          translateX: withSpring(switchTranslate.value, {
            mass: 1,
            damping: 15,
            stiffness: 120,
            overshootClamping: false,
            restSpeedThreshold: 0.001,
            restDisplacementThreshold: 0.001,
          }),
        },
      ],
    }
  })

  // Background Color Animation
  const backgroundColorStyle = useAnimatedStyle(() => {
    const backgroundColor = interpolateColor(
      progress.value,
      [0, 26],
      [inActiveColor, activeColor]
    )
    return {
      backgroundColor,
    }
  })

  return (
    <TouchableWithoutFeedback
      onPress={() => {
        const nextValue = !active
        setActive(nextValue)
        if (onChangeValue) {
          onChangeValue(nextValue)
        }
      }}
    >
      <Animated.View style={[styles.container, backgroundColorStyle]}>
        <Animated.View
          style={[styles.circle, customSpringStyles, circleStyle]}
        />
      </Animated.View>
    </TouchableWithoutFeedback>
  )
}

export default forwardRef(Switch)

const styles = StyleSheet.create({
  container: {
    width: 45,
    height: space.md,
    borderRadius: 30,
    justifyContent: 'center',
    backgroundColor: defaultColor[400],
  },
  circle: {
    width: space.xl,
    height: space.xl,
    borderRadius: 30,
    backgroundColor: 'white',
    shadowColor: 'black',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.2,
    shadowRadius: 2.5,
    elevation: 4,
  },
})
