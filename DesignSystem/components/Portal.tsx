import {ModalProps} from 'react-native-modalfy'

const Portal = ({modal: {getParam}}: ModalProps<'Portal'>) => {
  const children = getParam('children')

  return children
}

export default Portal
