import {
  View,
  Text,
  TextInput,
  TextInputProps,
  StyleSheet,
  Pressable,
  ViewProps,
} from 'react-native'
import React, {FC, forwardRef} from 'react'
import {IconName, Icon} from './Icon'
import {HStack} from './HStack'
import {
  colors,
  defaultColor,
  danger,
  fontSize,
  iconSize,
  screenSize,
  space,
  trueGray,
  primary,
} from '@ui-kit/theme'
import {useToggle} from '@ui-kit/hook'
import {BottomSheetTextInput} from '@gorhom/bottom-sheet'
import {TextProps} from './Text'
import {IS_IOS} from '@ui-kit/constant'

export interface InputProps extends TextInputProps {
  RightItem?: FC
  LeftItem?: FC
  icon?: IconName
  error?: string | boolean
  inputViewStyle?: ViewProps['style']
  inputStyle?: TextInputProps['style']
  message?: string
  inBottomSheet?: boolean
  iconColor?: string
  title?: string
  titleProps?: TextProps
  showMaxLength?: boolean
  maxLength?: number
  onChangeText?: (text: string) => void
  borderColor?: string
}

export interface InputRef {
  //
}

export const Input = forwardRef<any, InputProps>((props, ref) => {
  const {
    icon,
    RightItem,
    LeftItem,
    error,
    secureTextEntry = false,
    placeholder = 'Enter',
    multiline,
    inBottomSheet,
    title,
    iconColor,
    editable = true,
    titleProps,
    showMaxLength,
    maxLength,
    borderColor,
    inputViewStyle = {},
  } = props

  const [secureVisible, toggleSecure] = useToggle(secureTextEntry)
  const message = typeof error === 'string' ? error : props?.message

  const inputProps: InputProps = {
    placeholderTextColor: defaultColor[400],
    ...props,
    placeholder,
    style: [
      styles.inputStyle,
      multiline && styles.multiple,
      !editable && {color: colors.black},
      !editable && inputViewStyle,
    ],
    secureTextEntry: secureVisible,
  }

  return (
    <View style={props.style}>
      {title ? (
        <Text {...titleProps} style={[styles.titleStyle, titleProps?.style]}>
          {title}
        </Text>
      ) : null}
      <HStack
        style={[
          styles.container,
          !editable && {backgroundColor: defaultColor[100]},
          multiline && styles.multipleView,
          inputViewStyle,
          {
            borderColor: error
              ? danger[400]
              : borderColor
                ? borderColor
                : defaultColor[200],
          },
        ]}
      >
        {icon ? (
          <Icon icon={icon} color={iconColor} size={iconSize.sm} />
        ) : null}
        {LeftItem ? <LeftItem /> : null}
        <View
          style={[
            styles.inputWrapper,
            multiline && styles.multiple,
            multiline &&
              !icon &&
              !LeftItem && {
                paddingHorizontal: space.xs,
              },
          ]}
        >
          {inBottomSheet ? (
            <BottomSheetTextInput ref={ref} {...inputProps} />
          ) : (
            <TextInput
              ref={ref}
              placeholderTextColor={defaultColor[400]}
              {...inputProps}
            />
          )}
        </View>
        {RightItem ? (
          <RightItem />
        ) : secureTextEntry ? (
          <Pressable onPress={toggleSecure}>
            <Icon
              size={iconSize.sm}
              icon={secureVisible ? 'eye-invisible' : 'eye'}
              color={trueGray[300]}
            />
          </Pressable>
        ) : null}
      </HStack>
      {(!!message || (maxLength && showMaxLength)) && (
        <HStack style={styles.messageWrap}>
          <Text
            style={[
              styles.message,
              {color: error ? danger[400] : defaultColor[400]},
            ]}
          >
            {message}
          </Text>
          {maxLength && showMaxLength && (
            <Text style={styles.maxLengthText}>
              {`${
                inBottomSheet
                  ? props?.defaultValue?.length ?? 0
                  : props?.value?.length ?? 0
              }/${maxLength}`}
            </Text>
          )}
        </HStack>
      )}
    </View>
  )
})

export default Input

Input.displayName = 'Input'

const lineHeight = Math.round(fontSize.md * 1.5)

const styles = StyleSheet.create({
  titleStyle: {
    marginBottom: 8,
    fontSize: fontSize.md,
    fontWeight: '700',
    color: primary[800],
  },
  container: {
    borderWidth: 1,
    columnGap: space.xs,
    padding: space.xs,
    borderRadius: space['3xs'],
    borderColor: defaultColor[200],
    minHeight: iconSize.sm,
  },
  inputStyle: {
    flex: 1,
    fontSize: fontSize.md,
    paddingVertical: 0,
    paddingHorizontal: 0,
    fontFamily: 'Inter',
    textAlignVertical: 'center',
    verticalAlign: 'top',
  },
  inputWrapper: {
    flex: 1,
    height: lineHeight,
  },
  message: {
    fontSize: fontSize.xs,
    marginLeft: space['3xs'],
  },
  multiple: {
    height: screenSize.width / 3,
    textAlignVertical: 'top',
    lineHeight,
    marginTop: IS_IOS ? -3 : 0,
    // backgroundColor: 'red',
  },
  multipleView: {
    alignItems: 'flex-start',
  },
  messageWrap: {
    justifyContent: 'space-between',
    marginTop: space.xs,
  },
  maxLengthText: {
    fontStyle: 'italic',
    fontSize: fontSize.sm,
    color: defaultColor[400],
  },
})
