import React, {ReactNode, useState} from 'react'
import {defaultColor, primary, space} from '@ui-kit/theme'
import {StyleSheet, View, ViewProps} from 'react-native'
import {Button} from './Button'
import {HStack} from './HStack'
import {Icon} from './Icon'
import {useTranslate} from '@i18n/hook/useTranslate'
import {Text} from '@ui-kit/components/Text'
import {RNLayoutAnimation} from '@utils/animation'

interface IButtonViewMore extends ViewProps {
  reactElement: ReactNode
  viewMoreText?: string
  viewLessText?: string
  callbackPress?: (value: boolean) => void
  isOpen?: boolean
}

const ButtonViewMore: React.FC<IButtonViewMore> = ({
  reactElement,
  viewMoreText = 'View more',
  viewLessText,
  isOpen = false,
  ...rest
}) => {
  const defaultViewLessText = useTranslate('global.viewLess')
  const [isViewAll, setViewAll] = useState<boolean>(isOpen)
  const handleViewAll = () => {
    RNLayoutAnimation()
    setViewAll(state => !state)
    rest?.callbackPress?.(!isViewAll)
  }

  return (
    <View {...rest}>
      {isViewAll ? <View>{reactElement}</View> : null}
      {/* <Animated.View style={opacityAnimatedStyle}>{reactElement}</Animated.View> */}
      <Button style={styles.button} onPress={handleViewAll}>
        <HStack>
          <Text style={styles.buttonText}>
            {!isViewAll ? viewMoreText : viewLessText ?? defaultViewLessText}
          </Text>
          <Icon
            size={18}
            icon={!isViewAll ? 'down' : 'up'}
            color={primary[800]}
          />
        </HStack>
      </Button>
    </View>
  )
}

const styles = StyleSheet.create({
  button: {
    backgroundColor: defaultColor[100],
    borderRadius: 4,
    paddingVertical: 10,
    marginTop: 24,
    alignItems: 'center',
    marginHorizontal: space.md,
  },
  buttonText: {fontWeight: '700', marginRight: 5, color: primary[800]},
})

export default ButtonViewMore
