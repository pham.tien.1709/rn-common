import React, {LegacyRef, forwardRef} from 'react'
import {Layout, LayoutProps} from './Layout'
import {StyleSheet, View} from 'react-native'
import {colors, space} from '@ui-kit/theme'

export interface CardProps extends LayoutProps {
  //
}

const Card = forwardRef<LegacyRef<any>, CardProps>((props, ref) => {
  return (
    <Layout {...props} style={[styles.container, props?.style]}>
      {props.children}
    </Layout>
  )
})

const Header = forwardRef<LegacyRef<any>, CardProps>(
  (props, ref: LegacyRef<any>) => {
    return (
      <View ref={ref} {...props} style={[styles.header, props?.style]}>
        {props.children}
      </View>
    )
  }
)

Card.displayName = 'Card'

const styles = StyleSheet.create({
  container: {
    borderRadius: space['3xs'],
    paddingHorizontal: space.md,
    paddingVertical: space.sm,
  },
  header: {
    paddingHorizontal: space.md,
    marginBottom: space.xs,
    paddingBottom: space.xs,
    marginHorizontal: -space.md,
    borderBottomWidth: StyleSheet.hairlineWidth + 0.5,
    borderBottomColor: colors.divider,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
})

export default Object.assign(Card, {
  Header,
})
