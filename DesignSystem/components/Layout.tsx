import React, {forwardRef} from 'react'
import {View, ViewProps} from 'react-native'
import {ColorType, Level} from '@ui-kit/types'
import {useTheme} from '@ui-kit/hook'

export interface LayoutProps extends ViewProps {
  level?: Level
}

export const Layout = forwardRef<any, LayoutProps>((props, ref) => {
  const {children, level = 1} = props
  const theme = useTheme()

  const backgroundColor =
    level > 1
      ? theme[`background${level}` as keyof ColorType]
      : theme.background

  return (
    <View ref={ref} {...props} style={[{backgroundColor}, props?.style]}>
      {children}
    </View>
  )
})

Layout.displayName = 'Layout'
