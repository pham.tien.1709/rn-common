import {BottomSheetModalProvider} from '@gorhom/bottom-sheet'
import React, {ReactNode} from 'react'
import {ModalProvider} from 'react-native-modalfy'
import {SafeAreaProvider} from 'react-native-safe-area-context'
import {Notify, modalStack} from '../common'
import {NotifyView} from './Notify'

export const Provider = ({children}: {children: ReactNode}) => {
  return (
    <SafeAreaProvider>
      <BottomSheetModalProvider>
        <ModalProvider stack={modalStack}>
          {children}
          <NotifyView ref={Notify.notifyRef} />
        </ModalProvider>
      </BottomSheetModalProvider>
    </SafeAreaProvider>
  )
}
