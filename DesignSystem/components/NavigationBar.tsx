import {View, StyleSheet, GestureResponderEvent, Pressable} from 'react-native'
import React, {FC} from 'react'
import {Layout, LayoutProps} from './Layout'
import {useTheme} from '@ui-kit/hook'
import {
  colors,
  defaultColor,
  fontSize,
  iconSize,
  insets,
  space,
} from '@ui-kit/theme'
import {Text, TextProps} from './Text'
import {ParamListBase, Route, useNavigation} from '@react-navigation/native'
import {Icon} from './Icon'
import {
  NativeStackHeaderProps,
  NativeStackNavigationOptions,
  NativeStackNavigationProp,
} from '@react-navigation/native-stack'

type AccessoryItem = {
  color: string
}

interface Options extends NativeStackNavigationOptions {
  subTitle?: string
  hideTitle?: boolean
  level?: LayoutProps['level']
  hasDivider?: boolean
}

export interface NavigationBarProps
  extends LayoutProps,
    Omit<NativeStackHeaderProps, 'options' | 'navigation' | 'route'> {
  title?: string
  subTitle?: string
  RightItem?: FC<AccessoryItem>
  LeftItem?: FC<AccessoryItem>
  titleProps?: TextProps
  /**
   * override Navigation Bar's onBack default
   */
  onBack?: (e: GestureResponderEvent) => void
  hideBack?: boolean
  options?: Options
  /**
   * Route object for the current screen.
   */
  route?: Route<string>
  /**
   * Navigation prop for the header.
   */
  navigation?: NativeStackNavigationProp<ParamListBase>
  hasDivider?: boolean
}

export const NavigationBar = (props: NavigationBarProps) => {
  const {RightItem, titleProps, hideBack = false, LeftItem} = props
  const {foreground} = useTheme()
  const {goBack} = useNavigation()

  const subTitle = props?.subTitle ?? props.options?.subTitle
  const title = props?.title ?? props.options?.title
  const hasDivider = props?.hasDivider ?? props.options?.hasDivider

  const onBack = (e: GestureResponderEvent) => {
    if (typeof props?.onBack === 'function') return props?.onBack?.(e)
    goBack()
  }

  return (
    <Layout
      {...props}
      style={[styles.container, hasDivider && styles.divider, props?.style]}
    >
      {LeftItem ? (
        <View style={styles.item}>
          {LeftItem ? LeftItem({color: foreground}) : null}
        </View>
      ) : !hideBack ? (
        <Pressable onPress={onBack} style={styles.item} hitSlop={space.xs}>
          <Icon icon="arrow-left" size={iconSize.sm} color={foreground} />
        </Pressable>
      ) : (
        <View style={[styles.item, styles.rightItem]} />
      )}
      <View style={styles.content}>
        {title ? (
          <Text
            {...titleProps}
            adjustsFontSizeToFit
            numberOfLines={1}
            style={[styles.title, titleProps?.style]}
          >
            {title}
          </Text>
        ) : null}
        {subTitle ? (
          <Text style={styles.subTitle} adjustsFontSizeToFit numberOfLines={1}>
            {subTitle}
          </Text>
        ) : null}
      </View>
      <View style={[styles.item, styles.rightItem]}>
        {RightItem ? RightItem({color: foreground}) : null}
      </View>
    </Layout>
  )
}

const styles = StyleSheet.create({
  container: {
    paddingVertical: space.xs,
    paddingTop: insets.paddingTop,
    flexDirection: 'row',
    columnGap: space.md,
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: defaultColor[100],
  },
  divider: {
    borderBottomWidth: 1,
    borderColor: defaultColor[100],
  },
  content: {
    flex: 6,
    alignItems: 'center',
  },
  item: {
    flex: 2,
    paddingHorizontal: space.md,
  },
  rightItem: {
    alignItems: 'flex-end',
  },
  title: {
    fontWeight: 'bold',
    fontSize: fontSize.lg,
    textAlign: 'center',
    color: colors.primary,
    //
  },
  subTitle: {
    textAlign: 'center',
    fontSize: fontSize.sm,
    marginTop: space['4xs'],
  },
})
