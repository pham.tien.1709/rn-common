import React, {LegacyRef, forwardRef} from 'react'
import {FlashList, FlashListProps} from '@shopify/flash-list'

export interface ListProps extends FlashListProps<any> {
  itemKey?: string
}

type ListRef = LegacyRef<any> | undefined

export const List = forwardRef<ListRef, ListProps>((props, ref?: ListRef) => {
  const {itemKey} = props

  return (
    <FlashList
      ref={ref}
      keyExtractor={(item, index) =>
        `${itemKey ? item?.[itemKey]?.toString() : ''}_${index.toString()}`
      }
      showsHorizontalScrollIndicator={false}
      showsVerticalScrollIndicator={false}
      {...props}
    />
  )
})

List.displayName = 'List'
