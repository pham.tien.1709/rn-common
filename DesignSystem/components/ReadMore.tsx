import React, {LegacyRef, forwardRef, useState} from 'react'
import {StyleProp, TextLayoutLine, TextStyle, StyleSheet} from 'react-native'
import {Text, TextProps} from './Text'
import {IS_ANDROID, IS_IOS} from '@constant/constant/constants'
import {RNLayoutAnimation} from '@utils/animation'

export interface ReadMoreProps extends TextProps {
  style?: StyleProp<TextStyle>
  numberOfLines?: number
  children: string
  readMoreText?: string
  readLessText?: string
  readMoreStyle?: StyleProp<TextStyle>
  readLessStyle?: StyleProp<TextStyle>
  isViewAll?: boolean
}

interface TextProperties {
  length: number
  isTruncatedText: boolean
}

export const ReadMore = forwardRef<LegacyRef<any>, ReadMoreProps>(
  (props, ref) => {
    const {
      style,
      numberOfLines = 1,
      children,
      readMoreText = 'Read More',
      readLessText = 'Less',
      readMoreStyle = {color: 'black'},
      readLessStyle = {color: 'black'},
      isViewAll
    } = props

    const [readMore, setReadMore] = useState<boolean>(isViewAll??false)
    const [text, setText] = useState<TextProperties>({
      length: 0,
      isTruncatedText: false,
    })

    const onReadMore = () => {
      setReadMore(!readMore)
      RNLayoutAnimation()
    }

    const getReadMoreStyle = () => {
      if (readMore) {
        return readLessStyle
      }
      return readMoreStyle
    }

    function handleReadMoreText(textLayoutLines: TextLayoutLine[]) {
      let textLength = 0
      if (textLayoutLines.length > numberOfLines) {
        for (let line = 0; line < numberOfLines; line++) {
          textLength += textLayoutLines[line].text.length
        }
        setText({length: textLength, isTruncatedText: true})
        return
      }
      setText({length: children?.length, isTruncatedText: false})
    }
    return (
      <>
        {/**
        iOS always requires one element without a line number
        to determine the number of lines.
       */}
        {IS_IOS && (
          <Text
            style={styles.text}
            onTextLayout={({nativeEvent: {lines}}) => {
              if (text.length > 0) {
                return
              }
              if (IS_IOS) {
                handleReadMoreText(lines)
              }
            }}
          >
            {children}
          </Text>
        )}
        <Text
          {...props}
          style={style}
          numberOfLines={text.length === 0 ? numberOfLines : 0}
          onTextLayout={({nativeEvent: {lines}}) => {
            if (text.length > 0) {
              return
            }
            if (IS_ANDROID) {
              handleReadMoreText(lines)
            }
          }}
        >
          {text.isTruncatedText && !readMore && text.length !== 0
            ? `${children?.slice(0, text.length - 30).trim()}...`
            : children}
          {text.isTruncatedText && (
            <Text
              style={[styles.bold, getReadMoreStyle()]}
              onPress={onReadMore}
              suppressHighlighting={true}
            >
              {' '}
              {readMore ? readLessText : readMoreText}
            </Text>
          )}
        </Text>
      </>
    )
  }
)

const styles = StyleSheet.create({
  lessText: {
    //
  },
  moreText: {
    //
  },
  text: {
    height: 0,
  },
  bold: {
    fontWeight: 'bold',
  },
})
