import {View, ViewProps, StyleSheet} from 'react-native'
import React from 'react'
import {Level} from '@ui-kit/types'
import {useTheme} from '@ui-kit/hook'

export interface DividerProps extends ViewProps {
  level?: Level
  color?: string
  size?: number
}

export const Divider = (props: DividerProps) => {
  const theme = useTheme()

  return (
    <View
      {...props}
      style={[
        styles.container,
        {borderBottomColor: props?.color ?? theme.divider},
        props?.style,
        props?.size ? {borderBottomWidth: props?.size} : {},
      ]}
    />
  )
}

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: StyleSheet.hairlineWidth + 0.5,
  },
})
