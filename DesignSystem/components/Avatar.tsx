import {StyleSheet} from 'react-native'
import React from 'react'
import {avatarSize, screenSize} from '@ui-kit/theme'
import {type AvatarSize} from '@ui-kit/types'
import {Image, type ImageProps} from './Image/Image'

interface AvatarProps extends ImageProps {
  size?: AvatarSize | number
}

export const Avatar = (props: AvatarProps) => {
  const {size: sizeProp = 'md', source} = props

  const size =
    typeof sizeProp === 'string'
      ? avatarSize?.[sizeProp] ?? avatarSize.md
      : sizeProp

  const avatarViewStyle: any = {
    width: size,
    height: size,
  }

  return (
    <Image
      source={source}
      resizeMode="cover"
      style={[styles.container, avatarViewStyle, props?.style]}
    />
  )
}

const styles = StyleSheet.create({
  container: {
    borderRadius: screenSize.width,
  },
})

Avatar.displayName = 'Avatar'
