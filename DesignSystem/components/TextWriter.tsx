// SOURCE: https://github.com/7nohe/react-native-typewriter-effect/blob/main/src/index.tsx

import React, {
  useState,
  useEffect,
  useRef,
  useCallback,
  forwardRef,
  useImperativeHandle,
} from 'react'
import {type TextStyle, type StyleProp} from 'react-native'
import {Text, TextProps} from './Text'
import {useIsFocused} from '@react-navigation/native'

const DEFAULT_MAX_DELAY = 100
const DEFAULT_MIN_DELAY = 10

export interface TextWriterProps extends TextProps {
  // The text to be typed.
  content: string
  // The maximum delay between each character.
  maxDelay?: number
  // The minimum delay between each character.
  minDelay?: number
  // Callback function that is called when a character is typed.
  onTyped?: (char: string, currentCharIndex: number) => void
  // Callback function that is called when the typing ends.
  onTypingEnd?: () => void
  // The style of the text.
  style?: StyleProp<TextStyle>
  // Whether to vibrate when a character is typed.
  vibration?: boolean

  loop?: boolean
}

export const TextWriter = forwardRef((props: TextWriterProps, ref) => {
  const {
    content,
    onTyped,
    onTypingEnd,
    minDelay = DEFAULT_MIN_DELAY,
    maxDelay = DEFAULT_MAX_DELAY,
    vibration = true,
  } = props
  const [currentCharIndex, setCurrentCharIndex] = useState(0)
  const [backspaceEffect, setBackspaceEffect] = useState(false)
  const timeoutId = useRef<NodeJS.Timeout | null>(null)
  const delta = backspaceEffect ? -1 : 1

  const isFocused = useIsFocused()

  // handle ref
  useImperativeHandle(ref, () => ({
    restart,
  }))

  const restart = () => {
    clear()
    setCurrentCharIndex(0)
    startTypeWriter(
      Math.round(Math.random() * (maxDelay - minDelay) + minDelay)
    )
  }

  useEffect(() => {
    if (isFocused) restart()
    else clear()
  }, [isFocused])

  const startTypeWriter = useCallback(
    (ms: number) => {
      timeoutId.current = setTimeout(
        () => {
          setCurrentCharIndex(currentCharIndex + delta)
        },
        backspaceEffect ? 80 : ms
      )
    },
    [currentCharIndex, vibration, delta, backspaceEffect]
  )

  const clear = () => {
    if (timeoutId.current) {
      clearTimeout(timeoutId.current)
      timeoutId.current = null
    }
  }

  useEffect(() => {
    setBackspaceEffect(false)
    setCurrentCharIndex(0)
  }, [content])

  useEffect(() => {
    clear()

    const currentChar = content.charAt(currentCharIndex)
    const nextChar = content.charAt(currentCharIndex + delta)

    if (currentChar) {
      onTyped?.(currentChar, currentCharIndex)
    }

    if (!nextChar) {
      const delay = setTimeout(() => {
        setBackspaceEffect(true)
      }, 1000)
      return () => {
        clearTimeout(delay)
      }
    } else {
      startTypeWriter(
        Math.round(Math.random() * (maxDelay - minDelay) + minDelay)
      )
    }

    return () => {
      clear()
    }
  }, [startTypeWriter, currentCharIndex, delta])

  useEffect(() => {
    if (backspaceEffect && currentCharIndex === 0) {
      onTypingEnd?.()
    }
  }, [backspaceEffect, currentCharIndex])

  return (
    <Text {...props}>
      {content?.substring?.(
        0,
        backspaceEffect ? currentCharIndex : currentCharIndex + 1
      )}
    </Text>
  )
})
