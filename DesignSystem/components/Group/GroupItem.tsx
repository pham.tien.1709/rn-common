import React, {FC, forwardRef} from 'react'
import {
  Pressable,
  PressableProps,
  StyleSheet,
  View,
  ViewStyle,
} from 'react-native'
import {Text, TextProps} from '../Text'
import {useTheme} from '@ui-kit/hook'
import {fontSize, iconSize, space} from '@ui-kit/theme'
import {ItemType} from '@ui-kit/types'
import {Icon, IconProps} from '../Icon'

export interface GroupItemProps extends PressableProps {
  titleProps?: TextProps
  subTitle?: string
  subTitleProps?: TextProps
  RightItem?: IconProps | FC<ItemType>
  LeftItem?: IconProps | FC<ItemType>
  border?: boolean
}

export const GroupItem = forwardRef<any, GroupItemProps>((props, ref) => {
  const {foreground, divider} = useTheme()

  const {
    children,
    titleProps,
    subTitle,
    RightItem = {icon: 'right'},
    subTitleProps,
    LeftItem,
    border = true,
  } = props

  return (
    <Pressable
      ref={ref}
      {...props}
      style={[
        styles.container,
        props?.style as ViewStyle,
        {borderBottomColor: divider},
        !border ? styles.lastItem : null,
      ]}
    >
      {LeftItem ? (
        typeof LeftItem === 'object' ? (
          LeftItem?.icon ? (
            <Icon size={iconSize.xs} {...LeftItem} />
          ) : null
        ) : (
          LeftItem({color: foreground})
        )
      ) : null}
      <View style={styles.info}>
        {children ? (
          <Text {...titleProps} style={[styles.title, titleProps?.style]}>
            {children as string}
          </Text>
        ) : null}
        {subTitle ? (
          <Text
            {...subTitleProps}
            style={[styles.subTitle, subTitleProps?.style]}
          >
            {subTitle}
          </Text>
        ) : null}
      </View>
      {RightItem ? (
        typeof RightItem === 'object' ? (
          RightItem?.icon ? (
            <Icon size={iconSize.xs} {...RightItem} />
          ) : null
        ) : (
          RightItem({color: foreground})
        )
      ) : null}
    </Pressable>
  )
})

GroupItem.displayName = 'GroupItem'

const styles = StyleSheet.create({
  container: {
    paddingBottom: space.sm,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: StyleSheet.hairlineWidth,
    columnGap: space.sm,
  },
  title: {
    //
  },
  info: {
    flex: 1,
    rowGap: space['3xs'],
  },
  subTitle: {
    fontSize: fontSize.sm,
  },
  lastItem: {
    borderBottomWidth: 0,
    paddingBottom: 0,
  },
})
