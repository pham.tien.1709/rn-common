import {StyleSheet} from 'react-native'
import React, {ReactElement, createContext} from 'react'
import {Layout, LayoutProps} from '../Layout'
import {space} from '@ui-kit/theme'
import {GroupItem} from './GroupItem'

export const GroupContext = createContext<{lastChild: string}>({lastChild: ''})

export interface GroupProps extends Omit<LayoutProps, 'children'> {
  children?: ReactElement[]
}

const Group = (props: GroupProps) => {
  const {children} = props
  return (
    <Layout {...props} style={[style.container, props?.style]}>
      {children}
    </Layout>
  )
}

export default Object.assign(Group, {
  Item: GroupItem,
})

const style = StyleSheet.create({
  container: {
    padding: space.md,
    borderRadius: space['3xs'],
    rowGap: space.sm,
  },
})
