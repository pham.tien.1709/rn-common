import React from 'react'
import {
  ImageSourcePropType,
  StyleSheet,
  View,
  Image as RNImage
} from 'react-native'

import FastImage, {
  type FastImageProps,
  type Source
} from 'react-native-fast-image'
import type {ImageRequireSource} from 'react-native'
import {Images} from '@constant/themes/images'
import {colors} from '@ui-kit/theme'

export interface ImageProps extends Omit<FastImageProps, 'source'> {
  source?: ImageRequireSource | string | Source | ImageSourcePropType
  headers?: Source['headers']
  priority?: Source['priority']
  cache?: Source['cache']
}

export const Image = (props: ImageProps) => {
  const {
    style: containerStyle,
    source,
    defaultSource = Images.default_image
  } = props

  const defaultSourceProps = {
    headers: props?.headers,
    priority: props?.priority,
    cache: props?.cache
  }

  const imageSource =
    typeof source === 'string'
      ? {
          uri: source,
          ...defaultSourceProps
        }
      : typeof source === 'number'
        ? source
        : {
            ...(source as Source),
            ...defaultSourceProps
          }

  return !source || (typeof source === 'object' && !(source as any)?.uri) ? (
    <View style={[styles.defaultImageView, containerStyle]}>
      <RNImage
        source={defaultSource}
        style={styles.defaultImage}
        resizeMode="contain"
        key="image-default"
      />
    </View>
  ) : (
    <FastImage
      {...props}
      defaultSource={defaultSource}
      style={containerStyle}
      source={imageSource}
    />
  )
}

const styles = StyleSheet.create({
  placeholder: {},
  defaultImage: {
    width: '100%',
    height: '100%'
  },
  defaultImageView: {
    overflow: 'hidden',
    backgroundColor: colors.background3
  }
})
