import {ViewProps} from 'react-native'
import React, {forwardRef} from 'react'
import Animated, {
  AnimateProps,
  SlideInRight,
  SlideOutLeft,
} from 'react-native-reanimated'

interface ViewPropTypes extends ViewProps {
  layoutKey: any
  isShow?: boolean
}

const AnimatedSlideView = forwardRef(
  (props: AnimateProps<ViewPropTypes>, ref: any) => {
    const {layoutKey, isShow = true} = props

    return isShow ? (
      <Animated.View
        ref={ref}
        key={layoutKey}
        entering={SlideInRight}
        exiting={SlideOutLeft}
        {...props}
      />
    ) : null
  }
)

AnimatedSlideView.displayName = 'AnimatedSlideView'

export default AnimatedSlideView
