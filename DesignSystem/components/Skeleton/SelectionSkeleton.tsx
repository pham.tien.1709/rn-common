import {View, StyleSheet, ViewProps} from 'react-native'
import React from 'react'
import {Placeholder, PlaceholderLine, Fade} from 'rn-placeholder'
import {colors, space} from '@ui-kit/theme'

export interface SelectionSkeletonProps extends ViewProps {}

const SKELETON_COLOR = colors.background // FIXME: Change this color

export const SelectionSkeleton = (props: SelectionSkeletonProps) => {
  return (
    <View style={styles.container} {...props}>
      <Placeholder Animation={Fade}>
        <View style={styles.row}>
          <PlaceholderLine
            noMargin
            style={styles.image}
            color={SKELETON_COLOR}
          />
          <View style={styles.flex}>
            <View style={styles.statusRow}>
              <View style={styles.row}>
                <PlaceholderLine
                  noMargin
                  style={[styles.statusTag, styles.statusFirst]}
                  color={SKELETON_COLOR}
                />
                <PlaceholderLine
                  noMargin
                  style={[styles.statusTag, styles.statusSecond]}
                  color={SKELETON_COLOR}
                />
                <PlaceholderLine
                  noMargin
                  style={[styles.statusTag, styles.statusThird]}
                  color={SKELETON_COLOR}
                />
              </View>
              <PlaceholderLine
                noMargin
                style={styles.checkbox}
                color={SKELETON_COLOR}
              />
            </View>
            <PlaceholderLine
              noMargin
              style={styles.title}
              color={SKELETON_COLOR}
            />
            <PlaceholderLine
              noMargin
              style={styles.price}
              color={SKELETON_COLOR}
            />
            <View style={styles.likeViewWrap}>
              <PlaceholderLine
                noMargin
                style={styles.likeView}
                color={SKELETON_COLOR}
              />
              <PlaceholderLine
                noMargin
                style={styles.likeView}
                color={SKELETON_COLOR}
              />
            </View>
          </View>
        </View>
      </Placeholder>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {height: 110},
  row: {flex: 1, flexDirection: 'row'},
  image: {width: 140, height: 98, borderRadius: 4, marginRight: 16},
  flex: {flex: 1},
  statusRow: {flex: 1, flexDirection: 'row', justifyContent: 'space-between'},
  statusTag: {height: 20, marginRight: 4},
  statusFirst: {width: 41},
  statusSecond: {width: 50},
  statusThird: {width: 46},
  checkbox: {width: 20, height: 20},
  title: {height: 36, marginTop: 24},
  price: {height: 18, marginTop: space['4xs']},
  likeViewWrap: {flexDirection: 'row', marginTop: space['4xs']},
  likeView: {width: 42, height: 12, marginRight: 4},
})
