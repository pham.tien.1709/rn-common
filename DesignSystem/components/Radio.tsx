import {
  View,
  Text,
  PressableProps,
  StyleSheet,
  Pressable,
  ViewStyle,
} from 'react-native'
import React from 'react'
import {TextProps} from './Text'
import {
  colors,
  defaultColor,
  fontSize,
  screenSize,
  space,
} from '@ui-kit/theme'
import {Haptic} from '@ui-kit/common'

export interface RadioProps extends PressableProps {
  titleProps?: TextProps
  type?: ''
  size?: 'sm' | 'md' | 'lg'
  checked?: boolean
  onCheck?: (value: boolean) => void
}

export const Radio = (props: RadioProps) => {
  const {children, size = 'md', checked = false, titleProps} = props
  const disabled = props?.disabled

  const onPress = () => {
    Haptic()
    props?.onCheck?.(!checked)
  }

  return (
    <Pressable
      {...props}
      onPress={onPress}
      style={[
        styles.container,
        props?.style as ViewStyle,
        disabled && styles.disabled,
      ]}
    >
      <>
        <View
          style={[
            styles.radio,
            checked && [styles.checked, disabled && styles.checkedDisabled],
            {...(RadioSize?.[size] ?? RadioSize.md)},
          ]}
        >
          {checked ? (
            <View
              style={[
                styles.dot,
                {...RadioSize[size].dot},
                disabled && styles.dotDisabled,
              ]}
            />
          ) : null}
        </View>
        {children ? (
          typeof children === 'string' ? (
            <Text {...titleProps}>{children}</Text>
          ) : (
            children
          )
        ) : null}
      </>
    </Pressable>
  )
}

const RadioSize = {
  sm: {
    width: fontSize.xl,
    height: fontSize.xl,
    dot: {
      width: fontSize['2xs'],
      height: fontSize['2xs'],
    },
  },
  md: {
    width: fontSize['2xl'],
    height: fontSize['2xl'],
    dot: {
      width: fontSize['2xs'],
      height: fontSize['2xs'],
    },
  },
  lg: {
    width: fontSize['3xl'],
    height: fontSize['3xl'],
    dot: {
      width: fontSize.sm,
      height: fontSize.sm,
    },
  },
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    gap: space['2xs'],
  },
  radio: {
    borderRadius: screenSize.width,
    borderWidth: 2,
    borderColor: defaultColor[300],
    justifyContent: 'center',
    alignItems: 'center',
  },
  dot: {
    backgroundColor: colors.primary,
    borderRadius: screenSize.width,
  },
  checked: {
    borderColor: colors.primary,
  },
  checkedDisabled: {
    borderColor: defaultColor[500],
  },
  disabled: {
    opacity: 0.4,
  },
  dotDisabled: {
    backgroundColor: defaultColor[500],
  },
})
