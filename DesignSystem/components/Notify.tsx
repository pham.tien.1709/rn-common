import React, {
  forwardRef,
  useEffect,
  useImperativeHandle,
  useState,
} from 'react'
import Animated, {
  ZoomOutUp,
  useAnimatedStyle,
  useSharedValue,
  withSpring,
} from 'react-native-reanimated'
import {Notify, NotifyOptions} from '@ui-kit/common'
import {View, StyleSheet, Pressable} from 'react-native'
import {Text} from './Text'
import {
  colors,
  defaultColor,
  fontSize,
  iconSize,
  screenSize,
  space,
  statusColor,
} from '@ui-kit/theme'
import {useSafeAreaInsets} from 'react-native-safe-area-context'
import {HStack} from './HStack'
import {IS_ANDROID} from '@constant/constant/constants'
import {Icon} from './Icon'
import {Status} from '@ui-kit/types'

const defaultOption: NotifyOptions = {
  icon: 'info',
  status: 'info',
  type: 'solid',
  title: '',
  message: '',
  duration: 3000,
}

export const NotifyView = forwardRef((_, ref) => {
  const [stack, setStack] = useState<NotifyOptions[]>([])
  const insets = useSafeAreaInsets()
  const positionTop = insets?.top || space.sm
  const top = IS_ANDROID ? positionTop + space.sm : positionTop

  useImperativeHandle(ref, () => ({
    show,
    clearByIndex,
  }))

  const clearByIndex = (index: number) => {
    let arr = [...stack]
    const {length} = arr

    if (arr?.[index]) {
      arr[index] = undefined
    }

    // clear stack
    for (let i = 0; i < length; i++) {
      if (arr[i] !== undefined) {
        break
      }
      if (i === length - 1) {
        arr = []
      }
    }

    setStack(arr)
  }

  const show = (response: NotifyOptions) => {
    setStack(prev => {
      const object = {
        ...defaultOption,
        ...response,
      }

      return [...prev, object]
    })
  }

  const renderNotify = (item: NotifyOptions, index: number) =>
    item ? (
      <NotifyItem
        top={top}
        {...item}
        index={index}
        key={`notify-${index}`}
        stack={stack}
      />
    ) : null

  if (stack?.length === 0)return;
  return <View style={styles.wrapper}>{stack?.map?.(renderNotify)}</View>
})

interface NotifyItemProps extends Omit<NotifyOptions, 'index'> {
  index: number
  top: number
  stack: NotifyOptions[]
}

const INIT_OFFSET = -screenSize.half_height

const NotifyItem = ({
  icon,
  status,
  type,
  title,
  message,
  duration,
  index,
  onClick,
  top,
  stack,
  dismissOnClick,
}: NotifyItemProps) => {
  const length = stack?.length ?? 1
  const offset = useSharedValue(INIT_OFFSET)
  const scaleValue = 1 - (length - index - 1) / 10
  const scale = scaleValue <= 0 ? 0.1 : scaleValue

  const defaultStatusColor = statusColor[status as Status][600] as string

  const backgroundColor =
    type === 'subtle' ? defaultColor[50] : defaultStatusColor

  const color = type === 'solid' ? colors.white : colors.black

  const borderColor = type === 'solid' ? color : defaultStatusColor

  useEffect(() => {
    offset.value = 0 // mounting
    const timeout = setTimeout(() => unMount(), duration)
    return () => {
      clearTimeout(timeout)
    }
  }, [])

  const unMount = () => {
    Notify.clearByIndex(index)
  }

  const transitionStyle = useAnimatedStyle(() => {
    return {
      transform: [
        {
          translateY: withSpring(offset.value, {
            mass: 1,
            damping: 20,
            stiffness: 100,
            overshootClamping: false,
            restSpeedThreshold: 0.001,
            restDisplacementThreshold: 0.001,
          }),
        },
        {scale: length - 1 === index ? 1 : withSpring(scale)},
      ],
    }
  })

  const onPress = () => {
    if (dismissOnClick) unMount()
    onClick?.()
  }

  return (
    <Animated.View
      exiting={ZoomOutUp.duration(400)}
      key={`Notify-${index}`}
      style={[
        styles.container,
        {backgroundColor, top},
        type === 'outline' && [styles.outline, {borderColor}],
        transitionStyle,
      ]}
    >
      <Pressable onPress={onPress}>
        <HStack style={styles.row}>
          {icon ? (
            <Icon
              icon={icon}
              color={borderColor}
              size={iconSize.sm}
              style={styles.icon}
            />
          ) : null}
          <Text fontWeight="500" style={[styles.title, {color}]}>
            {title}
          </Text>
        </HStack>
        {message ? (
          <Text style={[styles.message, {color}]}>{message}</Text>
        ) : null}
      </Pressable>
    </Animated.View>
  )
}

const styles = StyleSheet.create({
  wrapper: {
    position: 'absolute',
    zIndex: 999,
    right: 0,
    left: 0,
  },
  container: {
    padding: space.md,
    borderRadius: space['xs'],
    width: screenSize.width - space.sm * 2,
    position: 'absolute',
    alignSelf: 'center',
    gap: space['3xs'],
  },
  row: {
    columnGap: space.sm,
  },
  icon: {},
  title: {
    flex: 1,
    fontSize: fontSize.lg,
  },
  message: {
    marginLeft: space.sm + iconSize.sm,
  },
  outline: {
    borderWidth: 1,
    backgroundColor: colors.white,
  },
})
