import React, {ReactNode, Ref, forwardRef, useCallback} from 'react'
import {
  BottomSheetBackdrop,
  BottomSheetBackdropProps,
  BottomSheetModal as RNBottomSheet,
  BottomSheetModalProps as RNBottomSheetProps,
  BottomSheetView,
} from '@gorhom/bottom-sheet'
import {Pressable, StyleSheet, View, ViewStyle} from 'react-native'
import {Text, TextProps} from './Text'

import {
  defaultColor,
  fontSize,
  iconSize,
  insets,
  primary,
  space,
} from '@ui-kit/theme'
import {HStack} from './HStack'
import {BottomSheetModalMethods} from '@gorhom/bottom-sheet/lib/typescript/types'
import {setStatusBar} from '@ui-kit/common'
import {Icon} from './Icon'

export interface BottomSheetProps extends RNBottomSheetProps {
  title?: string
  subTitle?: string
  titleProps?: TextProps
  subTitleProps?: TextProps
  backdropProps?: BottomSheetBackdropProps
  headerStyle?: ViewStyle
  backgroundStyle?: ViewStyle
  isScrollView?: boolean
}

export type BottomSheetRef = BottomSheetModalMethods

export {BottomSheetView}

export const BottomSheet = forwardRef(
  (props: BottomSheetProps, ref: Ref<BottomSheetRef>) => {
    const {
      title,
      titleProps,
      backdropProps,
      headerStyle,
      backgroundStyle,
      enableDynamicSizing,
      children,
      subTitle,
      subTitleProps,
      isScrollView = false,
    } = props

    const dynamicProps = enableDynamicSizing ? {} : {snapPoints: ['50%']}

    const backdropComponent = useCallback(
      (props: BottomSheetBackdropProps) => (
        <BottomSheetBackdrop
          {...props}
          {...backdropProps}
          appearsOnIndex={0}
          disappearsOnIndex={-1}
        />
      ),
      []
    )

    const onClose = () => {
      ;(ref as any)?.current?.close()
    }

    const onChange = (index: number) => {
      setStatusBar()
      props?.onChange?.(index)
    }

    // const footerComponent = () => {
    //   return (
    //     <View>
    //       <View style={{paddingBottom: bottom}} />
    //     </View>
    //   )
    // }

    return (
      <RNBottomSheet
        ref={ref}
        backgroundStyle={[styles.backgroundStyle, backgroundStyle]}
        handleComponent={() => (
          <HStack style={[styles.header, headerStyle]}>
            <View style={styles.content}>
              <Text
                numberOfLines={1}
                adjustsFontSizeToFit
                {...titleProps}
                style={[styles.title, titleProps?.style]}
              >
                {title || ''}
              </Text>
              {subTitle ? (
                <Text
                  style={[styles.subTitle, subTitleProps?.style]}
                  numberOfLines={1}
                  adjustsFontSizeToFit
                  {...subTitleProps}
                >
                  {subTitle}
                </Text>
              ) : null}
            </View>
            <Pressable onPress={onClose}>
              <Icon
                icon="cross"
                size={iconSize.sm}
                color={primary[800]}
              />
            </Pressable>
          </HStack>
        )}
        backdropComponent={backdropComponent}
        enablePanDownToClose
        enableHandlePanningGesture
        {...dynamicProps}
        {...props}
        onChange={onChange}
      >
        {enableDynamicSizing && !isScrollView ? (
          <BottomSheetView style={styles.bottomSheetView}>
            {children as ReactNode[] | ReactNode}
          </BottomSheetView>
        ) : (
          children
        )}
      </RNBottomSheet>
    )
  }
)

BottomSheet.displayName = 'BottomSheet'

const styles = StyleSheet.create({
  header: {
    padding: space.md,
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderBottomColor: defaultColor[100],
  },
  title: {
    fontWeight: '700',
    fontSize: fontSize.lg,
  },
  subTitle: {
    fontSize: fontSize.sm,
    color: defaultColor[700],
  },
  backgroundStyle: {
    borderRadius: space.sm,
    paddingBottom: insets.bottom,
  },
  content: {
    flex: 1,
  },
  bottomSheetView: {
    paddingBottom: insets.bottom,
  },
})
