import React from 'react'
import {Keyboard, TouchableWithoutFeedback} from 'react-native'
import {Container, ContainerProps} from './Container'

export type KeyboardDismissContainerProps = ContainerProps

export function KeyboardDismissContainer(props: KeyboardDismissContainerProps) {
  const onPress = () => {
    Keyboard.dismiss()
  }

  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <Container {...props} />
    </TouchableWithoutFeedback>
  )
}
