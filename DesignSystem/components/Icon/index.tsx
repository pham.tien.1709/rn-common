import React from 'react'
import IcoMoon, {type IconProps as IconMoonProps} from 'react-icomoon'
import {Svg, Path} from 'react-native-svg'
import iconSet from './icomoon.json'
import {colors, iconSize} from '@ui-kit/theme'
import {IconName} from './IconNameType'

export type {IconName}

export const Icon: React.FC<IconProps & {color?: string}> = ({
  color = colors.primary,
  ...props
}: IconProps) => (
  <IcoMoon
    native
    SvgComponent={Svg}
    PathComponent={Path}
    iconSet={iconSet}
    color={color}
    size={iconSize.md}
    {...props}
  />
)

export interface IconProps extends IconMoonProps {
  icon: IconName
}
