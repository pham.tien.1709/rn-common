import React, {ReactNode, useRef, useState} from 'react'
import {
  Slider as RangeSlider,
  SliderProps as RNSliderProps,
} from '@miblanchard/react-native-slider'
import {StyleSheet, View, ViewStyle} from 'react-native'
import {colors, defaultColor, iconSize, space} from '@ui-kit/theme'
import {Text} from './Text'
import {Haptic} from '@ui-kit/common'

export interface SliderProps extends Omit<RNSliderProps, 'animationType'> {
  style?: ViewStyle
  renderBubble?: ({index, value}: {index: number; value: number}) => ReactNode
  animationType?: 'spring' | 'timing'
}

export const Slider = (props: SliderProps) => {
  const {step = 1} = props
  const [value, setValue] = useState<SliderProps['value']>(
    props.value ?? [0, 100]
  )
  const ref = useRef(null)
  const onValueChange = (value: Array<number>) => {
    setValue(value)
    props?.onValueChange?.(value)
  }

  const renderBubble = (index: number) => {
    const rangeValue = (
      Array.isArray(value) ? value?.[index] ?? value : value
    ) as number
    return (
      <View style={styles.bubble}>
        <View style={styles.triangle} />
        {props?.renderBubble?.({index, value: rangeValue}) ?? (
          <Text style={styles.value}>{rangeValue}</Text>
        )}
      </View>
    )
  }

  const onSlidingComplete = (value: Array<number>) => {
    Haptic()
    props?.onSlidingComplete?.(value)
  }

  return (
    <RangeSlider
      {...props}
      ref={ref}
      value={value}
      step={step}
      onSlidingComplete={onSlidingComplete}
      onValueChange={onValueChange}
      containerStyle={[styles.container, props?.style] as ViewStyle}
      trackStyle={[styles.track, props?.trackStyle] as ViewStyle}
      thumbStyle={[styles.thumb, props?.thumbStyle] as ViewStyle}
      thumbTintColor={colors.primary}
      renderBelowThumbComponent={renderBubble}
      thumbTouchSize={{
        width: space.xl,
        height: space.lg,
      }}
      maximumTrackStyle={
        [styles.maximumTrackStyle, props?.maximumTrackStyle] as ViewStyle
      }
    />
  )
}

const styles = StyleSheet.create({
  container: {
    //
  },
  track: {},
  thumb: {},
  bubble: {
    right: '65%',
    width: '130%',
    alignItems: 'center',
    backgroundColor: colors.primary,
    minWidth: iconSize.sm,
    borderRadius: space['3xs'],
    top: -space.xs,
  },
  triangle: {
    width: 0,
    height: 0,
    top: -space['3xs'],
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderLeftWidth: space['3xs'],
    borderRightWidth: space['3xs'],
    borderBottomWidth: space['2xs'],
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: colors.primary,
    position: 'absolute',
  },
  value: {
    color: colors.white,
    fontWeight: '500',
  },
  maximumTrackStyle: {
    backgroundColor: defaultColor[200],
  },
})
