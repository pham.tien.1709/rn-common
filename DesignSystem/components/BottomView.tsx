import {colors, defaultColor, insets, space} from '@ui-kit/theme'
import React from 'react'
import {View, ViewProps, StyleSheet} from 'react-native'
import {useReanimatedKeyboardAnimation} from 'react-native-keyboard-controller'
import Animated, {
  Extrapolation,
  interpolate,
  useAnimatedStyle,
} from 'react-native-reanimated'

export interface BottomViewProps extends ViewProps {
  avoidKeyboard?: boolean
}

export const BottomView = (props: BottomViewProps) => {
  const {avoidKeyboard} = props
  return (
    <View style={[styles.container, props?.style]}>
      {avoidKeyboard ? (
        <AvoidKeyboardView>
          <Bottom {...props} />
        </AvoidKeyboardView>
      ) : (
        <Bottom {...props} />
      )}
      <View style={{height: insets.bottom}} />
    </View>
  )
}

interface AvoidKeyboardView extends ViewProps {}

const AvoidKeyboardView = ({children}: AvoidKeyboardView) => {
  const {height, progress} = useReanimatedKeyboardAnimation()

  const animatedStyle = useAnimatedStyle(() => {
    const translateY = interpolate(
      progress.value,
      [0, 1],
      [0, height.value + insets.bottom],
      {
        extrapolateRight: Extrapolation.CLAMP,
      }
    )

    const paddingBottom = interpolate(progress.value, [0, 1], [0, space.md], {
      extrapolateRight: Extrapolation.CLAMP,
    })

    return {
      transform: [{translateY}],
      paddingBottom,
    }
  })

  // const spacingAnimated = useAnimatedStyle(() => {
  //   const height = interpolate(progress.value, [0, 1], [0, space.md], {
  //     extrapolateRight: Extrapolation.CLAMP,
  //   })
  //   return {
  //     height,
  //   }
  // })

  return (
    <Animated.View style={[styles.container, animatedStyle]}>
      {children}
    </Animated.View>
  )
}

const Bottom = (props: BottomViewProps) => {
  const {children} = props
  return (
    <View {...props} style={[styles.bottomView, props.style]}>
      {children}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    shadowOpacity: 0.04,
    backgroundColor: colors.background,
    borderTopLeftRadius: space['3xs'],
    borderTopRightRadius: space['3xs'],
    borderTopWidth: 1,
    borderTopColor: defaultColor[100]
  },
  bottomView: {
    paddingHorizontal: space.md,
    paddingTop: space.sm,
  },
})
