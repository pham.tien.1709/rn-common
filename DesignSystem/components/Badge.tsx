/* eslint-disable react-native/no-unused-styles */
import {View, ViewProps, StyleSheet} from 'react-native'
import React from 'react'
import {colors, fontSize, space, statusColor} from '@ui-kit/theme'
import {Text} from './Text'
import {Status} from '@ui-kit/types'

export type BadgeType = 'solid' | 'outline' | 'subtle'

export type BadgeSize = 'sm' | 'lg'

export interface BadgeProps extends ViewProps {
  type?: BadgeType
  status?: Status
  size?: BadgeSize
}

export const Badge = (props: BadgeProps) => {
  const {children, type = 'solid', status = 'default', size = 'lg'} = props

  const color = statusColor?.[status]?.[
    status === 'primary' ? 800 : 500
  ] as string

  return (
    <View
      {...props}
      style={[
        styles.container,
        {
          backgroundColor: type === 'subtle' ? statusColor[status][200] : color,
        },
        type === 'outline' && [styles.outline, {borderColor: color}],
        props?.style,
      ]}
    >
      <Text
        style={[
          styles.text,
          size === 'sm' && styles.smallText,
          type !== 'solid' && {color},
        ]}
      >
        {children}
      </Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: space.xs,
    paddingVertical: space['4xs'],
    borderRadius: space['4xs'],
  },
  text: {
    textTransform: 'capitalize',
    color: colors.white,
    fontWeight: '500',
    fontSize: fontSize.sm,
  },
  outline: {
    borderWidth: 1,
    backgroundColor: colors.transparent,
  },

  smallText: {
    fontSize: fontSize['2xs'],
  },
})
