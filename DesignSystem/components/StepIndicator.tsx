import {View, ViewProps, StyleSheet} from 'react-native'
import React from 'react'
import {HStack} from './HStack'
import {Text} from './Text'
import {
  defaultColor,
  fontSize,
  iconSize,
  screenSize,
  space,
} from '@ui-kit/theme'
import {Divider} from './Divider'
import {useTheme} from '@ui-kit/hook'
import {Icon} from './Icon'

export interface StepIndicatorProps extends ViewProps {
  gap?: number
  data: string[]
  onPressItem?: (index: number) => void
  currentIndex?: number
}

export const StepIndicator = (props: StepIndicatorProps) => {
  const {
    // children,
    currentIndex = 0,
    data,
    //  onPressItem
  } = props

  const renderItem = (item: string, index: number) => {
    return (
      <StepItem
        currentIndex={currentIndex}
        data={item}
        index={index}
        isLast={index === data.length - 1}
        key={`step-item-${index}`}
      />
    )
  }

  return (
    <HStack
      {...props}
      gap={space['2xs']}
      style={[styles.container, props?.style]}
    >
      {data.map(renderItem)}
    </HStack>
  )
}

const StepItem = ({
  data,
  currentIndex,
  index, // isLast,
  // onPressItem,
}: {
  data: string
  currentIndex: number
  index: number
  isLast: boolean
  onPressItem?: void
}) => {
  const {primary, divider, white} = useTheme()
  const checked = currentIndex > index
  const active = currentIndex === index
  const isFirst = index === 0

  return (
    <>
      {!isFirst ? (
        <Divider
          style={[
            styles.divider,
            {borderBottomColor: active || checked ? primary : divider},
          ]}
        />
      ) : null}
      <HStack style={styles.infoView} gap={space['2xs']}>
        <View
          style={[
            styles.iconView,
            checked ? {borderColor: primary, borderWidth: 1.35} : null,
            active ? {backgroundColor: primary} : null,
          ]}
        >
          {checked ? (
            <Icon icon="check" size={14} color={primary} />
          ) : (
            <Text
              style={[styles.index, {color: active ? white : defaultColor[400]}]}
            >
              {index + 1}
            </Text>
          )}
        </View>
        <Text
          style={[
            styles.text,
            {color: active || checked ? primary[800] : defaultColor[400]},
          ]}
        >
          {data}
        </Text>
      </HStack>
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    paddingVertical: space.sm,
    justifyContent: 'space-between',
  },
  divider: {
    borderBottomWidth: 1,
    flex: 1,
    maxWidth: space['4xl'],
    // marginHorizontal: 14,
  },
  iconView: {
    borderRadius: screenSize.width,
    borderWidth: StyleSheet.hairlineWidth,
    width: iconSize.md,
    height: iconSize.md,
    justifyContent: 'center',
    alignItems: 'center',
  },
  infoView: {
    maxWidth: screenSize.width / 3,
  },
  text: {
    fontWeight: '500',
  },
  index: {
    position: 'absolute',
    fontSize: fontSize.sm,
    fontWeight: '500',
  },
})
