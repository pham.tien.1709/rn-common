import React from 'react'
import {Linking, Text as RNText, StyleSheet, TextStyle} from 'react-native'
import RNMarkdown, {
  type MarkdownProps as RNMarkdownProps,
} from 'react-native-markdown-display'
import {Text, TextProps} from './Text'
import {info} from '@ui-kit/theme'

export interface Markdown extends Omit<RNMarkdownProps, 'style'>, TextProps {}

export const Markdown = (props: Markdown) => {
  const {children} = props

  return (
    <RNMarkdown
      renderer={{
        render(nodes) {
          return nodes?.map?.((node, index): Element | Element[] => {
            const groups = node?.children?.[0]?.children ?? []

            return (
              <Text key={`markdown-${index}`} style={props.style}>
                {groups?.length
                  ? groups?.map?.((text: any, indexItem: number) => {
                      const textStyle: TextStyle[] = []
                      const link = text?.attributes?.href

                      switch (text?.sourceType) {
                        case 'strong':
                          textStyle.push(styles.bold)
                          break
                        case 'link':
                          textStyle.push(styles.link)
                          break
                        default:
                      }

                      const breakText = text?.sourceType === 'softbreak'

                      const markdownContent = text?.children?.[0]?.content
                      return (
                        <RNText
                          onPress={
                            link ? () => Linking.openURL(link) : undefined
                          }
                          style={textStyle}
                          key={`markdown-item-${indexItem}`}
                        >
                          {breakText ? '\n' : markdownContent || text.content}
                        </RNText>
                      )
                    })
                  : children}
              </Text>
            )
          })
        },
      }}
    >
      {children}
    </RNMarkdown>
  )
}

const styles = StyleSheet.create({
  bold: {
    fontWeight: 'bold',
  },
  link: {
    color: info[500],
  },
})
