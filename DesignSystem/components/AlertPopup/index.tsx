import React, {
  LegacyRef,
  Ref,
  forwardRef,
  useImperativeHandle,
  useState,
  useEffect,
  useRef,
} from 'react'

import {AlertView, AlertViewProps} from './AlertView'
import {ModalEventListener, ModalProps} from 'react-native-modalfy'
import {
  Modal,
  StyleSheet,
  TouchableWithoutFeedback,
  View,
  ViewStyle,
} from 'react-native'
import {colors} from '@ui-kit/theme'

export * from './AlertView'

export const AlertModal = ({
  modal: {getParam, closeModal, addListener},
}: ModalProps<'AlertPopup'>) => {
  const props: AlertViewProps = getParam('props')
  const onCloseListener = useRef<ModalEventListener>()

  useEffect(() => {
    onCloseListener.current = addListener('onClose', () => props?.onAlertHide?.())
    return () => {
      onCloseListener.current?.remove()
    }
  }, [])

  const onClose = () => {
    closeModal('AlertPopup')
  }

  return <AlertView onPressClose={onClose} {...props} />
}

type AlertViewPopUpRef = {
  open: () => void
  close: () => void
}

export const AlertPopup = forwardRef<
  LegacyRef<AlertViewPopUpRef>,
  AlertViewProps
>((props, ref: Ref<any>) => {
  const [visible, setVisible] = useState(false)

  useImperativeHandle(ref, () => ({
    open,
    close,
  }))

  const open = () => {
    setVisible(true)
  }

  const close = () => {
    setVisible(false)
  }

  return (
    <Modal animationType="fade" visible={visible} transparent>
      <TouchableWithoutFeedback onPress={close}>
        <View style={styles.overlay} />
      </TouchableWithoutFeedback>
      <View style={styles.container}>
        {visible ? (
          <AlertView {...props} onPressClose={close}>
            {props?.children ?? null}
          </AlertView>
        ) : null}
      </View>
    </Modal>
  )
})

AlertPopup.displayName = 'AlertPopup'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  overlay: {
    ...(StyleSheet.absoluteFill as ViewStyle),
    backgroundColor: colors.black + '92',
  },
})
