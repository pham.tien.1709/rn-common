import React from 'react'
import {Pressable, StyleSheet, View} from 'react-native'
import {Layout, LayoutProps} from '../Layout'
import {HStack} from '../HStack'
import {Button, ButtonProps} from '../Button'
import {Text} from '../Text'
import {
  colors,
  defaultColor,
  fontSize,
  success,
  iconSize,
  warning,
  screenSize,
  space,
} from '@ui-kit/theme'
import {Icon} from '../Icon'
import {closeAlertPopup} from '@ui-kit/common'

export interface AlertViewProps extends LayoutProps {
  title?: string
  message?: string
  button?: string
  cancel?: string
  cancelProps?: ButtonProps
  confirmProps?: ButtonProps
  confirm?: string
  status?: 'success' | 'warning'
  onConfirm?: () => void
  onPressClose?: () => void
  onPressCloseIcon?: () => void
  warningMessage?: string
  onPressWarning?: () => void
  disableCloseButton?: boolean
  onAlertHide?: () => void
}

export const AlertView = (props: AlertViewProps) => {
  const {
    title,
    message,
    cancel,
    confirm,
    status,
    children,
    confirmProps,
    cancelProps,
    warningMessage,
    onPressWarning,
    disableCloseButton = false,
  } = props
  const successful = status === 'success'

  const onCloseButtonPress = () => {
    if (typeof props?.onPressCloseIcon === 'function') {
      props.onPressCloseIcon?.()
    } else {
      closeAlertPopup()
    }
  }

  const onClose = () => {
    if (typeof props?.onPressClose === 'function') {
      props.onPressClose?.()
    } else {
      closeAlertPopup()
    }
  }

  const onConfirm = () => {
    if (typeof props?.onConfirm === 'function') {
      props?.onConfirm?.()
    } else {
      closeAlertPopup()
    }
  }

  return (
    <Layout {...props} style={[styles.container, props?.style]}>
      <View style={styles.content}>
        {status ? (
          <View
            style={[
              styles.icon,
              {backgroundColor: successful ? success[500] : warning[400]},
            ]}
          >
            <Icon
              size={iconSize['3xl']}
              color={colors.white}
              icon={successful ? 'checkmark' : 'radio-unchecked'}
            />
          </View>
        ) : null}
        {title ? (
          <Text style={[styles.title, !status ? styles.onlyTitle : {}]}>
            {title}
          </Text>
        ) : null}
        {message || warningMessage ? (
          <HStack>
            {message ? <Text style={styles.message}>{message}</Text> : null}
            {warningMessage ? (
              <Pressable onPress={onPressWarning}>
                <Text style={styles.warningMessage}> {warningMessage}</Text>
              </Pressable>
            ) : null}
          </HStack>
        ) : null}
        {children ? <View style={styles.children}>{children}</View> : null}
        <HStack style={styles.bottom}>
          {cancel ? (
            <Button
              {...cancelProps}
              onPress={onClose}
              type="outline"
              style={[styles.cancel, styles.button]}
            >
              {cancel}
            </Button>
          ) : null}
          {confirm ? (
            <Button
              {...confirmProps}
              onPress={onConfirm}
              style={[styles.confirm, styles.button]}
            >
              {confirm}
            </Button>
          ) : null}
        </HStack>
      </View>
      {!disableCloseButton ? (
        <Pressable onPress={onCloseButtonPress} style={styles.close}>
          <Icon
            icon="close-outline"
            size={iconSize.xs}
            color={colors.foreground}
          />
        </Pressable>
      ) : null}
    </Layout>
  )
}

const styles = StyleSheet.create({
  container: {
    padding: space.md,
    borderRadius: space.xs,
    width: screenSize.width - space.md * 2,
    rowGap: space.md,
  },
  title: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: fontSize.lg,
    color: colors.primary,
  },
  onlyTitle: {
    margin: space.md,
    paddingHorizontal: space.md,
  },
  content: {
    alignItems: 'center',
    rowGap: space.xl,
  },
  message: {
    textAlign: 'center',
    color: defaultColor[600],
  },
  warningMessage: {
    textAlign: 'center',
    color: warning[400],
  },
  cancel: {},
  confirm: {
    //
  },
  icon: {
    padding: space.xs,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: screenSize.width,
    marginTop: space.md,
  },
  close: {
    padding: space['4xs'],
    position: 'absolute',
    right: space.md,
    top: space.md,
  },
  button: {
    minWidth: screenSize.half_width / 2,
  },
  bottom: {
    alignItems: 'center',
    justifyContent: 'center',
    columnGap: space.sm,
  },
  children: {
    width: '100%',
  },
})
