import React from 'react'
import {StyleSheet, View, ViewProps} from 'react-native'
import {colors, space} from '@ui-kit/theme'

export interface CircleProps extends ViewProps {
  size: number
  backgroundColor?: string
}

export function Circle(props: CircleProps) {
  const {size = space.sm, backgroundColor = colors.primary} = props
  return (
    <View
      {...props}
      style={[
        styles.container,
        props?.style,
        {
          height: size,
          width: size,
          backgroundColor,
        },
      ]}
    />
  )
}

const styles = StyleSheet.create({
  container: {},
})
