import {
  View,
  Text,
  PressableProps,
  StyleSheet,
  Pressable,
  ViewStyle,
} from 'react-native'
import React from 'react'
import {TextProps} from './Text'
import {colors, defaultColor, fontSize, space} from '@ui-kit/theme'
import {Haptic} from '@ui-kit/common'
import {Icon} from './Icon'

interface CheckBoxProps extends PressableProps {
  titleProps?: TextProps
  type?: ''
  size?: 'sm' | 'md' | 'lg'
  checked?: boolean
  onCheck?: (value: boolean) => void
}

export const CheckBox = (props: CheckBoxProps) => {
  const {children, size = 'md', checked = false} = props
  const disabled = props?.disabled

  const onPress = () => {
    Haptic()
    props?.onCheck?.(!checked)
  }

  return (
    <Pressable
      {...props}
      onPress={onPress}
      style={[
        styles.container,
        props?.style as ViewStyle,
        disabled && styles.disabled,
      ]}
    >
      <>
        <View
          style={[
            styles.checkBox,
            checked && [styles.checked, disabled && styles.checkedDisabled],
            {...(CheckBoxSize?.[size] ?? CheckBoxSize.md)},
          ]}
        >
          {checked ? (
            <Icon size={20} icon="checkmark" color={colors.white} />
          ) : null}
        </View>
        {children ? (
          typeof children === 'string' ? (
            <Text>{children}</Text>
          ) : (
            children
          )
        ) : null}
      </>
    </Pressable>
  )
}

const CheckBoxSize = {
  sm: {
    width: fontSize.xl,
    height: fontSize.xl,
  },
  md: {
    width: fontSize['2xl'],
    height: fontSize['2xl'],
  },
  lg: {
    width: fontSize['3xl'],
    height: fontSize['3xl'],
  },
}

// const colorType = {
//   //
// }

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    columnGap: space.xs,
  },
  checkBox: {
    borderRadius: space['3xs'],
    borderWidth: 2,
    borderColor: defaultColor[300],
    justifyContent: 'center',
    alignItems: 'center',
  },
  checked: {
    backgroundColor: colors.primary,
    borderWidth: 0,
  },
  checkedDisabled: {
    backgroundColor: defaultColor[500],
  },
  disabled: {
    opacity: 0.4,
  },
})
