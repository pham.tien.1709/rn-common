import {View, ViewProps, StyleSheet, ViewStyle} from 'react-native'
import React, {LegacyRef, forwardRef} from 'react'

export interface HStackProps extends ViewProps {
  gap?: number
  alignItems?: ViewStyle['alignItems']
  justifyContent?: ViewStyle['justifyContent']
}

export const HStack = forwardRef<LegacyRef<any>, HStackProps>((props, ref) => {
  const {children, gap, justifyContent, alignItems} = props
  return (
    <View
      {...props}
      style={[
        styles.container,
        alignItems ? {alignItems} : {},
        justifyContent ? {justifyContent} : {},
        {columnGap: gap, justifyContent},
        props?.style,
      ]}
    >
      {children}
    </View>
  )
})

HStack.displayName = 'HStack'

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
})
