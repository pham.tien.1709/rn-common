import {View, StyleSheet} from 'react-native'
import React, {useEffect, useRef, useState} from 'react'
import {colors, fontSize, space} from '@ui-kit/theme'
import {
  Calendar as RNCalendar,
  CalendarProps as RNCalendarProps,
  LocaleConfig,
  DateData,
} from 'react-native-calendars'
import {BottomSheet, BottomSheetRef} from './BottomSheet'
import {HStack} from './HStack'
import {Button} from './Button'
import {ModalProps} from 'react-native-modalfy'
import {Haptic} from '@ui-kit/common'
import dayjs from 'dayjs'

export interface CalendarProps extends RNCalendarProps {
  onConfirm?: (day: DateData) => void
  title?: string
  cancelTitle?: string
  doneTitle?: string
  props?: RNCalendarProps
  dayIsSelected?: DateData
}

export interface CalendarRef {
  open: () => void
  close: () => void
}

export const Calendar = ({
  modal: {closeModal, getParam},
}: ModalProps<'Calendar'>) => {
  const title = getParam('title', 'Calendar')
  const doneTitle = getParam('doneTitle', 'Done')
  const cancelTitle = getParam('cancelTitle', 'Cancel')
  const confirm = getParam('onConfirm')
  const props = getParam('props')
  const dayIsSelected: DateData = getParam('dayIsSelected')

  const [day, setDay] = useState<DateData>()
  const sheetRef = useRef<BottomSheetRef>(null)

  const currentDate = dayjs(new Date().getTime()).format('YYYY-MM-DD')

  const checkEndOfMonth = () => {
    const now = new Date().getTime()
    const day = dayjs(now).get('date')
    const month = dayjs(now).get('months')
    if (
      ([1, 3, 5, 7, 8, 10, 12].includes(month + 1) && day === 31) ||
      ([4, 6, 9, 11].includes(month + 1) && day === 30) ||
      (month + 1 === 2 && [28, 29].includes(day))
    ) {
      return true
    }
    return false
  }

  useEffect(() => {
    sheetRef.current?.present()
    if (dayIsSelected?.dateString) {
      setDay(dayIsSelected)
    }
  }, [])

  const onConfirm = () => {
    if (day) confirm?.(day)
    onClose()
  }

  const onClose = () => {
    sheetRef.current?.close()
  }

  const onSheetChange = (index: number) => {
    if (index < 0) closeModal('Calendar')
  }

  const onDayPress = (date: DateData) => {
    Haptic()
    setDay(date)
  }

  return (
    <View style={styles.container}>
      <BottomSheet
        title={title}
        ref={sheetRef}
        animateOnMount
        onChange={onSheetChange}
        enableDynamicSizing
      >
        <View style={styles.content}>
          <RNCalendar
            current={
              checkEndOfMonth()
                ? dayjs(new Date().getTime()).format('YYYY-MM-DD')
                : currentDate
            }
            minDate={dayjs().format('YYYY-MM-DD')}
            monthFormat={'MMMM'}
            enableSwipeMonths
            {...props}
            onDayPress={onDayPress}
            theme={theme}
            markedDates={{
              [day?.dateString ?? '']: {
                selected: true,
                disableTouchEvent: true,
                selectedColor: colors.primary,
              },
              // [currentDate]: {disabled: true},
            }}
            hideExtraDays
          />
          <HStack style={styles.row}>
            <Button style={styles.button} onPress={onClose} type="outline">
              {cancelTitle}
            </Button>
            <Button style={styles.button} onPress={onConfirm}>
              {doneTitle}
            </Button>
          </HStack>
        </View>
      </BottomSheet>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  row: {
    justifyContent: 'space-between',
    paddingHorizontal: space.md,
    paddingBottom: space.md,
    paddingTop: space['4xl'],
  },
  button: {
    width: '48%',
  },
  content: {
    paddingHorizontal: space.md,
  },
})

const theme: RNCalendarProps['theme'] = {
  arrowColor: colors.primary,
  textSectionTitleColor: colors.primary,
  textDayStyle: {
    fontWeight: '500',
  },
  todayTextColor: colors.primary,
  textMonthFontSize: fontSize.xl,
  textMonthFontWeight: 'bold',
  'stylesheet.calendar.header': {
    week: {
      marginTop: space['4xl'],
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingHorizontal: space.xs,
    },
  },
}

LocaleConfig.locales['fr'] = {
  monthNames: [
    'Janvier',
    'Février',
    'Mars',
    'Avril',
    'Mai',
    'Juin',
    'Juillet',
    'Août',
    'Septembre',
    'Octobre',
    'Novembre',
    'Décembre',
  ],
  monthNamesShort: [
    'Janv.',
    'Févr.',
    'Mars',
    'Avril',
    'Mai',
    'Juin',
    'Juil.',
    'Août',
    'Sept.',
    'Oct.',
    'Nov.',
    'Déc.',
  ],
  dayNames: [
    'Dimanche',
    'Lundi',
    'Mardi',
    'Mercredi',
    'Jeudi',
    'Vendredi',
    'Samedi',
  ],
  dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
  today: "Aujourd'hui",
}
