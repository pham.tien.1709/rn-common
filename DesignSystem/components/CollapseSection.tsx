import React, {useState} from 'react'
import {StyleSheet, TouchableOpacity, View, ViewProps} from 'react-native'
import {HStack, Icon, Text} from '@ui-kit/components'
import {defaultColor, fontSize, space} from '@ui-kit/theme'
import Animated, {SlideInUp, SlideOutUp} from 'react-native-reanimated'
import {RNLayoutAnimation} from '@utils/animation'
import {IS_IOS} from '@ui-kit/constant'

export interface CollapseSectionProps extends ViewProps {
  title: string
  expanded?: boolean
}

export function CollapseSection({
  children,
  title,
  ...props
}: CollapseSectionProps) {
  const [expanded, setExpanded] = useState(props?.expanded ?? true)

  const toggle = () => {
    setExpanded(!expanded)
    RNLayoutAnimation()
  }

  return (
    <View {...props} style={[styles.container, props.style]}>
      <TouchableOpacity onPress={toggle} activeOpacity={0.9}>
        <HStack style={styles.titleView}>
          <Text style={styles.title}>{title}</Text>
          <Icon icon={!expanded ? 'down' : 'up'} />
        </HStack>
      </TouchableOpacity>
      {expanded ? <View style={styles.collapseView}>{children}</View> : null}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    overflow: 'hidden',
  },
  titleView: {
    paddingHorizontal: space.md,
    paddingVertical: space.sm,
    backgroundColor: defaultColor[50],
    gap: space.md,
    zIndex: 10,
  },
  title: {
    fontWeight: 'bold',
    fontSize: fontSize.lg,
    flex: 1,
  },
  collapseView: {
    overflow: 'hidden',
    zIndex: 0,
  },
})
