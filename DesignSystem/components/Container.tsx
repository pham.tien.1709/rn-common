/* eslint-disable @typescript-eslint/no-unused-vars */
import {View, ViewProps, StyleSheet} from 'react-native'
import React from 'react'
import {ColorType, Level} from '@ui-kit/types'
import {useTheme} from '@ui-kit/hook'
import ErrorBoundary from 'react-native-error-boundary'
import ErrorView from '@components/ErrorView'

export interface ContainerProps extends ViewProps {
  level?: Level | `${Level}`
  isHidedHeader?: boolean
  title?: string
}

export const Container = (props: ContainerProps) => {
  const {children, level = 1, isHidedHeader, title} = props
  const theme = useTheme()

  const backgroundColor =
    Number(level) > 1
      ? theme[`background${level}` as keyof ColorType]
      : theme.background
  // eslint-disable-next-line @typescript-eslint/ban-types
  const CustomFallback = (props: {error: Error; resetError: Function}) => (
    <View style={styles.emptyContainer}>
      {/* {!isHidedHeader ? (
        <NavigationBar title={title ?? translate('maintainer.title')} />
      ) : null} */}
      <ErrorView />
    </View>
  )

  return (
    <ErrorBoundary FallbackComponent={CustomFallback}>
      <View
        {...props}
        style={[
          styles.container,
          Number(level) >= 1 ? {backgroundColor} : null,
          props?.style,
        ]}
      >
        {children}
      </View>
    </ErrorBoundary>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  emptyContainer: {
    flex: 1,
  },
})
