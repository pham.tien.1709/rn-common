import {
  PressableProps,
  Pressable,
  StyleSheet,
  GestureResponderEvent,
  ViewStyle,
  ActivityIndicator,
} from 'react-native'
import React, {FC, forwardRef, useImperativeHandle} from 'react'
import {
  space,
  fontSize,
  defaultColor,
  colors,
  trueGray,
} from '@ui-kit/theme'
import {Text, TextProps} from './Text'
import {useTheme} from '@ui-kit/hook'
import {ThemeName} from '@ui-kit/types'
import {Haptic} from '@ui-kit/common'

export type ButtonType = 'solid' | 'outline' | 'ghost'
export type ButtonSize = 'xs' | 'sm' | 'md' | 'lg'

interface ItemType {
  color: string
}

export interface ButtonProps extends PressableProps {
  type?: ButtonType
  loading?: boolean
  size?: ButtonSize
  titleProps?: TextProps
  RightItem?: FC<ItemType>
  activeOpacity?: number
  theme?: ThemeName,
  LeftItem?: FC<ItemType>
}

export interface ButtonRef {
  onPress: (e: GestureResponderEvent) => void
}

export const Button = forwardRef<ButtonRef, ButtonProps>((props, ref) => {
  const {
    type = 'solid',
    loading,
    disabled,
    children,
    titleProps,
    size = 'md',
    RightItem,
    activeOpacity = 0.9,
    LeftItem,
  } = props
  const {theme} = useTheme()
  const textSize = fontSize[size]

  const {buttonStyle, textStyle} = dynamicTheme(
    props?.theme ?? theme,
    type,
    disabled
  )

  // handle ref
  useImperativeHandle(ref, () => ({
    onPress,
  }))

  const onPress = (e: GestureResponderEvent) => {
    Haptic()
    props?.onPress?.(e)
  }

  return (
    <Pressable
      {...props}
      disabled={disabled || loading}
      onPress={onPress}
      style={({pressed}) => [
        styles.container, // default style
        {padding: !children ? space.sm : space.xs}, // for children === null
        buttonStyle,
        {
          opacity: pressed ? activeOpacity : 1,
        },
        props?.style as ViewStyle, // custom style props
      ]}
    >
      <>
        {LeftItem ? (
          LeftItem({color: textStyle.color})
        ) : null}
        {typeof children === 'string' || typeof children === 'number' ? (
          <Text
            adjustsFontSizeToFit
            numberOfLines={1}
            {...titleProps}
            fontSize={size}
            style={[
              styles.text,
              {
                fontSize: textSize,
              },
              textStyle,
              titleProps?.style,
            ]}
          >
            {children || ''}
          </Text>
        ) : (
          children
        )}
        {loading ? (
          <ActivityIndicator color={textStyle.color} />
        ) : RightItem ? (
          RightItem({color: textStyle.color})
        ) : null}
      </>
    </Pressable>
  )
})

Button.displayName = 'Button'

export const dynamicTheme = (
  theme: ThemeName,
  type: ButtonType,
  disabled?: ButtonProps['disabled']
) => {
  if (theme === 'dark') {
    const dark = darkStyle(disabled)
    return {
      buttonStyle: dark[type],
      textStyle: dark[`${type}Text`],
    }
  }
  const light = lightStyle(disabled)

  return {
    buttonStyle: light[type],
    textStyle: light[`${type}Text`],
  }
}

// general style
const styles = StyleSheet.create({
  container: {
    borderRadius: space['3xs'],
    paddingHorizontal: space.sm,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    columnGap: space.xs,
  },
  text: {
    fontWeight: '500',
    textTransform: 'uppercase',
  },
})

const lightStyle = (disabled?: PressableProps['disabled']) =>
  StyleSheet.create({
    solid: {
      backgroundColor: disabled ? defaultColor[100] : colors.primary,
    },

    solidText: {
      color: disabled ? defaultColor[500] : colors.white,
    },

    outline: {
      borderWidth: 1,
      borderColor: disabled ? defaultColor[100] : trueGray[300],
    },

    outlineText: {
      color: disabled ? defaultColor[400] : defaultColor[500],
    },

    ghost: {
      //
    },
    ghostText: {
      color: disabled ? defaultColor[400] : colors.primary,
    },
  })

const darkStyle = (disabled?: PressableProps['disabled']) =>
  StyleSheet.create({
    // solid
    solid: {
      backgroundColor: disabled ? defaultColor[600] : colors.white,
    },
    solidText: {
      color: disabled ? defaultColor[400] : colors.primary,
    },
    // outline
    outline: {
      borderWidth: 1,
      borderColor: disabled ? defaultColor[400] : colors.white,
    },
    outlineText: {
      color: disabled ? defaultColor[400] : colors.white,
    },

    ghost: {
      //
    },
    ghostText: {
      color: disabled ? defaultColor[400] : colors.white,
    },
  })
