import React from 'react'
import {
  Text as RNText,
  type TextProps as RNTextProps,
  StyleSheet,
  type TextStyle,
} from 'react-native'
// import {Text as RNText} from 'react-native/Libraries/Text/Text'
import {colors, fontSize} from '@ui-kit/theme'
import {useGetFonts, useTheme} from '@ui-kit/hook'
import {FontFamily, FontSizeType} from '@ui-kit/types'

export interface TextProps extends Omit<RNTextProps, 'children'> {
  /**
   *  Renders components as Text children. Accepts a JSX.Element or an array of JSX.Element.
   */
  children?: React.ReactNode | string
  /**
   * The size of font
   */
  fontSize?: FontSizeType
  /**
   * Line height
   */
  lineHeight?: number
  /**
   * Font weight
   */
  fontWeight?: TextStyle['fontWeight']
  /**
   * Used to make the text bold.
   */
  bold?: boolean
  muted?: boolean
  fontFamily?: FontFamily
  color?: string
}

const convertPX = (value?: string) =>
  value ? parseFloat(value?.replace?.(/px/g, '')) : undefined

export const Text = (props: TextProps) => {
  const {children, fontWeight = '400', bold, color = colors.primary} = props
  const {fontFamily} = useGetFonts(props)

  const {foreground, muted} = useTheme()

  const sizeProp = props?.fontSize

  // check fontSize
  const size: number =
    fontSize?.[sizeProp as FontSizeType] ??
    ((typeof sizeProp === 'number'
      ? sizeProp
      : convertPX(sizeProp as string)) ||
      fontSize.md)

  const textStyle: TextStyle = StyleSheet.flatten([
    styles.container,
    {
      fontWeight: bold ? '700' : fontWeight,
      fontSize: size || fontSize['md'],
      color: props?.muted ? muted : color || foreground,
      fontFamily,
    },
    props?.style,
  ])

  const lineHeight =
    typeof props?.lineHeight === 'number'
      ? props?.lineHeight
      : convertPX(props?.lineHeight) || (textStyle.fontSize as number) * 1.5

  textStyle.lineHeight = lineHeight

  return (
    <RNText {...props} style={textStyle}>
      {children}
    </RNText>
  )
}

const styles = StyleSheet.create({
  container: {},
})
