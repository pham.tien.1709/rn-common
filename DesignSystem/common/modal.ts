import {screenSize} from '@ui-kit/theme'
import {
  ModalOptions,
  ModalStackConfig,
  createModalStack,
  modalfy,
  useModal as useRNModal,
} from 'react-native-modalfy'
import {Calendar, CalendarProps} from '@ui-kit/components/Calendar'
import {AlertModal, AlertViewProps} from '@ui-kit/components'
import {StatusBar} from 'react-native'
import Portal from '@ui-kit/components/Portal'
import {ReactNode} from 'react'
import {IS_ANDROID} from '@ui-kit/constant'

const modalConfig: ModalStackConfig = {
  Calendar: {
    modal: Calendar,
    position: 'bottom',
    backdropOpacity: 0,
    transitionOptions: () => {
      return {}
    },
    animateOutConfig: {
      duration: 0,
    },
    animateInConfig: {
      duration: 0,
    },
    backdropAnimationDuration: 0,
    // backdropAnimationDuration: 1,
    // disableFlingGesture: true,
  },
  Portal: {
    modal: Portal,
    transitionOptions: () => {
      return {}
    },
    animateOutConfig: {
      duration: 0,
    },
    animateInConfig: {
      duration: 0,
    },
    backdropOpacity: 0,
    backBehavior: 'none',
    backdropAnimationDuration: 100,
    disableFlingGesture: true,
    pointerEventsBehavior: 'none',
  },
  AlertPopup: {
    modal: AlertModal,
    position: 'center',
  },
  BannersPopup: {
    modal: Portal,
    transitionOptions: () => {
      return {}
    },
    animateOutConfig: {
      duration: 0,
    },
    animateInConfig: {
      duration: 0,
    },
    backdropOpacity: 0,
    backBehavior: 'none',
    backdropAnimationDuration: 100,
  },
}

const defaultConfig: ModalOptions = {
  animateInConfig: {
    duration: 500,
  },
  animateOutConfig: {
    duration: 500,
  },
  backdropOpacity: 0.5,
  transitionOptions: animatedValue => ({
    transform: [
      {
        translateY: animatedValue.interpolate({
          inputRange: [0, 1, 2],
          outputRange: [screenSize.height, 0, screenSize.height],
        }),
      },
      // {
      //   scale: animatedValue.interpolate({
      //     inputRange: [0, 1, 2],
      //     outputRange: [0, 1, 0.9],
      //   }),
      // },
    ],
  }),
}

export type ModalName = Exclude<keyof ModalList, 'Modal'>

export interface ModalList {
  Calendar: CalendarProps
  AlertPopup: AlertViewProps
  Portal: {
    children: ReactNode
  }
  BannersPopup: {
    children: ReactNode
  }
}

export const modalStack = createModalStack(modalConfig, defaultConfig)

export const setStatusBar = () => {
  if (IS_ANDROID) {
    StatusBar.setTranslucent(true)
    StatusBar.setBackgroundColor('transparent')
  }
}

export const useModal = () => useRNModal<ModalList>()

export const openCalendar = (options: CalendarProps) => {
  setStatusBar()
  openModal('Calendar', options)
}

export const openAlertPopup = (options: AlertViewProps) => {
  setStatusBar()
  openModal('AlertPopup', {props: options})
}

export const closeAlertPopup = (callback?: () => void) =>
  closeModal('AlertPopup', callback)

export const openPortal = (children: ReactNode) => {
  setStatusBar()
  openModal('Portal', {
    children,
  })
}
export const closePortal = (callback?: () => void) =>
  closeModal('Portal', callback)

export const openBannersPopup = (children: ReactNode) => {
  setStatusBar()
  openModal('BannersPopup', {
    children,
  })
}
export const closeBannersPopup = () => closeModal('BannersPopup')

export const {
  currentModal,
  openModal,
  closeModal,
  closeModals,
  closeAllModals,
} = modalfy<ModalList>()
