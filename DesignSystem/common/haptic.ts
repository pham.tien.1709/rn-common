import {HapticFeedbackTypes, trigger} from 'react-native-haptic-feedback'

const options = {
  enableVibrateFallback: false,
  ignoreAndroidSystemSettings: false,
}

export const Haptic = (
  type: HapticFeedbackTypes = HapticFeedbackTypes.keyboardPress
) => {
  trigger(type, options)
}
