/* eslint-disable import/no-named-as-default-member */
import {IconName} from '@ui-kit/components'
import {createRef} from 'react'
import {Haptic} from './haptic'
import {Status} from '@ui-kit/types'

export interface NotiBannerProps {}

export type NotifyType = 'solid' | 'outline' | 'subtle'

export interface NotifyOptions {
  icon?: IconName
  status?: Status
  type?: NotifyType
  title: string
  message?: string
  duration?: number
  onClick?: () => void
  onClose?: () => void
  dismissOnClick?: boolean
}

export type NotifyMethod = {
  show: (options: NotifyOptions) => void
  clearByIndex: (index: number) => void
}

class Notifier {
  notifyRef = createRef<NotifyMethod>()

  // show notify
  show = (options: NotifyOptions): void => {
    Haptic()
    this.notifyRef?.current?.show?.(options)
  }

  clearByIndex = (index: number) => {
    this.notifyRef?.current?.clearByIndex?.(index)
  }
}

export const Notify = new Notifier()
