import {Platform} from 'react-native'
import {getSystemVersion} from 'react-native-device-info'

export const IS_IOS = Platform.OS === 'ios'
export const IS_ANDROID = Platform.OS === 'android'
export const OS_VERSION = parseInt(getSystemVersion(), 10)
