import {avatarSize, colors, fontSize, space} from '@ui-kit/theme'

export type Level = 0 | 1 | 2 | 3 | 4 | 5

export type SizeType = keyof typeof space

export type FontSizeType = keyof typeof fontSize

export type ColorType = typeof colors

export type AvatarSize = typeof avatarSize

export type ThemeName = 'light' | 'dark'

export type ItemType = {
  color: string,
  image?: any;
}

export type Status =
  | 'primary'
  | 'danger'
  | 'success'
  | 'warning'
  | 'info'
  | 'default'

export type FontFamily = 'Inter' | 'Kantumruy Pro' | 'Noto Sans SC'
