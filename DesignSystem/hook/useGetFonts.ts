import {useTranslation} from '@i18n/useTranslation'
import {FontFamily} from '@ui-kit/types'

interface Props {
  fontFamily?: FontFamily
}
export function useGetFonts(props?: Props) {
  const {localeProvider} = useTranslation()

  const fontFamily: FontFamily =
    props?.fontFamily ?? localeProvider === 'km'
      ? 'Kantumruy Pro'
      : localeProvider === 'zh'
      ? 'Noto Sans SC'
      : 'Inter'

  return {fontFamily}
}
