import {RootState} from '@stores'
import {useSelector} from 'react-redux'

export * from './useToggle'
export * from './useTimeout'
export * from './useGetFonts'

export const useTheme = () => useSelector((state: RootState) => state.theme)
